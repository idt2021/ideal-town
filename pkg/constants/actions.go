package constants

const Action_Invalid uint32 = 0     // 非法操作
const Action_Deposit uint32 = 1     // 入金/轉入
const Action_Withdraw uint32 = 2    // 出金/轉出
const Action_Fee uint32 = 3         // 手續費
const Action_Charge uint32 = 4      // 充能
const Action_Uncharge uint32 = 5    // 取消充能
const Action_Reward uint32 = 6      // 創作獎勵
const Action_Revenue uint32 = 7     // 訂閱收入
const Action_Lock uint32 = 8        // 鎖定資產
const Action_Unlock uint32 = 9      // 解鎖資產
const Action_Buy uint32 = 10        // 購買
const Action_Sell uint32 = 11       // 掛賣
const Action_CancelSell uint32 = 12 // 取消掛賣
