package constants

const Phase_Polling uint32 = 1
const Phase_P2T uint32 = 2
const Phase_Trading uint32 = 3
