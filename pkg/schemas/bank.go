package schemas

type Banks struct {
	Base

	Tid    uint32 `gorm:"primaryKey"`
	Uid    string
	Action uint32
	Remark string
	Idc    float32
}

type Wallets struct {
	Base

	Uid string `gorm:"primaryKey"`
	Idc float32
}

type Locks struct {
	Base

	Lid uint32 `gorm:"primaryKey"`
	Uid string
	Idc float32
}
