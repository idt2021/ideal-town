-- SECTION BANK
CREATE DATABASE IF NOT EXISTS bank CHARACTER SET utf8mb4;
use bank;

CREATE TABLE banks(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    tid  INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid  VARCHAR(36) NOT NULL,
    action INT UNSIGNED NOT NULL CHECK (action > 0),
    remark VARCHAR(32) NOT NULL,
    idc FLOAT NOT NULL,

    PRIMARY KEY(tid)
)ENGINE = INNODB;

CREATE TABLE wallets(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    uid VARCHAR(36) NOT NULL,
    idc FLOAT NOT NULL,

    PRIMARY KEY(uid)
)ENGINE = INNODB;

CREATE TABLE locks(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,
    
    lid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    idc FLOAT NOT NULL CHECK (idc > 0),

    PRIMARY KEY(lid)
)ENGINE = INNODB;