CREATE DATABASE IF NOT EXISTS geopolitical CHARACTER SET utf8mb4;

use geopolitical;


CREATE TABLE transactions(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    tid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    Iid INT UNSIGNED NOT NULL,
    uid VARCHAR(36) NOT NULL,
    action INT UNSIGNED NOT NULL CHECK (action > 0), 
    idc INT UNSIGNED NOT NULL,
    idt INT UNSIGNED NOT NULL,

    PRIMARY KEY(tid)
) ENGINE = INNODB;

CREATE TABLE orders(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    oid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,
    price INT UNSIGNED NOT NULL,
    origin INT UNSIGNED NOT NULL,
    remain INT UNSIGNED NOT NULL,

    PRIMARY KEY(oid)
) ENGINE = INNODB;

CREATE TABLE idt_records(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,
    action INT UNSIGNED NOT NULL CHECK (action > 0),
    idt INT UNSIGNED NOT NULL,

    PRIMARY KEY(id)
) ENGINE = INNODB;

CREATE TABLE assets(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    iid INT UNSIGNED NOT NULL,
    uid VARCHAR(36) NOT NULL,
    idt INT UNSIGNED NOT NULL,
    is_locked BOOLEAN NOT NULL,

    PRIMARY KEY(iid,uid)
) ENGINE = INNODB;