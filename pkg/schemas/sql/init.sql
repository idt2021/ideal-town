-- SECTION geopolitical
CREATE DATABASE IF NOT EXISTS geopolitical CHARACTER SET utf8mb4;
use geopolitical;

CREATE TABLE transactions(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    tid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    Iid INT UNSIGNED NOT NULL,
    uid VARCHAR(36) NOT NULL,
    action INT UNSIGNED NOT NULL CHECK (action > 0), 
    idc FLOAT NOT NULL,
    idt INT UNSIGNED NOT NULL,

    PRIMARY KEY(tid)
) ENGINE = INNODB;

CREATE TABLE orders(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    oid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,
    price FLOAT NOT NULL,
    origin INT UNSIGNED NOT NULL,
    remain INT UNSIGNED NOT NULL,

    PRIMARY KEY(oid)
) ENGINE = INNODB;

CREATE TABLE idt_records(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,
    action INT UNSIGNED NOT NULL CHECK (action > 0),
    idt INT UNSIGNED NOT NULL,

    PRIMARY KEY(id)
) ENGINE = INNODB;

CREATE TABLE assets(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    iid INT UNSIGNED NOT NULL,
    uid VARCHAR(36) NOT NULL,
    idt INT UNSIGNED NOT NULL,
    is_locked INT UNSIGNED NOT NULL,

    PRIMARY KEY(iid,uid)
) ENGINE = INNODB;

CREATE TABLE ideas(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,
    
	iid         INT UNSIGNED NOT NULL AUTO_INCREMENT,
	uid         VARCHAR(36) NOT NULL,
	phase       INT NOT NULL,
	winner_sid   INT UNSIGNED DEFAULT 0,
	title       VARCHAR(100) NOT NULL,
	description TEXT,
	link        TEXT,
    gid         CHAR(8) NOT NULL,

    PRIMARY KEY(iid)
) ENGINE = INNODB;

CREATE TABLE solutions(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,
    
	sid         INT UNSIGNED NOT NULL AUTO_INCREMENT,
	iid         INT UNSIGNED NOT NULL,
	uid         VARCHAR(36) NOT NULL,
    link        TEXT,
    gid         CHAR(8) NOT NULL,

    PRIMARY KEY(sid, iid)
) ENGINE = INNODB;

CREATE TABLE pollings(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,
    
	pid         INT UNSIGNED NOT NULL AUTO_INCREMENT,
	iid         INT UNSIGNED NOT NULL,
	sid         INT UNSIGNED NOT NULL,
	uid         VARCHAR(36) NOT NULL,
	target      INT NOT NULL,
	idc         FLOAT NOT NULL,

    PRIMARY KEY(pid)
) ENGINE = INNODB;

CREATE TABLE prs(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    rid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,
    link TEXT NOT NULL,
    gid CHAR(8) NOT NULL,
    PRIMARY KEY(rid)
) ENGINE = INNODB;

CREATE TABLE charges(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    uid VARCHAR(36) NOT NULL,
    rid INT UNSIGNED NOT NULL,

    PRIMARY KEY(rid, uid)
) ENGINE = INNODB;

CREATE TABLE subscriptions(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    ssid INT UNSIGNED NOT NULL AUTO_INCREMENT,
    uid VARCHAR(36) NOT NULL,
    iid INT UNSIGNED NOT NULL,

    PRIMARY KEY(ssid)
) ENGINE = INNODB;