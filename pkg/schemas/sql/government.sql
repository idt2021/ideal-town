-- SECTION GOVERNMENT
CREATE DATABASE IF NOT EXISTS government CHARACTER SET utf8mb4;
use government;

CREATE TABLE timers(
    created_at DATETIME,
    updated_at DATETIME,
    deleted_at DATETIME,

    category INT UNSIGNED NOT NULL,
    id INT UNSIGNED NOT NULL,
    deadline DATETIME NOT NULL,

    PRIMARY KEY(category, id)
)ENGINE = INNODB;
