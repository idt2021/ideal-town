package schemas

type IdtRecords struct {
	Base

	ID     uint32 `gorm:"primaryKey"`
	Uid    string
	Iid    uint32
	Action uint32
	Idt    uint32
}

type Assets struct {
	Base

	Iid      uint32 `gorm:"primaryKey"`
	Uid      string
	Idt      uint32
	IsLocked uint32
}
