package schemas

// 交易紀錄
//
// 當成交後，會新增買賣兩方的掛單
type Transactions struct {
	Base

	Tid    uint32 `gorm:"primaryKey"`
	Uid    string
	Iid    uint32
	Idc    float32
	Idt    uint32
	Action uint32
}

// 賣方掛單
type Orders struct {
	Base

	Oid    uint32 `gorm:"primaryKey"`
	Uid    string
	Iid    uint32
	Price  float32
	Origin uint32
	Remain uint32
}
