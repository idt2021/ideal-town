package schemas

type Prs struct {
	Base

	Rid         uint32
	Iid         uint32
	Uid         string
	Link        string
	Gid 		string
}

type Charges struct {
	Base

	Rid         uint32
	Uid         string
}