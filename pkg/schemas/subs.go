package schemas

type Subscriptions struct {
	Base

	Ssid		uint32
	Iid         uint32
	Uid         string
}