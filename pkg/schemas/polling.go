package schemas

type Ideas struct {
	Base

	Iid         uint32
	Uid         string
	Phase       uint32
	WinnerSid   uint32
	Title       string
	Description string
	Link        string
	Gid         string
}
type Solutions struct {
	Base

	Sid  uint32
	Iid  uint32
	Uid  string
	Link string
	Gid  string
}
type Pollings struct {
	Base

	Pid    uint32
	Iid    uint32
	Sid    uint32 // idea: 0, solution:>0
	Uid    string
	Target int32 // 1: idea , 2: solution
	Idc    float32 // +: deposit -: withdraw
}
