package schemas

import "time"

type Timer struct {
	Base

	Category uint32
	Id       uint32
	Deadline time.Time
}
