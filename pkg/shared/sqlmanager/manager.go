package sqlmanager

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Topic: Why not to use sqlmock to test ?
// Ans: https://github.com/go-gorm/gorm/issues/3565

type Agent struct {
	Conn   *gorm.DB
	unsafe bool
}

type ConnInfo struct {
	Username  string
	Password  string
	Host      string
	Port      string
	DBName    string
	DebugMode bool
}

// NewConn establish a new connection to database, and some unsafe methods are disabled.
func NewConn(info *ConnInfo) *Agent {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Asia%sTaipei",
		info.Username, info.Password, info.Host, info.Port, info.DBName, "%2f")
	conn, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to create new db connection: %v", err)
	}
	return &Agent{Conn: conn}
}

// NewUnsafeConn establish a new debug connection to database with all danger features on.
func NewUnsafeConn(info *ConnInfo) *Agent {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Asia%sTaipei",
		info.Username, info.Password, info.Host, info.Port, info.DBName, "%2f")
	conn, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		AllowGlobalUpdate: true,
	})
	if err != nil {
		log.Fatalf("Failed to create new db connection: %v", err)
	}
	log.Println("\033[31mWatch out! Your database connection is in UNSAFE mode\033[0m")
	agent := Agent{Conn: conn, unsafe: true}
	if info.DebugMode {
		agent.Conn = agent.Conn.Debug()
	}
	return &agent
}

// ResetDB physically delete all given table records. If you're not in unsafe mode
// then no operations will be held.
func (agent *Agent) ResetDB(table ...interface{}) {
	if !agent.unsafe {
		log.Println("\033[31mWatch out! Your database is in SAFE mode, ResetDB is disabled.\033[0m")
		return
	}
	for _, t := range table {
		agent.Conn.Unscoped().Delete(t)
	}
}
