package sqlmanager

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

func TestNewConn(t *testing.T) {
	assert.NotPanics(t, func() {
		x := NewConn(&ConnInfo{
			Username: "alankingdom",
			Password: "idtps",
			Host:     "140.113.67.110",
			Port:     "5211",
			DBName:   "idtdb",
		})
		assert.Equal(t, false, x.unsafe)
		sqlDB, _ := x.Conn.DB()
		sqlDB.Close()
	})
}

func TestNewUnsafeConn(t *testing.T) {
	assert.NotPanics(t, func() {
		x := NewUnsafeConn(&ConnInfo{
			Username: "alankingdom",
			Password: "idtps",
			Host:     "140.113.67.110",
			Port:     "5211",
			DBName:   "idtdb",
		})
		assert.Equal(t, true, x.unsafe)
		sqlDB, _ := x.Conn.DB()
		sqlDB.Close()
	})
}

func TestResetDB(t *testing.T) {
	agent := NewUnsafeConn(&ConnInfo{
		Username: "alankingdom",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "idtdb",
	})
	type testTable1 struct {
		gorm.Model
		Name string
	}
	type testTable2 struct {
		gorm.Model
		Gender bool
	}

	agent.Conn.Migrator().DropTable(&testTable1{}, &testTable2{})
	agent.Conn.Migrator().CreateTable(&testTable1{}, &testTable2{})
	assert.True(t, agent.Conn.Migrator().HasTable(&testTable1{}))
	assert.True(t, agent.Conn.Migrator().HasTable(&testTable2{}))

	agent.Conn.Create(&testTable1{Name: "a"})
	agent.Conn.Create(&testTable1{Name: "b"})
	agent.Conn.Create(&testTable1{Name: "c"})

	agent.Conn.Create(&testTable2{Gender: true})
	agent.Conn.Create(&testTable2{Gender: false})
	agent.Conn.Create(&testTable2{Gender: false})

	var count int64 = -1
	agent.Conn.Model(&testTable1{}).Count(&count)
	assert.Zero(t, count)
	agent.Conn.Model(&testTable2{}).Count(&count)
	assert.Zero(t, count)

	// Start test
	agent.ResetDB(&testTable1{}, &testTable2{})
	count = -1
	agent.Conn.Model(&testTable1{}).Count(&count)
	assert.Zero(t, count)
	agent.Conn.Model(&testTable2{}).Count(&count)
	assert.Zero(t, count)

	agent.Conn.Migrator().DropTable(&testTable1{}, &testTable2{})
	sqlDB, _ := agent.Conn.DB()
	sqlDB.Close()

}
