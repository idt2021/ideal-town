package errors

// Input argument error
const ErrExpectedPositive = "expected number to be 0 or positive"
const ErrExpectedNegative = "expected number to be 0 or negative"

// Stage error
const ErrStageNotReached = "wrong stage"

// Assets error
const ErrNoEnoughIDC = "no enough IDC"
const ErrNoEnoughIDT = "no enough IDT"
const ErrAssetsIsLocked = "your asset is locked"

// Not found error
const ErrPollingNotFound = "polling not found"
const ErrUserNotFound = "uid not found"
const ErrIdeaNotFound = "idea not found"
const ErrSolutionNotFound = "solution not found"
const ErrPullRequestNotFound = "pull request not found"
const ErrChargeRecordNotFound = "charge record not found"
const ErrSubscriptionNotFound = "subscription not found"
const ErrLocksNotFound = "lid not found"

// Invalid action
const ErrInvalidAction = "invalid action"

// Already Exist
const ErrUserAlreadyExist = "user already exist"
