package errors

const ErrMustHaveTitle = "title can not be empty string"
const ErrMustHaveGitlabID = "missing gitlab id"