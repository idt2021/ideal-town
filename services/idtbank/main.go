package main

import (
	"context"
	gw "ideal-town/api/proto/v1/idtbank"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	idtbank "ideal-town/services/idtbank/internal/microserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":6000"
	httpServerEndpoint = ":9005"
)

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterIdtAssetHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	s := grpc.NewServer()
	gw.RegisterIdtAssetServer(s, &idtbank.IdtBankServer{
		Agent: agent,
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}