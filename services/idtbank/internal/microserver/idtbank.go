// package bankserver dedicate to control the gRPC flow of bank service
package idtbankserver

import (
	"context"
	"ideal-town/api/proto/v1/common"
	pib "ideal-town/api/proto/v1/idtbank"
	ibkdb "ideal-town/services/idtbank/internal/microserver/idtbankdb"

	"ideal-town/pkg/shared/sqlmanager"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IdtBankServer struct {
	Agent *sqlmanager.Agent
	pib.UnimplementedIdtAssetServer
}

// TODO
// 查看使用者在某個idea中擁有的IDT
func (b *IdtBankServer) GetAmountByUser(ctx context.Context, req *pib.AmountRequest) (*pib.AmountReply, error) {
	amount, err := ibkdb.GetAmountByUser(b.Agent.Conn, req.Uid, req.Iid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pib.AmountReply{Uid: amount.Uid, Iid: amount.Iid, Idt: amount.Idt}, nil
}
func (b *IdtBankServer) GetManyAmountByUser(ctx context.Context, req *pib.AmountRequest) (*pib.AmountReplyList, error) {
	amountList, err := ibkdb.GetManyAmountByUser(b.Agent.Conn, req.Uid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	var amountReply []*pib.AmountReply
	for _, ele := range amountList {
		amountReply = append(amountReply, &pib.AmountReply{Uid: ele.Uid, Iid: ele.Iid, Idt: ele.Idt})
	}
	return &pib.AmountReplyList{Amount: amountReply}, nil
}

// 查看某個idea中所有的IDT數量
func (b *IdtBankServer) GetAmountByIdea(ctx context.Context, req *pib.AmountRequest) (*pib.AmountReplyList, error) {
	amountList, err := ibkdb.GetAmountByIdea(b.Agent.Conn, req.Iid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	var amountReply []*pib.AmountReply
	for _, ele := range amountList {
		amountReply = append(amountReply, &pib.AmountReply{Uid: ele.Uid, Iid: ele.Iid, Idt: ele.Idt})
	}
	return &pib.AmountReplyList{Amount: amountReply}, nil
}

// 將IDT轉出IDT銀行
//  1.  查看Assets中IDT餘額是否足夠&是否有鎖上
//  2.  如果足夠，扣除IDT餘額，更新IdtRecords
//  3.  同時更新Assets中的數量
func (b *IdtBankServer) Withdraw(ctx context.Context, req *pib.IdtAssetRequest) (*common.Empty, error) {
	_, err := ibkdb.Withdraw(b.Agent.Conn, req.Uid, req.Iid, req.Idt, uint32(req.Action))
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

// 將IDT存入IDT銀行
// 同時更新Assets中的數量
func (b *IdtBankServer) Deposit(ctx context.Context, req *pib.IdtAssetRequest) (*common.Empty, error) {
	parm := &pib.AmountRequest{Iid: req.GetIid(), Uid: req.GetUid()}
	if _, err := b.GetAmountByUser(context.Background(), parm); err != nil {
		if err := ibkdb.CreateIdtAssets(b.Agent.Conn, req.Iid, req.Uid); err != nil {
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	_, err := ibkdb.Deposit(b.Agent.Conn, req.Uid, req.Iid, req.Idt, uint32(req.Action))
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	er := ibkdb.CreateTransRecord(b.Agent.Conn, req.GetIid(), req.GetUid(), uint32(req.GetAction()), 0.0, req.GetIdt())
	if er != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

// 將IDT進行鎖定
// 1. 檢查餘額是否足夠
// 2. 在idtbank新增一筆 action是lock的紀錄 回傳id
// 3. (這次先不會實踐)逾期沒有Confirm就會自動Unlock
func (b *IdtBankServer) Lock(ctx context.Context, req *pib.LockRequest) (*pib.IdInfo, error) {
	lid, err := ibkdb.Lock(b.Agent.Conn, req.Iid, req.Uid, req.Idt)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pib.IdInfo{Id: lid}, nil
}

// 發生錯誤時，將IDT解鎖
// 1. 檢查bank是否存在此tid
// 2. 將tid從lock中刪除
func (b *IdtBankServer) Unlock(ctx context.Context, req *pib.IdInfo) (*common.Empty, error) {
	_, err := ibkdb.Unlock(b.Agent.Conn, req.Id)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil

}

// 執行成功，將IDT轉為支付紀錄
// 1. 檢查是否存在id
// 2. 將此id的action改為Withdraw
func (b *IdtBankServer) Confirm(ctx context.Context, req *pib.IdInfo) (*common.Empty, error) {
	_, err := ibkdb.Confirm(b.Agent.Conn, req.Id)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
