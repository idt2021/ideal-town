package idtbankserver

import (
	"context"
	"ideal-town/api/proto/v1/common"
	pib "ideal-town/api/proto/v1/idtbank"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	schemas "ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const ConnByteSize = 1024 * 1024
const DialTarget = ""

var agent = initUnsafeBankAgent()

func initUnsafeBankAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}
func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.IdtRecords{}, &schemas.Assets{})
}

// Create dialer with db connection
func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	pib.RegisterIdtAssetServer(server, &IdtBankServer{
		Agent: agent,
	})
	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()
	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, pib.IdtAssetClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pib.NewIdtAssetClient(conn)
	return ctx, conn, client
}
func TestIdtBankServer_GetAmountByUser(t *testing.T) {
	type args struct {
		uid string
		iid uint32
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				uid: "a",
				iid: 1,
			},

			wantErr: nil,
		},
		{
			name: "[x] Uid in Assets Not Found",
			args: args{
				uid: "b",
				iid: 1,
			},
			wantErr: status.Error(codes.Aborted, errs.ErrAssetsNotFound),
		},
		{
			name: "[x] Iid in Assets Not Found",
			args: args{
				uid: "a",
				iid: 2,
			},
			wantErr: status.Error(codes.Aborted, errs.ErrAssetsNotFound),
		},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			_, err := client.GetAmountByUser(ctx, &pib.AmountRequest{Uid: tt.args.uid, Iid: tt.args.iid})
			assert.Equal(t, tt.wantErr, err)

		})
	}
}

func TestIdtBankServer_GetManyAmountByUser(t *testing.T) {
	type args struct {
		uid string
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				uid: "a",
			},

			wantErr: nil,
		},
		{
			name: "[v] valid",
			args: args{
				uid: "b",
			},

			wantErr: nil,
		},
		{
			name: "[x] Uid Assets Not Found",
			args: args{
				uid: "c",
			},

			wantErr: status.Error(codes.Aborted, errs.ErrAssetsNotFound),
		},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "b", Idt: 200, IsLocked: 1})
			agent.Conn.Create(&schemas.Assets{Iid: 3, Uid: "b", Idt: 300, IsLocked: 1})
			_, err := client.GetManyAmountByUser(ctx, &pib.AmountRequest{Uid: tt.args.uid})
			assert.Equal(t, tt.wantErr, err)

		})
	}
}

func TestIdtBankServer_GetAmountByIdea(t *testing.T) {
	type args struct {
		iid uint32
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				iid: 1,
			},

			wantErr: nil,
		},
		{
			name: "[v] valid",
			args: args{
				iid: 2,
			},

			wantErr: nil,
		},
		{
			name: "[x] Iid Assets Not Found",
			args: args{
				iid: 3,
			},
			wantErr: status.Error(codes.Aborted, errs.ErrAssetsNotFound),
		},
	}
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "b", Idt: 200, IsLocked: 1})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "c", Idt: 300, IsLocked: 1})
			ctx, conn, client := createConn(t)
			defer conn.Close()
			_, err := client.GetAmountByIdea(ctx, &pib.AmountRequest{Iid: tt.args.iid})
			assert.Equal(t, tt.wantErr, err)

		})
	}
}

func TestIdtBankServer_Withdraw(t *testing.T) {
	type args struct {
		uid    string
		iid    uint32
		idt    uint32
		action common.Action
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		want    *common.Empty
		wantErr error
	}{
		{"[v] Valid 1", args{"a", 1, 30, common.Action_Withdraw}, 70, nil, nil},
		{"[v] Valid 2", args{"a", 1, 40, common.Action_Withdraw}, 30, nil, nil},
		{"[x] ErrNoEnoughIDT", args{"a", 1, 50, common.Action_Withdraw}, 30, nil, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)},
		{"[x] ErrInvalidAction 1", args{"a", 1, 50, common.Action_Deposit}, 30, nil, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] ErrInvalidAction 2", args{"a", 1, 30, common.Action_Lock}, 30, nil, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] ErrAssetsNotFound", args{"b", 1, 30, common.Action_Withdraw}, 0, nil, status.Error(codes.Aborted, errs.ErrAssetsNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Withdraw(ctx, &pib.IdtAssetRequest{Uid: tt.args.uid, Iid: tt.args.iid, Idt: tt.args.idt, Action: tt.args.action})
			assert.Equal(t, tt.wantErr, err)

		})
	}
}

func TestIdtBankServer_Deposit(t *testing.T) {
	type args struct {
		uid    string
		iid    uint32
		idt    uint32
		action common.Action
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		want    *common.Empty
		wantErr error
	}{
		{"[v] Valid 1", args{"a", 1, 30, common.Action_Deposit}, 40, nil, nil},
		{"[v] Valid 2", args{"a", 1, 40, common.Action_Deposit}, 80, nil, nil},
		{"[x] ErrInvalidAction 1", args{"a", 1, 50, common.Action_Withdraw}, 30, nil, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] ErrInvalidAction 2", args{"a", 1, 30, common.Action_Fee}, 30, nil, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] ErrAssetsNotFound", args{"b", 1, 30, common.Action_Deposit}, 0, nil, status.Error(codes.Aborted, errs.ErrAssetsNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Deposit(ctx, &pib.IdtAssetRequest{Uid: tt.args.uid, Iid: tt.args.iid, Idt: tt.args.idt, Action: tt.args.action})
			assert.Equal(t, tt.wantErr, err)

		})
	}
}

func TestIdtBankServer_Lock(t *testing.T) {
	type args struct {
		iid uint32
		uid string
		idt uint32
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		wantErr error
	}{
		{"[v] Valid", args{1, "a", 20}, 80, nil},
		{"[v] Valid", args{1, "a", 50}, 30, nil},
		{"[x] no enough idt", args{1, "a", 100}, 11, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)},
		{"[x] user not found", args{1, "b", 10}, 11, status.Error(codes.Aborted, errs.ErrAssetsNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Lock(ctx, &pib.LockRequest{Iid: tt.args.iid, Uid: tt.args.uid, Idt: tt.args.idt})
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func TestIdtBankServer_Unlock(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		lid    uint32
		remain uint32
		err    error
	}{
		{"[v] Valid", 1, "a", 0, 150, nil},
		{"[x] lock not found", 1, "b", 0, 150, status.Error(codes.Aborted, errs.ErrLocksNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 150})
	lid, _ := client.Lock(ctx, &pib.LockRequest{Iid: 1, Uid: "a", Idt: 100})
	tests[0].lid = lid.Id

	var check schemas.Assets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Unlock(ctx, &pib.IdInfo{Id: tt.lid})
			assert.Equal(t, tt.err, err)
			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&check)
			assert.Equal(t, tt.remain, check.Idt)
		})
	}
}

func TestIdtBankServer_Confirm(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		lid    uint32
		remain uint32
		err    error
	}{
		{"[v] Valid", 1, "a", 0, 50, nil},
		{"[x] lock not found", 1, "b", 0, 50, status.Error(codes.Aborted, errs.ErrLocksNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Uid: "a", Iid: 1, Idt: 150})

	lid, _ := client.Lock(ctx, &pib.LockRequest{Iid: 1, Uid: "a", Idt: 100})
	tests[0].lid = lid.Id

	var check schemas.Assets
	var record schemas.IdtRecords
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Confirm(ctx, &pib.IdInfo{Id: tt.lid})
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&check)
			assert.Equal(t, tt.remain, check.Idt)

			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&record)
			assert.Equal(t, constants.Action_Sell, record.Action)
		})
	}
}
