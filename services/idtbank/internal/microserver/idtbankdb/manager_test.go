package idtbankdb

import (
	"errors"
	"ideal-town/api/proto/v1/common"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.IdtRecords{}, &schemas.Assets{})
}
func TestGetAmountByUser(t *testing.T) {
	type args struct {
		uid string
		iid uint32
	}
	tests := []struct {
		name    string
		args    args
		want    Amount
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				uid: "a",
				iid: 1,
			},
			want: Amount{
				Uid: "a",
				Iid: 1,
				Idt: 100,
			},
			wantErr: nil,
		},
		{
			name: "[x] Uid in Assets Not Found",
			args: args{
				uid: "b",
				iid: 1,
			},
			want:    Amount{},
			wantErr: errors.New(errs.ErrAssetsNotFound),
		},
		{
			name: "[x] Iid in Assets Not Found",
			args: args{
				uid: "a",
				iid: 2,
			},
			want:    Amount{},
			wantErr: errors.New(errs.ErrAssetsNotFound),
		},
	}
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			got, err := GetAmountByUser(agent.Conn, tt.args.uid, tt.args.iid)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)

		})
	}
}

func TestGetManyAmountByUser(t *testing.T) {
	type args struct {
		uid string
	}
	tests := []struct {
		name    string
		args    args
		want    []Amount
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				uid: "a",
			},
			want: []Amount{
				{
					Uid: "a",
					Iid: 1,
					Idt: 100,
				},
			},
			wantErr: nil,
		},
		{
			name: "[v] valid",
			args: args{
				uid: "b",
			},
			want: []Amount{
				{
					Uid: "b",
					Iid: 1,
					Idt: 100,
				},
				{
					Uid: "b",
					Iid: 2,
					Idt: 200,
				},
				{
					Uid: "b",
					Iid: 3,
					Idt: 300,
				},
			},
			wantErr: nil,
		},
		{
			name: "[x] Uid Assets Not Found",
			args: args{
				uid: "c",
			},
			want:    nil,
			wantErr: errors.New(errs.ErrAssetsNotFound),
		},
	}
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "b", Idt: 200, IsLocked: 1})
			agent.Conn.Create(&schemas.Assets{Iid: 3, Uid: "b", Idt: 300, IsLocked: 1})
			got, err := GetManyAmountByUser(agent.Conn, tt.args.uid)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)

		})
	}
}

func TestGetAmountByIdea(t *testing.T) {
	type args struct {
		iid uint32
	}
	tests := []struct {
		name    string
		args    args
		want    []Amount
		wantErr error
	}{
		{
			name: "[v] valid",
			args: args{
				iid: 1,
			},
			want: []Amount{
				{
					Uid: "a",
					Iid: 1,
					Idt: 100,
				},
			},
			wantErr: nil,
		},
		{
			name: "[v] valid",
			args: args{
				iid: 2,
			},
			want: []Amount{
				{
					Uid: "a",
					Iid: 2,
					Idt: 100,
				},
				{
					Uid: "b",
					Iid: 2,
					Idt: 200,
				},
				{
					Uid: "c",
					Iid: 2,
					Idt: 300,
				},
			},
			wantErr: nil,
		},
		{
			name: "[x] Iid Assets Not Found",
			args: args{
				iid: 3,
			},
			want:    nil,
			wantErr: errors.New(errs.ErrAssetsNotFound),
		},
	}
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "a", Idt: 100, IsLocked: 0})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "b", Idt: 200, IsLocked: 1})
			agent.Conn.Create(&schemas.Assets{Iid: 2, Uid: "c", Idt: 300, IsLocked: 1})
			got, err := GetAmountByIdea(agent.Conn, tt.args.iid)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)

		})
	}
}

func TestWithdraw(t *testing.T) {
	type args struct {
		uid    string
		iid    uint32
		idt    uint32
		action uint32
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		want    *common.Empty
		wantErr error
	}{
		{"[v] Valid 1", args{"a", 1, 30, constants.Action_Withdraw}, 70, nil, nil},
		{"[v] Valid 2", args{"a", 1, 40, constants.Action_Withdraw}, 30, nil, nil},
		{"[x] ErrNoEnoughIDT", args{"a", 1, 50, constants.Action_Withdraw}, 30, nil, errors.New(errs.ErrNoEnoughIDT)},
		{"[x] ErrInvalidAction 1", args{"a", 1, 50, constants.Action_Deposit}, 30, nil, errors.New(errs.ErrInvalidAction)},
		{"[x] ErrInvalidAction 2", args{"a", 1, 30, constants.Action_Lock}, 30, nil, errors.New(errs.ErrInvalidAction)},
		{"[x] ErrAssetsNotFound", args{"b", 1, 30, constants.Action_Withdraw}, 0, nil, errors.New(errs.ErrAssetsNotFound)},
	}
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Withdraw(agent.Conn, tt.args.uid, tt.args.iid, tt.args.idt, tt.args.action)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)
			if err == nil {
				amount, _ := GetAmountByUser(agent.Conn, tt.args.uid, tt.args.iid)
				assert.Equal(t, tt.remain, amount.Idt)
			}

		})
	}
}

func TestDeposit(t *testing.T) {
	type args struct {
		uid    string
		iid    uint32
		idt    uint32
		action uint32
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		want    *common.Empty
		wantErr error
	}{
		{"[v] Valid 1", args{"a", 1, 30, constants.Action_Deposit}, 40, nil, nil},
		{"[v] Valid 2", args{"a", 1, 40, constants.Action_Deposit}, 80, nil, nil},
		{"[x] ErrInvalidAction 1", args{"a", 1, 50, constants.Action_Withdraw}, 30, nil, errors.New(errs.ErrInvalidAction)},
		{"[x] ErrInvalidAction 2", args{"a", 1, 30, constants.Action_Fee}, 30, nil, errors.New(errs.ErrInvalidAction)},
		{"[x] ErrAssetsNotFound", args{"b", 1, 30, constants.Action_Deposit}, 0, nil, errors.New(errs.ErrAssetsNotFound)},
	}
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Deposit(agent.Conn, tt.args.uid, tt.args.iid, tt.args.idt, tt.args.action)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)
			if err == nil {
				amount, _ := GetAmountByUser(agent.Conn, tt.args.uid, tt.args.iid)
				assert.Equal(t, tt.remain, amount.Idt)
			}

		})
	}
}

func TestLock(t *testing.T) {
	type args struct {
		iid uint32
		uid string
		idt uint32
	}
	tests := []struct {
		name    string
		args    args
		remain  uint32
		wantErr error
	}{
		{"[v] Valid", args{1, "a", 20}, 80, nil},
		{"[v] Valid", args{1, "a", 50}, 30, nil},
		{"[x] no enough idt", args{1, "a", 100}, 11, errors.New(errs.ErrNoEnoughIDT)},
		{"[x] user not found", args{1, "b", 10}, 11, errors.New(errs.ErrAssetsNotFound)},
	}
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 100, IsLocked: 0})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Lock(agent.Conn, tt.args.iid, tt.args.uid, tt.args.idt)
			assert.Equal(t, tt.wantErr, err)
			if err == nil {
				amount, _ := GetAmountByUser(agent.Conn, tt.args.uid, tt.args.iid)
				assert.Equal(t, tt.remain, amount.Idt)
			}

		})
	}
}

func TestUnlock(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		lid    uint32
		remain uint32
		err    error
	}{
		{"[v] Valid", 1, "a", 0, 150, nil},
		{"[x] lock not found", 1, "b", 0, 150, errors.New(errs.ErrLocksNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 150})
	lid, _ := Lock(agent.Conn, 1, "a", 100)
	tests[0].lid = lid

	var check schemas.Assets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Unlock(agent.Conn, tt.lid)
			assert.Equal(t, tt.err, err)
			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&check)
			assert.Equal(t, tt.remain, check.Idt)
		})
	}
}

func TestConfirm(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		lid    uint32
		remain uint32
		err    error
	}{
		{"[v] Valid", 1, "a", 0, 50, nil},
		{"[x] lock not found", 1, "b", 0, 50, errors.New(errs.ErrLocksNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Uid: "a", Iid: 1, Idt: 150})

	lid, _ := Lock(agent.Conn, 1, "a", 100)
	tests[0].lid = lid

	var check schemas.Assets
	var record schemas.IdtRecords
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Confirm(agent.Conn, tt.lid)
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&check)
			assert.Equal(t, tt.remain, check.Idt)

			agent.Conn.Where("uid=? and iid=?", tt.uid, tt.iid).Take(&record)
			assert.Equal(t, constants.Action_Sell, record.Action)
		})
	}
}
