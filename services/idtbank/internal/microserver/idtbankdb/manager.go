package idtbankdb

import (
	"errors"
	"ideal-town/api/proto/v1/common"
	"ideal-town/pkg/constants"
	"ideal-town/pkg/schemas"
	"time"

	errs "ideal-town/pkg/errors"

	"gorm.io/gorm"
)

// func sessionCloser(conn *gorm.DB) {
// 	if stmtManger, ok := tx.ConnPool.(*gorm.PreparedStmtDB); ok {
// 		stmtManger.Close()
// 	}
// }

type BankRecord struct {
	CreatedAt time.Time
	Action    uint32
	Idc       float32
}
type Amount struct {
	Uid string
	Iid uint32
	Idt uint32
}

func CreateIdtAssets(conn *gorm.DB, iid uint32, uid string) error {
	if err := conn.Create(&schemas.Assets{Iid: iid, Uid: uid}).Error; err != nil {
		return err
	}
	return nil
}

func CreateTransRecord(conn *gorm.DB, iid uint32, uid string, action uint32, idc float32, idt uint32) error {
	if err := conn.Create(&schemas.Transactions{Iid: iid, Uid: uid, Action: action, Idc: idc, Idt: idt}).Error; err != nil {
		return err
	}
	return nil
}

// 查看使用者在某個idea中擁有的IDT
func GetAmountByUser(conn *gorm.DB, uid string, iid uint32) (Amount, error) {
	var assets *schemas.Assets
	if err := conn.Model(&schemas.Assets{}).Where("uid=? and iid=?", uid, iid).Take(&assets).Error; err != nil {
		return Amount{}, errors.New(errs.ErrAssetsNotFound)
	}
	return Amount{Iid: assets.Iid, Uid: assets.Uid, Idt: assets.Idt}, nil
}

func GetManyAmountByUser(conn *gorm.DB, uid string) ([]Amount, error) {
	var assets []*schemas.Assets
	result := conn.Model(&schemas.Assets{}).Where("uid=?", uid).Find(&assets)
	if result.Error != nil || result.RowsAffected == 0 {
		return nil, errors.New(errs.ErrAssetsNotFound)
	}
	var resultList []Amount
	for _, element := range assets {
		resultList = append(resultList, Amount{Iid: element.Iid, Uid: element.Uid, Idt: element.Idt})
	}
	return resultList, nil
}

// 查看某個idea中所有的IDT數量
func GetAmountByIdea(conn *gorm.DB, iid uint32) ([]Amount, error) {
	var assets []*schemas.Assets
	result := conn.Model(&schemas.Assets{}).Where("iid=?", iid).Find(&assets)
	if result.Error != nil || result.RowsAffected == 0 {
		return nil, errors.New(errs.ErrAssetsNotFound)
	}
	var resultList []Amount
	for _, element := range assets {
		resultList = append(resultList, Amount{Iid: element.Iid, Uid: element.Uid, Idt: element.Idt})
	}
	return resultList, nil
}

// 將IDT轉出IDT銀行
//  1.  查看Assets中IDT餘額是否足夠&是否有鎖上
//  2.  如果足夠，扣除IDT餘額，更新IdtRecords
//  3.  同時更新Assets中的數量
func Withdraw(conn *gorm.DB, uid string, iid uint32, idt uint32, action uint32) (*common.Empty, error) {
	// check action
	switch action {
	case constants.Action_Withdraw: // Do Nothing
	default:
		return nil, errors.New(errs.ErrInvalidAction)
	}

	// check assets
	var assets schemas.Assets
	if err := conn.Where("uid=? and iid=? and is_locked=?", uid, iid, 0).Take(&assets).Error; err != nil {
		return nil, errors.New(errs.ErrAssetsNotFound)
	}

	// check idt
	if assets.Idt < idt {
		return nil, errors.New(errs.ErrNoEnoughIDT)
	}
	// create
	if err := conn.Create(
		&schemas.IdtRecords{
			Uid:    uid,
			Iid:    iid,
			Action: action,
			Idt:    idt,
		}).Error; err != nil {
		return nil, err
	}

	// update wallet
	assets.Idt -= idt
	if err := conn.Save(&assets).Error; err != nil {
		return nil, err
	}
	return nil, nil
}

// 將IDT存入IDT銀行
// 同時更新Assets中的數量
func Deposit(conn *gorm.DB, uid string, iid uint32, idt uint32, action uint32) (*common.Empty, error) {
	// check action
	switch action {
	case constants.Action_Deposit: // Do Nothing
	case constants.Action_Reward: // Do Nothing
	case constants.Action_Revenue: // Do Nothing
	default:
		return nil, errors.New(errs.ErrInvalidAction)
	}
	// check assets
	var assets schemas.Assets
	if err := conn.Where("uid=? and iid=? and is_locked=?", uid, iid, 0).Take(&assets).Error; err != nil {
		return nil, errors.New(errs.ErrAssetsNotFound)
	}
	// create
	if err := conn.Create(
		&schemas.IdtRecords{
			Uid:    uid,
			Iid:    iid,
			Action: action,
			Idt:    idt,
		}).Error; err != nil {
		return nil, err
	}
	// update wallet
	assets.Idt += idt
	if err := conn.Save(&assets).Error; err != nil {
		return nil, err
	}
	return nil, nil
}

// 將IDT進行鎖定
// 1. 檢查餘額是否足夠
// 2. 在idtbank新增一筆 action是lock的紀錄 回傳id
// 3. (這次先不會實踐)逾期沒有Confirm就會自動Unlock
func Lock(conn *gorm.DB, iid uint32, uid string, idt uint32) (uint32, error) {
	// check assets
	var assets schemas.Assets
	if err := conn.Where("uid=? and iid=? and is_locked=?", uid, iid, 0).Take(&assets).Error; err != nil {
		return 0, errors.New(errs.ErrAssetsNotFound)
	}
	// check idt
	if assets.Idt < idt {
		return 0, errors.New(errs.ErrNoEnoughIDT)
	}
	// update asset
	assets.Idt -= idt
	if err := conn.Save(&assets).Error; err != nil {
		return 0, err
	}
	// add to Idt_records table
	idtRecords := schemas.IdtRecords{
		Uid:    uid,
		Iid:    iid,
		Idt:    idt,
		Action: constants.Action_Lock,
	}
	if err := conn.Create(&idtRecords).Error; err != nil {
		return 0, err
	}
	return idtRecords.ID, nil
}

// 發生錯誤時，將IDT解鎖
// 1. 檢查bank是否存在此tid
// 2. 將tid從lock中刪除
func Unlock(conn *gorm.DB, lid uint32) (*common.Empty, error) {
	var lockedIdtRecords schemas.IdtRecords
	// check lock table
	if err := conn.Where("id=?", lid).Take(&lockedIdtRecords).Error; err != nil {
		return nil, errors.New(errs.ErrLocksNotFound)
	}
	// delete from lock
	if err := conn.Unscoped().Delete(&lockedIdtRecords).Error; err != nil {
		return nil, err
	}
	// update wallets
	if err := conn.Model(&schemas.Assets{}).
		Where("uid=? and iid=?", lockedIdtRecords.Uid, lockedIdtRecords.Iid).
		Update("idt", gorm.Expr("idt + ?", lockedIdtRecords.Idt)).Error; err != nil {
		return nil, err
	}
	return nil, nil

}

// 執行成功，將IDT轉為支付紀錄
// 1. 檢查是否存在id
// 2. 將此id的action改為Withdraw
func Confirm(conn *gorm.DB, lid uint32) (*common.Empty, error) {
	var lockedIdtRecords schemas.IdtRecords
	// check lock table
	if err := conn.Where("id=?", lid).Take(&lockedIdtRecords).Error; err != nil {
		return nil, errors.New(errs.ErrLocksNotFound)
	}
	// delete from lock
	if err := conn.Unscoped().Delete(&lockedIdtRecords).Error; err != nil {
		return nil, err
	}
	// add a IdtRecord
	if err := conn.Create(
		&schemas.IdtRecords{
			Uid:    lockedIdtRecords.Uid,
			Iid:    lockedIdtRecords.Iid,
			Action: constants.Action_Sell,
			Idt:    -lockedIdtRecords.Idt,
		}).Error; err != nil {
		return nil, err
	}
	return nil, nil
}
