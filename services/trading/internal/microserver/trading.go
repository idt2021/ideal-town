// package tradeserver dedicate to control the gRPC flow of trading service
package tradeserver

import (
	"context"
	bk "ideal-town/api/proto/v1/bank"
	common "ideal-town/api/proto/v1/common"
	gov "ideal-town/api/proto/v1/government"
	idtbank "ideal-town/api/proto/v1/idtbank"
	pb "ideal-town/api/proto/v1/trading"
	errs "ideal-town/pkg/errors"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/trading/internal/microserver/tradedb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// TradingServer handles requests and responses,
// and calls tradedb package to implement
// the database operations.
type TradingServer struct {
	Agent   *sqlmanager.Agent
	IDCBank bk.BankClient
	IDTBank idtbank.IdtAssetClient
	Govern  gov.GovernmentClient
	pb.UnimplementedTradingServer
}

// 掛賣IDT
func (t *TradingServer) WantSell(ctx context.Context, req *pb.SellRequest) (*common.Empty, error) {
	uid := req.GetUid()
	idt := req.GetIdt()
	iid := req.GetIid()
	price := req.GetPrice()

	// Start Transaction
	tx := t.Agent.Conn.Begin()
	{
		//  1. 嘗試扣除IDT (Asset)
		lockID, err := t.IDTBank.Lock(context.Background(), &idtbank.LockRequest{Iid: iid, Uid: uid, Idt: idt})
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		//  2. 新增一筆Orders紀錄
		_, err = db.WantSell(tx, uid, iid, idt, price)
		if err != nil {
			tx.Rollback()
			t.IDTBank.Unlock(context.Background(), &idtbank.IdInfo{Id: lockID.GetId()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}

	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	return &common.Empty{}, nil
}

// 買IDT
func (t *TradingServer) WantBuy(ctx context.Context, req *pb.BuyRequest) (*common.Empty, error) {
	uid := req.GetUid()
	iid := req.GetIid()
	idtAmount := req.GetIdt()
	idcPrice := req.GetIdc()

	var LockID *bk.LidInfo

	// Start Transaction
	tx := t.Agent.Conn.Begin()
	{
		//  1.  查看賣方掛單remian數量是否足夠
		//  2.  計算平均成交價格
		//  3.  如果滑價超過5%，取消交易
		updates, err := db.CheckEstimatePrice(t.Agent.Conn, iid, idtAmount, idcPrice)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		//  4.  嘗試鎖定IDC (Bank)
		LockID, err := t.IDCBank.Lock(context.Background(), &bk.LockRequest{Uid: uid, Idc: idcPrice})
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDC)
		}

		//  5.  更新賣方掛單的remain
		//  6.  新增買賣方交易紀錄
		//  7.  新增IDT資產(不使用請求，使用資料庫直接存取)
		tradeRecord, err := db.ExecuteBuy(tx, iid, uid, updates)
		if err != nil {
			tx.Rollback()
			t.IDCBank.Unlock(context.Background(), &bk.LidInfo{Lid: LockID.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		//  8.  請求government分配收入
		_, err = t.Govern.DistributeTradeRevenue(context.Background(), &gov.TradeRevenuesRequest{TradeRecord: tradeRecord})
		if err != nil {
			tx.Rollback()
			t.IDCBank.Unlock(context.Background(), &bk.LidInfo{Lid: LockID.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}

	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	//  9. Bank Confirm
	t.IDCBank.Confirm(context.Background(), &bk.LidInfo{Lid: LockID.GetLid()})

	return &common.Empty{}, nil
}

// 取消掛賣，返回掛賣資產
func (t *TradingServer) CancelSell(ctx context.Context, req *pb.CancelSellRequest) (*common.Empty, error) {
	if err := db.CancelSell(t.Agent.Conn, req.GetOid()); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

// 取得估計售價(不鎖定)
func (t *TradingServer) GetEstimatePrice(ctx context.Context, req *pb.EstPriceRequest) (*pb.EstPriceReply, error) {
	estPrice, err := db.GetEstimatePrice(t.Agent.Conn, req.GetIid(), req.GetAmount())
	if err != nil {
		return &pb.EstPriceReply{}, status.Error(codes.NotFound, err.Error())
	}
	return &pb.EstPriceReply{Price: estPrice}, nil
}

// 取得最新成交價
func (t *TradingServer) GetLatestPrice(ctx context.Context, req *pb.PriceRequest) (*pb.PriceReply, error) {
	latestPrice, err := db.GetLatestPrice(t.Agent.Conn, req.GetIid())
	if err != nil {
		return &pb.PriceReply{}, status.Error(codes.NotFound, err.Error())
	}
	return &pb.PriceReply{Price: latestPrice}, nil
}

func (t *TradingServer) GetMovingAvgPrice(ctx context.Context, req *pb.PriceRequest) (*pb.PriceReply, error) {
	return nil, status.Error(codes.Unimplemented, "not implement")
}
