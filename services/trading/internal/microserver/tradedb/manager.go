package tradedb

import (
	"errors"
	gov "ideal-town/api/proto/v1/government"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"math"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type match struct {
	Oid       uint32 // order oid
	SellerUid string // seller uid
	Remain    uint32 // remain idt
	Take      uint32 // Take idt
	Price     float32 // price for one unit
}

func sessionCloser(tx *gorm.DB) {
	if stmtManger, ok := tx.ConnPool.(*gorm.PreparedStmtDB); ok {
		stmtManger.Close()
	}
}


// ExecuteBuy execute the buying process of trading
//
// It will process below three steps:
// 		1. update remain of seller
// 		2. add transaction record
// 		3. add idt assets for the buyer
func ExecuteBuy(tx *gorm.DB, iid uint32, buyerUid string, updates []match) ([]*gov.TradeRecord, error) {
	var record []*gov.TradeRecord

	// create Idt assets record if not exist
	if err := tx.Clauses(clause.OnConflict{DoNothing: true}).
		Create(&schemas.Assets{Iid: iid, Uid: buyerUid}).Error; err != nil {
		return nil, err
	}

	for _, u := range updates {
		// Calculate total spencd
		totalPrice := u.Price * float32(u.Take)

		if u.Remain == 0 {
			// delete from orders
			if err := tx.Where("oid=?", u.Oid).Unscoped().Delete(&schemas.Orders{}).Error; err != nil {
				return nil, err
			}
		} else {
			// update order
			if err := tx.Model(&schemas.Orders{}).Where("oid=?", u.Oid).Update("remain", u.Remain).Error; err != nil {
				return nil, err
			}
		}

		// add buyer transation record
		if err := tx.Create(&schemas.Transactions{Uid: buyerUid, Iid: iid, Idc: totalPrice, Idt: u.Remain, Action: constants.Action_Buy}).Error; err != nil {
			return nil, err
		}
		// add seller transaction record
		if err := tx.Create(&schemas.Transactions{Uid: u.SellerUid, Iid: iid, Idc: totalPrice, Idt: u.Remain, Action: constants.Action_Sell}).Error; err != nil {
			return nil, err
		}
		// add buyer idt records
		if err := tx.Create(&schemas.IdtRecords{Iid: iid, Uid: buyerUid, Action: constants.Action_Deposit, Idt: u.Remain}).Error; err != nil {
			return nil, err
		}
		// update Idt assets snapshot of buyer
		if err := tx.Model(&schemas.Assets{}).Where("iid=? AND uid=?", iid, buyerUid).Update("idt", gorm.Expr("idt + ?", u.Remain)).Error; err != nil {
			return nil, err
		}
		// append trade records
		record = append(record, &gov.TradeRecord{Uid: u.SellerUid, Idc: totalPrice})
	}

	return record, nil
}

// WantSell execute the selling process by adding orders record
func WantSell(tx *gorm.DB, uid string, iid uint32, idt uint32, unitPrice float32) (uint32, error) {

	result := struct {
		Idt      uint32
		IsLocked bool
	}{}

	// check argument
	if unitPrice < 1 {
		return 0, errors.New(errs.ErrPriceTooLow)
	}

	// check if idt is enough
	if err := tx.Model(&schemas.Assets{}).
		Select("idt, is_locked").Where("iid=? AND uid=?", iid, uid).
		Scan(&result).Error; err != nil || result.Idt < idt {
		return 0, errors.New(errs.ErrNoEnoughIDT)
	}

	// Check if locked
	if result.IsLocked {
		return 0, errors.New(errs.ErrAssetsIsLocked)
	}

	// add idt records (negative)
	if err := tx.Create(&schemas.IdtRecords{Iid: iid, Uid: uid, Action: constants.Action_Sell, Idt: idt}).Error; err != nil {
		return 0, err
	}

	// update Idt assets snapshot
	if err := tx.Model(&schemas.Assets{}).Where("iid=? AND uid=?", iid, uid).Update("idt", gorm.Expr("idt - ?", idt)).Error; err != nil {
		return 0, err
	}

	order := schemas.Orders{
		Uid:    uid,
		Iid:    iid,
		Price:  unitPrice,
		Origin: idt,
		Remain: idt,
	}

	// create order records
	if err := tx.Create(&order).Error; err != nil {
		return 0, err
	}
	return order.Oid, nil
}

// CancelSell cancel an existing orders buy remove it from orders table.
func CancelSell(conn *gorm.DB, oid uint32) error {
	tx := conn.Begin()

	var order schemas.Orders

	// check if order exist
	if err := tx.Where("oid=?", oid).Take(&order).Error; err != nil {
		return errors.New(errs.ErrOrderNotFound)
	}

	// delete from orders table
	if err := tx.Where("oid=?", oid).Unscoped().Delete(&order).Error; err != nil {
		tx.Rollback()
		return err
	}

	// add idt assets
	if err := tx.Create(&schemas.IdtRecords{Uid: order.Uid, Iid: order.Iid, Action: constants.Action_CancelSell, Idt: order.Remain}).Error; err != nil {
		tx.Rollback()
		return err
	}

	// commit transaction
	if err := tx.Commit().Error; err != nil {
		return err
	}

	return nil
}

// CheckEstimatePrice estimate and check buying price via the request buying amount.
//
// If success, then return (average buy price, update_matches, nil)
//
// If the below conditions meets, then return (0, corresponding error)
// 	1. Solder IDT is not enough to handle request amount
// 	2. any database error
func CheckEstimatePrice(conn *gorm.DB, iid uint32, idtAmount uint32, idcPrice float32) ([]match, error) {
	var total uint32
	var orders []schemas.Orders

	// Start Caching
	tx := conn.Session(&gorm.Session{PrepareStmt: true})

	// Check if sum of remain is more than buying request [LOCKED]
	tx.Clauses(clause.Locking{Strength: "UPDATE"}).Model(&orders).Select("IFNULL(SUM(remain), 0)").Where("iid=?", iid).Scan(&total)
	if total < idtAmount {
		sessionCloser(tx)
		return nil, errors.New(errs.ErrNoEnoughIDT)
	}

	// Find all orders
	// Maybe consider: https://stackoverflow.com/questions/8691201/accumulated-sum-in-query
	rows, err := tx.Raw(`SELECT oid, uid, remain,  price, @accum := @accum + remain AS counter FROM 
	 (SELECT * FROM orders ORDER BY price, oid) AS T0,(SELECT @accum:=0) AS T1 HAVING @accum< ?`, idtAmount).Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Scan the result
	var updates []match
	var totalAmount, unuse uint32
	var totalPrice float32
	for rows.Next() {
		var m match
		rows.Scan(&m.Oid, &m.SellerUid, &m.Remain, &m.Price, &unuse)
		if m.Remain >= idtAmount {
			m.Take = idtAmount
			totalAmount += idtAmount
			totalPrice += float32(idtAmount) * m.Price
			m.Remain -= idtAmount
			updates = append(updates, m)
			break
		} else {
			m.Take = m.Remain
			totalAmount += m.Remain
			totalPrice += float32(m.Remain) * float32(m.Price)
			idtAmount -= m.Remain
			m.Remain = 0
			updates = append(updates, m)
		}
	}

	var estPrice = float64(totalPrice) / float64(totalAmount)

	// Check price slippage
	if math.Abs(estPrice-float64(idcPrice))/float64(idcPrice) > 0.05 {
		return nil, errors.New(errs.ErrPriceSlippage)
	}

	// Close session
	sessionCloser(tx)

	return updates, nil
}

// GetEstimantPrice calculates average buying price
func GetEstimatePrice(conn *gorm.DB, iid uint32, idtAmount uint32) (float32, error) {
	var total uint32
	var orders []schemas.Orders

	// Start Caching
	tx := conn.Session(&gorm.Session{PrepareStmt: true})

	// Check if sum of remain is more than buying request
	tx.Model(&orders).Select("SUM(remain)").Where("iid=?", iid).Scan(&total)
	if total < idtAmount {
		sessionCloser(tx)
		return 0, errors.New(errs.ErrNoEnoughIDT)
	}

	// Find all orders
	// Maybe consider: https://stackoverflow.com/questions/8691201/accumulated-sum-in-query
	rows, err := tx.Raw(`SELECT oid, uid, remain, price, @accum := @accum + remain AS counter FROM 
	 (SELECT * FROM orders ORDER BY price, oid) AS T0,(SELECT @accum:=0) AS T1 HAVING @accum< ?`, idtAmount).Rows()
	if err != nil {
		return 0, err
	}
	defer rows.Close()

	// Scan the result
	var totalAmount, unuse uint32
	var totalPrice float32
	for rows.Next() {
		var m match
		rows.Scan(&m.Oid, &m.SellerUid, &m.Remain, &m.Price, &unuse)
		if m.Remain >= idtAmount {
			totalAmount += idtAmount
			totalPrice += float32(idtAmount) * m.Price
			m.Remain -= idtAmount
			break
		} else {
			totalAmount += m.Remain
			totalPrice += float32(m.Remain) * float32(m.Price)
			idtAmount -= m.Remain
		}
	}
	// Close session
	sessionCloser(tx)
	return float32(totalPrice) / float32(totalAmount), nil
}

// GetLatestPrice fetch the lastest trading record in transaction table
func GetLatestPrice(conn *gorm.DB, iid uint32) (float32, error) {
	var trans schemas.Transactions
	if err := conn.Where("iid=?", iid).Last(&trans).Error; err != nil {
		return 0, errors.New(errs.ErrIdeaNotFound)
	}
	return float32(trans.Idc) / float32(trans.Idt), nil
}

func GetMovingAvgPrice(conn *gorm.DB, iid uint32) (float32, error) {
	return 0, nil
}
