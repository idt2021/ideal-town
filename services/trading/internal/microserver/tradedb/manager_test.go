package tradedb

import (
	"errors"
	gov "ideal-town/api/proto/v1/government"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Assets{}, &schemas.IdtRecords{}, &schemas.Orders{}, &schemas.Transactions{})
}
func TestWantSell(t *testing.T) {
	tests := []struct {
		name  string
		uid   string
		iid   uint32
		idt   uint32
		price uint32
		err   error
	}{
		{"[v] Valid", "a", 1, 10, 10, nil},
		{"[x] Price too low", "a", 1, 10, 0, errors.New(errs.ErrPriceTooLow)},
		{"[x] Lack of IDT", "a", 1, 10, 5, errors.New(errs.ErrNoEnoughIDT)},
		{"[x] Lack of IDT", "a", 2, 10, 5, errors.New(errs.ErrNoEnoughIDT)},
		{"[x] Locked", "b", 99, 10, 5, errors.New(errs.ErrAssetsIsLocked)},
	}
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 99, Uid: "b", Idt: 10, IsLocked: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := WantSell(agent.Conn, tt.uid, tt.iid, tt.idt, tt.price)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestExecuteBuy(t *testing.T) {
	tests := []struct {
		name    string
		uid     string
		iid     uint32
		updates []match
		record  []*gov.TradeRecord
		err     error
	}{
		{"[v] Valid", "buyer1", 1, []match{
			{Oid: 1, SellerUid: "a", Remain: 0, Take: 10, Price: 10},
			{Oid: 2, SellerUid: "b", Remain: 0, Take: 10, Price: 10},
			{Oid: 3, SellerUid: "a", Remain: 3, Take: 17, Price: 20},
		}, []*gov.TradeRecord{
			{Uid: "a", Idc: 100},
			{Uid: "b", Idc: 100},
			{Uid: "a", Idc: 340},
		}, nil},
	}
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 20})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 10})

	WantSell(agent.Conn, "a", 1, 10, 10)
	WantSell(agent.Conn, "a", 1, 10, 20)
	WantSell(agent.Conn, "b", 1, 10, 10)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, err := ExecuteBuy(agent.Conn, tt.iid, tt.uid, tt.updates)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, tt.record, r)
		})
	}
}

func TestCancelSell(t *testing.T) {
	tests := []struct {
		name string
		oid  uint32
		err  error
	}{
		{"[v] Valid", 0, nil},
		{"[v] Valid", 0, nil},
		{"[v] not found", 98564, errors.New(errs.ErrOrderNotFound)},
	}
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 10})

	oid, _ := WantSell(agent.Conn, "a", 1, 10, 10)
	tests[0].oid = oid
	oid, _ = WantSell(agent.Conn, "b", 1, 10, 10)
	tests[1].oid = oid

	ExecuteBuy(agent.Conn, 1, "buyer", []match{{Oid: 1, SellerUid: "a", Remain: 5, Take: 5, Price: 10}})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := CancelSell(agent.Conn, tt.oid)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestTradeAgent_CheckEstimatePrice(t *testing.T) {
	tests := []struct {
		name    string
		iid     uint32
		amount  uint32
		price   float32
		updates []match
		err     error
	}{
		{"[x] Lack of selling idt", 1, 50, 10, nil, errors.New(errs.ErrNoEnoughIDT)}, // want_amout:50 want_price:10
		{"[x] Price Slippage", 1, 30, 1, nil, errors.New(errs.ErrPriceSlippage)},     // want_amout:50 want_price:1
		{"[v] Valid", 1, 25, 10, []match{
			{SellerUid: "a", Remain: 0, Take: 10, Price: 10},
			{SellerUid: "b", Remain: 0, Take: 10, Price: 10},
			{SellerUid: "c", Remain: 5, Take: 5, Price: 10}}, nil}, // want_amout:25 want_price:10

	}
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "c", Idt: 10})

	oid, _ := WantSell(agent.Conn, "a", 1, 10, 10)
	tests[2].updates[0].Oid = oid
	oid, _ = WantSell(agent.Conn, "b", 1, 10, 10)
	tests[2].updates[1].Oid = oid
	oid, _ = WantSell(agent.Conn, "c", 1, 10, 10)
	tests[2].updates[2].Oid = oid

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			updates, err := CheckEstimatePrice(agent.Conn, tt.iid, tt.amount, tt.price)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, tt.updates, updates)
		})
	}
}

func TestTradeAgent_GetEstimatePrice(t *testing.T) {
	tests := []struct {
		name     string
		iid      uint32
		amount   uint32
		price    float32
		avgPrice float32
		err      error
	}{
		{"[x] Lack of selling idt", 1, 50, 10, 0, errors.New(errs.ErrNoEnoughIDT)}, // want_amout:50 want_price:10
		{"[v] Valid", 1, 25, 10, 10, nil},                                          // want_amout:30 want_price:10

	}
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "c", Idt: 10})

	WantSell(agent.Conn, "a", 1, 10, 10)
	WantSell(agent.Conn, "b", 1, 10, 10)
	WantSell(agent.Conn, "c", 1, 10, 10)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			estPrice, err := GetEstimatePrice(agent.Conn, tt.iid, tt.amount)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, tt.avgPrice, estPrice)
		})
	}
}

func TestTradeAgent_GetLatestPrice(t *testing.T) {
	tests := []struct {
		name    string
		iid     uint32
		want    float32
		wantErr error
	}{
		{"[v] Valid", 1, 20, nil},
		{"[x] Idea not found", 99999, 0, errors.New(errs.ErrIdeaNotFound)},
	}

	defer resetTable(agent)

	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "a", Idt: 10})
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "b", Idt: 10})

	WantSell(agent.Conn, "a", 1, 10, 10)
	WantSell(agent.Conn, "b", 1, 10, 20)

	// buyer buys 15 unit of idt
	ExecuteBuy(agent.Conn, 1, "buyer",
		[]match{
			{Oid: 1, SellerUid: "a", Remain: 0, Take: 10, Price: 10},
			{Oid: 2, SellerUid: "b", Remain: 5, Take: 5, Price: 20},
		})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetLatestPrice(agent.Conn, tt.iid)
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)
		})
	}
}
