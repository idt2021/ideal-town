package tradeserver

import (
	"context"
	"ideal-town/api/proto/v1/common"
	pb "ideal-town/api/proto/v1/trading"
	constants "ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	mock "ideal-town/services/trading/internal/microserver/mock"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const ConnByteSize = 1024 * 1024
const DialTarget = ""

var dbManager = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}
func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Assets{}, &schemas.IdtRecords{}, &schemas.Orders{}, &schemas.Transactions{})
}

// Create dialer with db connection
func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	pb.RegisterTradingServer(server, &TradingServer{
		IDCBank: &mock.BankClient{},
		IDTBank: &mock.IdtAssetClient{},
		Govern:  &mock.GovernmentClient{},
		Agent:   agent,
	})
	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()
	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, pb.TradingClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pb.NewTradingClient(conn)
	return ctx, conn, client
}

func TestTradingServer_WantSell(t *testing.T) {
	tests := []struct {
		name   string
		req    *pb.SellRequest
		resp   *common.Empty
		expErr error
	}{
		{"[v] Valid", &pb.SellRequest{Uid: "a", Iid: 1, Idt: 1, Price: 1}, &common.Empty{}, status.Error(codes.OK, "")},
		{"[x] Lack of IDT", &pb.SellRequest{Uid: "b", Iid: 2, Idt: 1000, Price: 1}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)},
		{"[x] Zero price", &pb.SellRequest{Uid: "c", Iid: 3, Idt: 10, Price: 0}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrPriceTooLow)},
		{"[x] Locked IDT", &pb.SellRequest{Uid: "d", Iid: 4, Idt: 10, Price: 0}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrPriceTooLow)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Assets{Uid: "a", Iid: 1, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "b", Iid: 2, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "c", Iid: 3, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "d", Iid: 4, Idt: 1, IsLocked: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.WantSell(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestTradingServer_CancelSell(t *testing.T) {
	tests := []struct {
		name   string
		req    *pb.CancelSellRequest
		resp   *common.Empty
		expErr error
	}{
		{"[v] Valid", &pb.CancelSellRequest{Oid: 1}, &common.Empty{}, nil},
		{"[x] oid not found", &pb.CancelSellRequest{Oid: 2}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrOrderNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Orders{Oid: 1, Remain: 10})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CancelSell(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestTradingServer_WantBuy(t *testing.T) {
	tests := []struct {
		name   string
		req    *pb.BuyRequest
		resp   *common.Empty
		expErr error
	}{
		{"[v] Valid1", &pb.BuyRequest{Uid: "b1", Iid: 1, Idt: 10, Idc: 1}, &common.Empty{}, status.Error(codes.OK, "")},
		{"[v] Valid2", &pb.BuyRequest{Uid: "b2", Iid: 2, Idt: 3, Idc: 5}, &common.Empty{}, status.Error(codes.OK, "")},
		{"[x] 滑價", &pb.BuyRequest{Uid: "b3", Iid: 2, Idt: 1, Idc: 10}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrPriceSlippage)},
		{"[x] Lack of idc", &pb.BuyRequest{Uid: "b4", Iid: 2, Idt: 1, Idc: 1000}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDC)},
		{"[x] Lack of selling idt", &pb.BuyRequest{Uid: "b5", Iid: 2, Idt: 9999, Idc: 1000}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Assets{Uid: "a", Iid: 1, Idt: 10})
	dbManager.Conn.Create(&schemas.Assets{Uid: "b", Iid: 2, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "c", Iid: 2, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "d", Iid: 2, Idt: 1})
	dbManager.Conn.Create(&schemas.Assets{Uid: "e", Iid: 2, Idt: 1})

	client.WantSell(ctx, &pb.SellRequest{Uid: "a", Iid: 1, Idt: 10, Price: 1})
	client.WantSell(ctx, &pb.SellRequest{Uid: "b", Iid: 2, Idt: 1, Price: 3})
	client.WantSell(ctx, &pb.SellRequest{Uid: "c", Iid: 2, Idt: 1, Price: 5})
	client.WantSell(ctx, &pb.SellRequest{Uid: "d", Iid: 2, Idt: 1, Price: 7})
	client.WantSell(ctx, &pb.SellRequest{Uid: "e", Iid: 2, Idt: 1, Price: 1000})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.WantBuy(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestTradingServer_GetLatestPrice(t *testing.T) {
	tests := []struct {
		name   string
		req    *pb.PriceRequest
		resp   *pb.PriceReply
		expErr error
	}{
		{"[v] Valid", &pb.PriceRequest{Iid: 888}, &pb.PriceReply{Price: 2.5}, status.Error(codes.OK, "")},
		{"[x] idea not found", &pb.PriceRequest{Iid: 5000}, &pb.PriceReply{}, status.Error(codes.NotFound, errs.ErrIdeaNotFound)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Transactions{Uid: "cb", Iid: 888, Idt: 4, Idc: 10, Action: constants.Action_Buy})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetLatestPrice(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.resp.GetPrice(), msg.GetPrice())
		})
	}
}
