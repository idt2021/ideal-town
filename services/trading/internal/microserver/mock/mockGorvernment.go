package mock

import (
	"context"
	"ideal-town/api/proto/v1/common"
	gov "ideal-town/api/proto/v1/government"

	"google.golang.org/grpc"
)

type GovernmentClient struct {
}
func (g *GovernmentClient) CreateTimer(ctx context.Context, in *gov.Timer, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (g *GovernmentClient) GetTimer(ctx context.Context, in *gov.Timer, opts ...grpc.CallOption) (*gov.Timer, error) {
	return nil, nil
}
func (g *GovernmentClient) UpdateTimer(ctx context.Context, in *gov.Timer, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (g *GovernmentClient) DeleteTimer(ctx context.Context, in *gov.Timer, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (g *GovernmentClient) GetIdeaState(ctx context.Context, in *gov.StateRequest, opts ...grpc.CallOption) (*gov.StateReply, error) {
	return nil, nil
}
func (g *GovernmentClient) GetSolState(ctx context.Context, in *gov.StateRequest, opts ...grpc.CallOption) (*gov.StateReply, error) {
	return nil, nil
}
func (g *GovernmentClient) GetPrState(ctx context.Context, in *gov.StateRequest, opts ...grpc.CallOption) (*gov.StateReply, error) {
	return nil, nil
}
func (g *GovernmentClient) GetFee(ctx context.Context, in *gov.FeeRequest, opts ...grpc.CallOption) (*gov.FeeReply, error) {
	return nil, nil
}
func (g *GovernmentClient) DistributeTradeRevenue(ctx context.Context, in *gov.TradeRevenuesRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (g *GovernmentClient) DistributePollReward(ctx context.Context, in *gov.PollRewardRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (g *GovernmentClient) DistributeChargeReward(ctx context.Context, opts ...grpc.CallOption) (gov.Government_DistributeChargeRewardClient, error) {
	return nil, nil
}
func (g *GovernmentClient) DistributeSubscribeReward(ctx context.Context, in *gov.SubscibeRewardRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
