package mock

import (
	"context"
	// "errors"
	"ideal-town/api/proto/v1/common"
	as "ideal-town/api/proto/v1/idtbank"
	errs "ideal-town/pkg/errors"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IdtAssetClient struct {
	as.UnimplementedIdtAssetServer
}

// 查看使用者在某個idea中擁有的IDT
func (c *IdtAssetClient) GetAmountByUser(ctx context.Context, in *as.AmountRequest, opts ...grpc.CallOption) (*as.AmountReply, error) {
	if in.Iid < 3 && in.Uid == "user" {
		return &as.AmountReply{Idt: 30}, nil
	}
	return &as.AmountReply{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)
}
func (c *IdtAssetClient) GetManyAmountByUser(ctx context.Context, in *as.AmountRequest, opts ...grpc.CallOption) (*as.AmountReplyList, error) {
	return nil, nil
}

// 查看某個idea中所有的IDT數量
func (c *IdtAssetClient) GetAmountByIdea(ctx context.Context, in *as.AmountRequest, opts ...grpc.CallOption) (*as.AmountReplyList, error) {
	var temp []*as.AmountReply
	if in.Iid < 3 {
		temp = append(temp, &as.AmountReply{Iid: in.GetIid(), Idt: 100})
		return &as.AmountReplyList{Amount: temp}, nil
	} 
	return &as.AmountReplyList{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)
}

// 將IDT轉出IDT銀行
//  1.  查看Assets中IDT餘額是否足夠&是否有鎖上
//  2.  如果足夠，扣除IDT餘額，更新IdtRecords
//  3.  同時更新Assets中的數量
func (c *IdtAssetClient) Withdraw(ctx context.Context, in *as.IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

// 將IDT存入IDT銀行
// 同時更新Assets中的數量
func (c *IdtAssetClient) Deposit(ctx context.Context, in *as.IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

// 將IDT進行鎖定
// 1. 檢查餘額是否足夠
// 2. 在idtbank新增一筆 action是lock的紀錄 回傳id
// 3. (這次先不會實踐)逾期沒有Confirm就會自動Unlock
func (c *IdtAssetClient) Lock(ctx context.Context, in *as.LockRequest, opts ...grpc.CallOption) (*as.IdInfo, error) {
	return nil, nil
}

// 發生錯誤時，將IDT解鎖
// 1. 檢查bank是否存在此tid
// 2. 將tid從lock中刪除
func (c *IdtAssetClient) Unlock(ctx context.Context, in *as.IdInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

// 執行成功，將IDT轉為支付紀錄
// 1. 檢查是否存在id
// 2. 將此id的action改為Withdraw
func (c *IdtAssetClient) Confirm(ctx context.Context, in *as.IdInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

func (c *IdtAssetClient) ChargeLock(ctx context.Context, in *as.ChargeLockRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return &common.Empty{}, status.Error(codes.OK, "")
}


