package mock

import (
	"context"
	common "ideal-town/api/proto/v1/common"
	pr "ideal-town/api/proto/v1/pr"
	errs "ideal-town/pkg/errors"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PRClient struct {
	pr.UnimplementedPullRequestServer
}

func (s *PRClient) CreatePR(ctx context.Context, in *pr.PRRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

func (s *PRClient) GetPRByIdea(ctx context.Context, in *pr.GetPR, opts ...grpc.CallOption) (*pr.PRList, error) {
	return nil, nil
}

func (s *PRClient) GetPRByUser(ctx context.Context, in *pr.GetPR, opts ...grpc.CallOption) (*pr.PRList, error) {
	return nil, nil
}

func (s *PRClient) GetPRByID(ctx context.Context, in *pr.GetPR, opts ...grpc.CallOption) (*pr.PR, error) {
	if in.GetRid() < 3 {
		return &pr.PR{Rid: in.GetRid(), Iid: in.GetRid(), Uid: "user", Link: "a gitlab link"}, status.Error(codes.OK, "")
	}
	return &pr.PR{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)
}

func (s *PRClient) DeletePR(ctx context.Context, in *pr.GetPR, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
