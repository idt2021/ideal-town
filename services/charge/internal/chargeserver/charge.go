package charge

import (
	"context"
	// errs "ideal-town/pkg/errors"
	charge "ideal-town/api/proto/v1/charge"
	common "ideal-town/api/proto/v1/common"
	as "ideal-town/api/proto/v1/idtbank"
	pr "ideal-town/api/proto/v1/pr"
	"ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/charge/internal/chargeserver/chargedb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ChargeServer struct {
	Agent   *sqlmanager.Agent
	Pr      pr.PullRequestClient
	IDTBank as.IdtAssetClient
	charge.UnimplementedChargeServer
}

func (s *ChargeServer) Charge(ctx context.Context, in *charge.ChargeRequest) (*common.Empty, error) {
	rid := in.GetRid()
	uid := in.GetUid()
	pr, err := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: rid})
	if err != nil {
		return &common.Empty{}, err
	}
	if _, err := s.IDTBank.GetAmountByUser(context.Background(), &as.AmountRequest{Iid: pr.GetIid(), Uid: uid}); err != nil {
		return &common.Empty{}, err
	}
	if err := db.ChargeLock(s.Agent.Conn, pr.GetIid(), uid); err != nil {
		return &common.Empty{}, err
	}
	if err := db.Charge(s.Agent.Conn, rid, uid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *ChargeServer) GetPRCharge(ctx context.Context, in *charge.ChargeRequest) (*charge.PRCharge, error) {
	rid := in.Rid
	pr, err := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: rid})
	if err != nil {
		return &charge.PRCharge{}, err
	}
	msg, _ := db.GetChargeList(s.Agent.Conn, rid, "")
	total, _ := s.IDTBank.GetAmountByIdea(context.Background(), &as.AmountRequest{Iid: pr.GetIid()})
	var sum uint32
	for _, element := range total.GetAmount() {
		sum += element.GetIdt()
	}
	var charged uint32
	for _, element := range msg {
		temp, _ := s.IDTBank.GetAmountByUser(context.Background(), &as.AmountRequest{Iid: pr.GetIid(), Uid: element.GetUid()})
		charged = temp.GetIdt()
	}
	return &charge.PRCharge{Rid: rid, Charge: float32(charged)/float32(sum)}, status.Error(codes.OK, "")
}

func (s *ChargeServer) GetUserCharge(ctx context.Context, in *charge.ChargeRequest) (*charge.PRCharge, error) {
	rid := in.Rid
	uid := in.Uid
	pr, err := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: rid})
	if err != nil {
		return &charge.PRCharge{}, err
	}
	if _, err := db.GetCharge(s.Agent.Conn, rid, uid); err != nil {
		return &charge.PRCharge{}, status.Error(codes.Aborted, err.Error())
	}
	total, _ := s.IDTBank.GetAmountByIdea(context.Background(), &as.AmountRequest{Iid: pr.GetIid()})
	charged, _ := s.IDTBank.GetAmountByUser(context.Background(), &as.AmountRequest{Iid: pr.GetIid(), Uid: uid})
	var sum uint32
	for _, element := range total.GetAmount() {
		sum += element.GetIdt()
	}
	return &charge.PRCharge{Rid: rid, Charge: float32(charged.GetIdt()) / float32(sum)}, status.Error(codes.OK, "")
}

func (s *ChargeServer) GetUserChargeList(ctx context.Context, in *charge.ChargeRequest) (*charge.PRChargeList, error) {
	var res []*charge.PRCharge
	uid := in.Uid
	msg, err := db.GetChargeList(s.Agent.Conn, 0, uid)
	if err != nil {
		return &charge.PRChargeList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		pr, _ := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: element.GetRid()})
		total, _ := s.IDTBank.GetAmountByIdea(context.Background(), &as.AmountRequest{Iid: pr.GetIid()})
		charged, _ := s.IDTBank.GetAmountByUser(context.Background(), &as.AmountRequest{Iid: pr.GetIid(), Uid: uid})
		var sum uint32
		for _, element := range total.GetAmount() {
			sum += element.GetIdt()
		}
		res = append(res, &charge.PRCharge{Rid: element.GetRid(), Charge: float32(charged.GetIdt()) / float32(sum)})
	}
	return &charge.PRChargeList{Prcharge: res}, status.Error(codes.OK, "")
}

func (s *ChargeServer) GetChargeList(ctx context.Context, in *charge.ChargeRequest) (*charge.ChargeList, error) {
	var res []*charge.Distribution
	rid := in.GetRid()
	pr, err := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: rid})
	if err != nil {
		return &charge.ChargeList{}, err
	}
	msg, err := db.GetChargeList(s.Agent.Conn, rid, "")
	if err != nil {
		return &charge.ChargeList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		charged, _ := s.IDTBank.GetAmountByUser(context.Background(), &as.AmountRequest{Iid: pr.GetIid(), Uid: element.GetUid()})

		res = append(res, &charge.Distribution{Uid: element.Uid, Idt: charged.GetIdt()})
	}
	return &charge.ChargeList{Distribution: res}, status.Error(codes.OK, "")
}

func (s *ChargeServer) UnCharge(ctx context.Context, in *charge.ChargeRequest) (*common.Empty, error) {
	rid := in.Rid
	uid := in.Uid
	pr, err := s.Pr.GetPRByID(context.Background(), &pr.GetPR{Rid: rid})
	if err != nil {
		return &common.Empty{}, err
	}
	if _, err := s.GetUserCharge(context.Background(), in); err != nil {
		return &common.Empty{}, err
	}
	if err := db.UnCharge(s.Agent.Conn, rid, uid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if err := db.ChargeUnLock(s.Agent.Conn, pr.GetIid(), uid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}
