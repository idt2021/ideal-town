package chargedb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/charge"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)
var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Charges{})
}
func TestCharge(t *testing.T) {
	tests := []struct {
		name    string
		rid    	uint32
		uid 	string
		expErr 	error
	}{
		{"[v] valid", 1, "user", nil}, 
		{"[x] user already charged", 1, "user", errors.New(errs.ErrAlreadyCharged)},
	}

	resetTable(agent)
	defer resetTable(agent)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Charge(agent.Conn, tt.rid, tt.uid)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestGetChargeList(t *testing.T) {
	tests := []struct {
		name    string
		rid	    uint32
		uid	 	string
		res    	int
		expErr 	error
	}{
		{"[v] valid", 1, "", 2, nil},
		{"[v] valid", 0, "user", 2, nil},
		{"[x] charge record not found(rid)", 10, "", 0, errors.New(errs.ErrChargeRecordNotFound)},
		{"[x] charge record not found(uid)", 0, "user10", 0, errors.New(errs.ErrChargeRecordNotFound)},
	}

	resetTable(agent)
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})
	agent.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user1"})
	agent.Conn.Create(&schemas.Charges{Rid: 2, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetChargeList(agent.Conn, tt.rid, tt.uid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg))
		})
	}
}

func TestGetCharge(t *testing.T) {
	tests := []struct {
		name    string
		rid    	uint32
		uid 	string
		exp    	*pb.ChargeRequest
		expErr 	error
	}{
		{"[v] valid", 1, "user", &pb.ChargeRequest{Rid: 1, Uid: "user"}, nil},
		{"[x] charge record not found(rid)", 10, "user", nil, errors.New(errs.ErrChargeRecordNotFound)},
		{"[x] charge record not found(uid)", 1, "user2", nil, errors.New(errs.ErrChargeRecordNotFound)},
	}

	resetTable(agent)
	defer resetTable(agent)

	agent.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetCharge(agent.Conn, tt.rid, tt.uid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.exp, msg)
		})
	}
}

func TestUnCharge(t *testing.T) {
	tests := []struct {
		name    string
		rid    	uint32
		uid 	string
		expErr 	error
	}{
		{"[v] valid", 1, "user", nil},
		{"[v] 亂刪", 10, "user10", nil},
	}

	defer resetTable(agent)

	agent.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := UnCharge(agent.Conn, tt.rid, tt.uid)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestChargeLock(t *testing.T) {
	tests := []struct {
		name	string
		iid		uint32
		uid 	string
		exprErr error
	}{
		{"[x] not enough IDT", 10, "user", errors.New(errs.ErrNoEnoughIDT)},
		{"[v] valid", 1, "user", nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "user", Idt: 30, IsLocked: 0})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ChargeLock(agent.Conn, tt.iid, tt.uid)
			assert.Equal(t, tt.exprErr, err)
		})
	}
}

func TestChargeUnLock(t *testing.T) {
	tests := []struct {
		name	string
		iid		uint32
		uid 	string
		exprErr error
	}{
		{"[x] not enough IDT", 10, "user", errors.New(errs.ErrNoEnoughIDT)},
		{"[v] valid", 1, "user", nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Assets{Iid: 1, Uid: "user", Idt: 30, IsLocked: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ChargeUnLock(agent.Conn, tt.iid, tt.uid)
			assert.Equal(t, tt.exprErr, err)
		})
	}
}