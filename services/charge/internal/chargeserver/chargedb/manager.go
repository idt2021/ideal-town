package chargedb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/charge"
	"ideal-town/pkg/schemas"

	"gorm.io/gorm"
)

func Charge(conn *gorm.DB, rid uint32, uid string) error {
	if err := conn.Create(&schemas.Charges{Rid: rid, Uid: uid}).Error; err != nil {
		return errors.New(errs.ErrAlreadyCharged)
	}
	return nil
}

func GetChargeList(conn *gorm.DB, rid uint32, uid string) ([]*pb.ChargeRequest, error) {
	var msg []schemas.Charges
	var res []*pb.ChargeRequest
	if rid != 0 {
		result := conn.Where(&schemas.Charges{Rid: rid}).Find(&msg)
		if err := result.Error; err != nil {
			return nil, errors.New(errs.ErrChargeRecordNotFound)
		}
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.ChargeRequest{
				Rid:    element.Rid,
				Uid:    element.Uid,
			})
		}
	} else {
		result := conn.Where(&schemas.Charges{Uid: uid}).Find(&msg)
		if err := result.Error; err != nil {
			return nil, errors.New(errs.ErrChargeRecordNotFound)
		}
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.ChargeRequest{
				Rid:    element.Rid,
				Uid:    element.Uid,
			})
		}
	}
	if len(res) == 0 {
		return res, errors.New(errs.ErrChargeRecordNotFound)
	}
	return res, nil
}

func GetCharge(conn *gorm.DB, rid uint32, uid string) (*pb.ChargeRequest, error) {
	var result pb.ChargeRequest
	var msg schemas.Charges
	if err := conn.Where(&schemas.Charges{Rid: rid, Uid: uid}).Take(&msg).Error; err != nil {
		return nil, errors.New(errs.ErrChargeRecordNotFound)
	}
	result = pb.ChargeRequest{Rid: msg.Rid, Uid: msg.Uid}
	return &result, nil
}

func UnCharge(conn *gorm.DB, rid uint32, uid string) error {
	if err := conn.Model(&schemas.Charges{}).Where("uid=?", uid).Delete(&schemas.Charges{Rid: rid}).Error; err != nil {
		return err
	}
	return nil
}

func ChargeLock(conn *gorm.DB, iid uint32, uid string) error {
	var data *schemas.Assets
	if err := conn.Model(&schemas.Assets{}).Where(&schemas.Assets{Iid: iid, Uid: uid}).Take(&data).Error; err!= nil {
		return errors.New(errs.ErrNoEnoughIDT)
	}
	data.IsLocked += 1
	conn.Save(&data)
	return nil 
}

func ChargeUnLock(conn *gorm.DB, iid uint32, uid string) error {
	var data *schemas.Assets
	if err := conn.Model(&schemas.Assets{}).Where(&schemas.Assets{Iid: iid, Uid: uid}).Take(&data).Error; err!= nil {
		return errors.New(errs.ErrNoEnoughIDT)
	}
	data.IsLocked -= 1 
	if err := conn.Save(&data).Error; err != nil {
		return err
	}
	return nil 
}