package charge

import (
	"context"
	charge "ideal-town/api/proto/v1/charge"
	common "ideal-town/api/proto/v1/common"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	mock "ideal-town/services/charge/internal/chargeserver/mock"

	"testing"

	"log"
	"net"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

const DialTarget = "bufnet"

var dbManager = initUnsafeChargeAgent()

func initUnsafeChargeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	// agent.Conn.Create(&schemas.Prs{Iid: 1, Uid: "user", Link: "a gitlab link"})
	// agent.Conn.Create(&schemas.Prs{Iid: 1, Uid: "user2", Link: "a gitlab link"})

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Charges{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)

	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	charge.RegisterChargeServer(server, &ChargeServer{
		Agent: agent,
		Pr: &mock.PRClient{},
		IDTBank: &mock.IdtAssetClient{},
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, charge.ChargeClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, 
        grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := charge.NewChargeClient(conn)
	return ctx, conn, client
}

func TestChargeServer_Charge(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    *common.Empty
		expErr error
	}{
		{"[x] pull request not found", &charge.ChargeRequest{Rid: 10}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[x] not enough IDT", &charge.ChargeRequest{Rid: 1, Uid: "user2"}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrNoEnoughIDT)},
		{"[v] valid", &charge.ChargeRequest{Rid: 1, Uid: "user"}, &common.Empty{}, status.Error(codes.OK, "")},
		{"[x] user has already charged this pull request", &charge.ChargeRequest{Rid: 1, Uid: "user"}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrAlreadyCharged)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Assets{Iid: 1, Uid: "user", Idt: 30, IsLocked: 0})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Charge(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestChargeServer_GetPRCharge(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    *charge.PRCharge
		expErr error
	}{
		{"[x] pull request not found", &charge.ChargeRequest{Rid: 10}, &charge.PRCharge{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[v] valid", &charge.ChargeRequest{Rid: 1}, &charge.PRCharge{Rid: 1, Charge: 0.3}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetPRCharge(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetCharge(), msg.GetCharge())
		})
	}
}

func TestChargeServer_GetUserCharge(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    *charge.PRCharge
		expErr error
	}{
		{"[x] pull request not found", &charge.ChargeRequest{Rid: 10}, &charge.PRCharge{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[x] user has no charge record of this pull request", &charge.ChargeRequest{Rid: 2, Uid: "user"}, &charge.PRCharge{}, status.Error(codes.Aborted, errs.ErrChargeRecordNotFound)},
		{"[v] valid", &charge.ChargeRequest{Rid: 1, Uid: "user"}, &charge.PRCharge{Rid: 1, Charge: 0.3}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetUserCharge(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetCharge(), msg.GetCharge())
		})
	}
}

func TestChargeServer_GetUserChargeList(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    int
		expErr error
	}{
		{"[x] user has no charge record", &charge.ChargeRequest{Uid: "user2"}, 0, status.Error(codes.Aborted, errs.ErrChargeRecordNotFound)},
		{"[v] valid", &charge.ChargeRequest{Uid: "user"}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Charges{Rid: 2, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetUserChargeList(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetPrcharge()))
		})
	}
}

func TestChargeServer_GetChargeList(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    int
		expErr error
	}{
		{"[x] pull request not found", &charge.ChargeRequest{Rid: 10}, 0, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[x] charge record not found", &charge.ChargeRequest{Rid: 2}, 0, status.Error(codes.Aborted, errs.ErrChargeRecordNotFound)},
		{"valid", &charge.ChargeRequest{Rid: 1}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user1"})
	
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetChargeList(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetDistribution()))
		})
	}
}

func TestChargeServer_UnCharge(t *testing.T) {
	tests := []struct {
		name   string
		req    *charge.ChargeRequest
		res    *common.Empty
		expErr error
	}{
		{"[x] pull request not found", &charge.ChargeRequest{Rid: 10}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[x] charge record not found", &charge.ChargeRequest{Rid: 1, Uid: "user2"}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrChargeRecordNotFound)},
		{"valid", &charge.ChargeRequest{Rid: 1, Uid: "user"}, &common.Empty{}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Charges{Rid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Assets{Iid: 1, Uid: "user", Idt: 30, IsLocked: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.UnCharge(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
