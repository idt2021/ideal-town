package main

import (
	"context"
	ibk "ideal-town/api/proto/v1/idtbank"
	"ideal-town/api/proto/v1/pr"
	gw "ideal-town/api/proto/v1/charge"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	ch "ideal-town/services/charge/internal/chargeserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":11011"
	httpServerEndpoint = ":12000"
)

func NewPrClient() pr.PullRequestClient {
	conn, err := grpc.Dial("localhost:7007", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
	}

	// defer conn.Close()

	client := pr.NewPullRequestClient(conn)

	return client
}

func NewIbkClient() ibk.IdtAssetClient {
	conn, err := grpc.Dial("localhost:6000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
	}

	// defer conn.Close()

	client := ibk.NewIdtAssetClient(conn)

	return client
}

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterChargeHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	s := grpc.NewServer()
	PrClient := NewPrClient()
	IbkClient := NewIbkClient()
	gw.RegisterChargeServer(s, &ch.ChargeServer{
		Agent: agent,
		Pr:  PrClient,
		IDTBank: IbkClient,
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}
