package ideaserver

import (
	"context"
	// "errors"
	common "ideal-town/api/proto/v1/common"
	idea "ideal-town/api/proto/v1/idea"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/idea/internal/microserver/ideadb"
	"log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IdeaServer struct {
	Agent *sqlmanager.Agent
	idea.UnimplementedIdeaServer
}

func (s *IdeaServer) CreateIdea(ctx context.Context, in *idea.IdeaInfo) (*common.Empty, error) {
	uid := in.GetUid()
	title := in.GetTitle()
	description := in.GetDescription()
	link := in.GetLink()
	gid := in.GetGid()
	log.Printf("Request received, create, uid: %s", in.GetUid())

	if title == "" {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrMustHaveTitle)
	}
	if gid == "" {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrMustHaveGitlabID)
	}
	if err := db.CreateIdea(s.Agent.Conn, uid, title, description, link, gid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdeaInfoByID(ctx context.Context, in *idea.IdeaRequest) (*idea.IdeaInfo, error) {
	iid := in.GetIid()

	log.Printf("Request received, get by iid: %d", in.GetIid())

	msg, err := db.GetIdea(s.Agent.Conn, iid)
	if err != nil {
		return &idea.IdeaInfo{}, status.Error(codes.Aborted, err.Error())
	}
	return &idea.IdeaInfo{Iid: msg.Iid, Uid: msg.Uid, Title: msg.Title, Description: msg.Description, Link: msg.Link, Gid: msg.Gid}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdea(ctx context.Context, in *common.Empty) (*idea.IdeaInfoList, error) {
	var res []*idea.IdeaInfo

	log.Printf("Request received get all")

	msg, err := db.GetIdeaList(s.Agent.Conn, "", 0)
	if err != nil && err.Error() != errs.ErrIdeaNotFound {
		return &idea.IdeaInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.IdeaInfo{
			Iid:         element.Iid,
			Uid:         element.Uid,
			Title:       element.Title,
			Description: element.Description,
			Link:        element.Link,
			Gid:         element.Gid})
	}
	return &idea.IdeaInfoList{IdeaInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdeaInfoByUser(ctx context.Context, in *idea.IdeaRequest) (*idea.IdeaInfoList, error) {
	var res []*idea.IdeaInfo
	uid := in.GetUid()

	log.Printf("Request received, get by uid: %s", in.GetUid())

	msg, err := db.GetIdeaList(s.Agent.Conn, uid, 0)
	if err != nil && err.Error() != errs.ErrIdeaNotFound {
		return &idea.IdeaInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.IdeaInfo{
			Iid:         element.Iid,
			Uid:         element.Uid,
			Title:       element.Title,
			Description: element.Description, 
			Link: element.Link,
			Gid: element.Gid})
	}
	return &idea.IdeaInfoList{IdeaInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdeaInfoByPhase(ctx context.Context, in *idea.IdeaRequest) (*idea.IdeaInfoList, error) {
	var res []*idea.IdeaInfo
	phase := in.GetPhase()

	log.Printf("Request received, get by phase: %d", in.GetPhase())

	msg, err := db.GetIdeaList(s.Agent.Conn, "", phase)
	if err != nil && err.Error() != errs.ErrIdeaNotFound {
		return &idea.IdeaInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.IdeaInfo{
			Iid: element.Iid,
			Uid:         element.Uid,
			Title:       element.Title,
			Description: element.Description,
			Link: element.Link,
			Gid: element.Gid})
	}
	return &idea.IdeaInfoList{IdeaInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdeaPhase(ctx context.Context, in *idea.PhaseRequest) (*idea.PhaseReply, error) {
	iid := in.GetId()

	log.Printf("Request received, get phase iid: %d", in.GetId())

	msg, err := db.GetIdea(s.Agent.Conn, iid)
	if err != nil {
		return &idea.PhaseReply{}, status.Error(codes.Aborted, err.Error())
	}
	return &idea.PhaseReply{Phase: msg.Phase}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetIdeaWinner(ctx context.Context, in *idea.PhaseRequest) (*idea.PhaseRequest, error) {
	iid := in.GetId()

	log.Printf("Request received, get winner iid: %d", in.GetId())

	msg, err := db.GetIdea(s.Agent.Conn, iid)
	if err != nil {
		return &idea.PhaseRequest{}, status.Error(codes.Aborted, err.Error())
	}
	if msg.Winner == 0 {
		return &idea.PhaseRequest{}, status.Error(codes.Aborted, errs.ErrStageNotReached)
	}
	return &idea.PhaseRequest{Id: msg.Winner}, status.Error(codes.OK, "")
}

func (s *IdeaServer) UpdateIdea(ctx context.Context, in *idea.UpdateRequest) (*common.Empty, error) {
	iid := in.GetId()
	title := in.GetTitle()
	description := in.GetDescription()
	link := in.GetLink()

	log.Printf("Request received, update iid: %d", in.GetId())

	msg, err := db.GetIdea(s.Agent.Conn, iid)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if title == "" {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrMustHaveTitle)
	}
	if title != msg.Title {
		if err := db.UpdateIdea(s.Agent.Conn, iid, 1, title); err != nil {
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}

	if description != msg.Description {
		if err := db.UpdateIdea(s.Agent.Conn, iid, 2, description); err != nil {
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}

	if link != msg.Link {
		if err := db.UpdateIdea(s.Agent.Conn, iid, 3, link); err != nil {
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}

	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *IdeaServer) CreateSolution(ctx context.Context, in *idea.SolutionInfo) (*common.Empty, error) {
	uid := in.GetUid()
	iid := in.GetIid()
	link := in.GetLink()
	gid := in.GetGid()
	log.Printf("Request received, create, iid: %d", in.GetIid())

	stage, err := s.GetIdeaPhase(context.Background(), &idea.PhaseRequest{Id: iid})
	if err != nil {
		return &common.Empty{}, err
	}
	if stage.GetPhase() != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrStageNotReached)
	}
	if gid == "" {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrMustHaveGitlabID)
	}
	if err := db.CreateSolution(s.Agent.Conn, iid, uid, link, gid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetSolutionInfoByID(ctx context.Context, in *idea.SolutionRequest) (*idea.SolutionInfo, error) {
	sid := in.GetSid()

	log.Printf("Request received, get by sid: %d", in.GetSid())

	msg, err := db.GetSolution(s.Agent.Conn, sid)
	if err != nil {
		return &idea.SolutionInfo{}, status.Error(codes.Aborted, err.Error())
	}
	return &idea.SolutionInfo{Sid: msg.Sid, Iid: msg.Iid, Uid: msg.Uid, Link: msg.Link, Gid: msg.Gid}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetSolution(ctx context.Context, in *common.Empty) (*idea.SolutionInfoList, error) {
	var res []*idea.SolutionInfo

	log.Printf("Request received, get all")

	msg, err := db.GetSolutionList(s.Agent.Conn, 0, "")
	if err != nil && err.Error() != errs.ErrSolutionNotFound {
		return &idea.SolutionInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.SolutionInfo{
			Sid: element.Sid,
			Iid: element.Iid,
			Uid:  element.Uid,
			Link: element.Link,
			Gid: element.Gid})
	}
	return &idea.SolutionInfoList{SolutionInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetSolutionInfoByUser(ctx context.Context, in *idea.SolutionRequest) (*idea.SolutionInfoList, error) {
	var res []*idea.SolutionInfo
	uid := in.GetUid()

	log.Printf("Request received, get by uid: %s", in.GetUid())

	msg, err := db.GetSolutionList(s.Agent.Conn, 0, uid)
	if err != nil  && err.Error() != errs.ErrSolutionNotFound {
		return &idea.SolutionInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.SolutionInfo{
			Sid: element.Sid,
			Iid: element.Iid,
			Uid:  element.Uid,
			Link: element.Link,
			Gid: element.Gid})
	}
	return &idea.SolutionInfoList{SolutionInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetSolutionInfoByIdea(ctx context.Context, in *idea.SolutionRequest) (*idea.SolutionInfoList, error) {
	var res []*idea.SolutionInfo
	iid := in.GetIid()

	log.Printf("Request received, get by iid: %d", in.GetIid())

	msg, err := db.GetSolutionList(s.Agent.Conn, iid, "")
	if err != nil && err.Error() != errs.ErrSolutionNotFound {
		return &idea.SolutionInfoList{}, status.Error(codes.Aborted, err.Error())
	}
	for _, element := range msg {
		res = append(res, &idea.SolutionInfo{
			Sid: element.Sid,
			Iid: element.Iid,
			Uid:  element.Uid,
			Link: element.Link,
			Gid: element.Gid})
	}
	return &idea.SolutionInfoList{SolutionInfo: res}, status.Error(codes.OK, "")
}

func (s *IdeaServer) GetSolutionPhase(ctx context.Context, in *idea.PhaseRequest) (*idea.PhaseReply, error) {
	sid := in.GetId()

	log.Printf("Request received, get phase sid: %d", in.GetId())

	msg, err := db.GetSolution(s.Agent.Conn, sid)
	if err != nil {
		return &idea.PhaseReply{}, status.Error(codes.Aborted, err.Error())
	}
	iid := msg.GetIid()
	res, _ := s.GetIdeaPhase(context.Background(), &idea.PhaseRequest{Id: iid})
	return res, status.Error(codes.OK, "")
}

func (s *IdeaServer) UpdateSolution(ctx context.Context, in *idea.UpdateRequest) (*common.Empty, error) {
	sid := in.GetId()

	log.Printf("Request received, update sid: %d", in.GetId())

	link := in.GetLink()
	if err := db.UpdateSolution(s.Agent.Conn, sid, link); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}
