package ideaserver

import (
	"context"
	errs "ideal-town/pkg/errors"
	idea "ideal-town/api/proto/v1/idea"
	"ideal-town/pkg/schemas"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	"testing"

	"log"
	"net"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const DialTarget = "bufnet"

var dbManager = initUnsafeIdeaAgent()

func initUnsafeIdeaAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Ideas{}, &schemas.Solutions{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)

	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})

	idea.RegisterIdeaServer(server, &IdeaServer{
		Agent: agent,
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, idea.IdeaClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget,
		grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := idea.NewIdeaClient(conn)
	return ctx, conn, client
}

func TestIdeaServer_CreateIdea(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.IdeaInfo
		expErr error
	}{
		{"[v] valid", &idea.IdeaInfo{Uid: "user", Title: "title", Description: "description", Link: "a gitlab link"}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CreateIdea(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestIdeaServer_GetIdeaInfoByID(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.IdeaRequest
		res    *idea.IdeaInfo
		expErr error
	}{
		{"[x] idea not found", &idea.IdeaRequest{Iid: 10}, &idea.IdeaInfo{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[v] valid", &idea.IdeaRequest{Iid: 1}, &idea.IdeaInfo{Iid: 1}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaInfoByID(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetIid(), msg.GetIid())
		})
	}
}

func TestIdeaServer_GetIdeaInfoByUser(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.IdeaRequest
		res    int
		expErr error
	}{
		{"[x] idea not found", &idea.IdeaRequest{Uid: "user2"}, 0, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[v] valid", &idea.IdeaRequest{Uid: "user"}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaInfoByUser(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetIdeaInfo()))
		})
	}
}
func TestIdeaServer_GetIdeaInfoByPhase(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.IdeaRequest
		res    int
		expErr error
	}{
		{"[x] idea not found", &idea.IdeaRequest{Phase: 2}, 0, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[v] valid", &idea.IdeaRequest{Phase: 1}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaInfoByPhase(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetIdeaInfo()))
		})
	}
}
func TestIdeaServer_GetIdeaPhase(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.PhaseRequest
		res    *idea.PhaseReply
		expErr error
	}{
		{"[x] idea not found", &idea.PhaseRequest{Id: 10}, &idea.PhaseReply{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[v] valid", &idea.PhaseRequest{Id: 1}, &idea.PhaseReply{Phase: 1}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaPhase(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetPhase(), msg.GetPhase())
		})
	}
}
func TestIdeaServer_GetIdeaWinner(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.PhaseRequest
		res    *idea.PhaseRequest
		expErr error
	}{
		{"[x] idea not found", &idea.PhaseRequest{Id: 10}, &idea.PhaseRequest{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] idea at wrong stage", &idea.PhaseRequest{Id: 1}, &idea.PhaseRequest{}, status.Error(codes.Aborted, errs.ErrStageNotReached)},
		{"[v] valid", &idea.PhaseRequest{Id: 3}, &idea.PhaseRequest{Id: 1}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaWinner(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetId(), msg.GetId())
		})
	}
}
func TestIdeaServer_UpdateIdea(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.UpdateRequest
		expErr error
	}{
		{"[x] idea not found", &idea.UpdateRequest{Id: 10}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] title can not be empty string", &idea.UpdateRequest{Id: 1, Title: "", Description: "new", Link: "new"}, status.Error(codes.Aborted, errs.ErrMustHaveTitle)},
		{"[v] valid", &idea.UpdateRequest{Id: 1, Title: "new", Description: "new", Link: "new"}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.UpdateIdea(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
func TestIdeaServer_CreateSolution(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.SolutionInfo
		expErr error
	}{
		{"[x] idea not found", &idea.SolutionInfo{Iid: 10, Uid: "user", Link: "a gitlab link"}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] idea at wrong stage", &idea.SolutionInfo{Iid: 3, Uid: "user", Link: "a gitlab link"}, status.Error(codes.Aborted, errs.ErrStageNotReached)},
		{"[v] valid", &idea.SolutionInfo{Iid: 1, Uid: "user", Link: "a gitlab link"}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})


	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CreateSolution(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestIdeaServer_GetSolutionInfoByID(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.SolutionRequest
		res    *idea.SolutionInfo
		expErr error
	}{
		{"[x] solution not found", &idea.SolutionRequest{Sid: 10}, &idea.SolutionInfo{}, status.Error(codes.Aborted, errs.ErrSolutionNotFound)},
		{"[v] valid", &idea.SolutionRequest{Sid: 1}, &idea.SolutionInfo{Sid: 1}, status.Error(codes.OK, "")},	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})



	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetSolutionInfoByID(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetSid(), msg.GetSid())
		})
	}
}

func TestIdeaServer_GetSolutionInfoByUser(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.SolutionRequest
		res    int
		expErr error
	}{
		{"[x] solution not found", &idea.SolutionRequest{Uid: "user2"}, 0, status.Error(codes.Aborted, errs.ErrSolutionNotFound)},
		{"[v] valid", &idea.SolutionRequest{Uid: "user"}, 2, status.Error(codes.OK, "")},

	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})


	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetSolutionInfoByUser(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetSolutionInfo()))
		})
	}
}

func TestIdeaServer_GetSolutionInfoByIdea(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.SolutionRequest
		res    int
		expErr error
	}{
		{"[x] solution not found", &idea.SolutionRequest{Iid: 2}, 0, status.Error(codes.Aborted, errs.ErrSolutionNotFound)},
		{"[v] valid", &idea.SolutionRequest{Iid: 1}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})


	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetSolutionInfoByIdea(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetSolutionInfo()))
		})
	}
}

func TestIdeaServer_GetSolutionPhase(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.PhaseRequest
		res    *idea.PhaseReply
		expErr error
	}{
		{"[x] solution not found", &idea.PhaseRequest{Id: 10}, &idea.PhaseReply{}, status.Error(codes.Aborted, errs.ErrSolutionNotFound)},
		{"[v] valid", &idea.PhaseRequest{Id: 1}, &idea.PhaseReply{Phase: 1}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})


	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetSolutionPhase(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetPhase(), msg.GetPhase())
		})
	}
}
func TestIdeaServer_UpdateSolution(t *testing.T) {
	tests := []struct {
		name   string
		req    *idea.UpdateRequest
		expErr error
	}{
		{"[v] valid", &idea.UpdateRequest{Id: 1, Title: "new", Description: "new", Link: "new"}, status.Error(codes.OK, "")},
		{"[x] solution not found", &idea.UpdateRequest{Id: 10}, status.Error(codes.Aborted, errs.ErrSolutionNotFound)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Ideas{Iid: 3, Uid: "user1", Phase: 3, Title: "title", Description: "description", Link: "a gitlab link", WinnerSid: 1})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})


	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.UpdateSolution(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
