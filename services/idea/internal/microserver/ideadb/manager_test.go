package ideadb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/idea"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Ideas{}, &schemas.Solutions{})
}

func TestCreateIdea(t *testing.T) {
	tests := []struct {
		name        string
		uid         string
		title       string
		description string
		link        string
		expErr      error
	}{
		{"[v] valid", "user", "title", "description", "a gitlab link", nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := CreateIdea(agent.Conn, tt.uid, tt.title, tt.description, tt.link)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestGetIdea(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		res    *IdeaDetail
		expErr error
	}{
		{"[v] valid", 1, &IdeaDetail{Iid: 1, Uid: "user", Phase: 1, Winner: 0, Title: "title", Description: "description", Link: "a gitlab link"}, nil},
		{"[x] idea not found", 10, &IdeaDetail{}, errors.New(errs.ErrIdeaNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetIdea(agent.Conn, tt.iid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.Iid, msg.Iid)
		})
	}
}

func TestGetIdeaList(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		phase  uint32
		res    int
		expErr error
	}{
		{"[v] valid", "user", 0, 2, nil},
		{"[v] valid", "", 1, 2, nil},
		{"[x] idea not found(uid)", "user1", 0, 0, errors.New(errs.ErrIdeaNotFound)},
		{"[x] idea not found(phase", "", 2, 0, errors.New(errs.ErrIdeaNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetIdeaList(agent.Conn, tt.uid, tt.phase)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg))
		})
	}
}

func TestUpdateIdea(t *testing.T) {
	tests := []struct {
		name        string
		iid         uint32
		field 		uint32
		content     string
		expErr      error
	}{
		{"[v] valid", 1, 1, "new", nil},
		{"[v] valid", 1, 2, "new", nil},
		{"[v] valid", 1, 3, "new", nil},
		{"[x] idea not found", 10, 0, "", errors.New(errs.ErrIdeaNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := UpdateIdea(agent.Conn, tt.iid, tt.field, tt.content)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestCreateSolution(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		link   string
		expErr error
	}{
		{"[v] valid", 2, "user", "a gitlab link", nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := CreateSolution(agent.Conn, tt.iid, tt.uid, tt.link)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestGetSolution(t *testing.T) {
	tests := []struct {
		name   string
		sid    uint32
		res    *pb.SolutionInfo
		expErr error
	}{
		{"[v] valid", 1, &pb.SolutionInfo{Sid: 1, Iid: 1, Uid: "user"}, nil},
		{"[x] solution not found", 10, &pb.SolutionInfo{}, errors.New(errs.ErrSolutionNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetSolution(agent.Conn, tt.sid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetSid(), msg.GetSid())
		})
	}
}

func TestGetSolutionList(t *testing.T) {
	tests := []struct {
		name   string
		iid    uint32
		uid    string
		res    int
		expErr error
	}{
		{"[v] valid", 1, "", 2, nil},
		{"[v] valid", 0, "user", 2, nil},
		{"[x] solution not found(iid)", 10, "", 0, errors.New(errs.ErrSolutionNotFound)},
		{"[x] solution not found(uid)", 0, "user2", 0, errors.New(errs.ErrSolutionNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetSolutionList(agent.Conn, tt.iid, tt.uid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg))
		})
	}
}

func TestUpdateSolution(t *testing.T) {
	tests := []struct {
		name   string
		sid    uint32
		link   string
		expErr error
	}{
		{"[v] valid", 1, "new", nil},
		{"[x] solution not found", 10, "", errors.New(errs.ErrSolutionNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Ideas{Iid: 1, Uid: "user", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Ideas{Iid: 2, Uid: "user1", Phase: 1, Title: "title", Description: "description", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := UpdateSolution(agent.Conn, tt.sid, tt.link)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
