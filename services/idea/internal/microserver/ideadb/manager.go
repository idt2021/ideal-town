package ideadb

import (
	"errors"
	pb "ideal-town/api/proto/v1/idea"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"

	"gorm.io/gorm"
)

type IdeaDetail struct {
	Iid         uint32
	Uid         string
	Phase       uint32
	Winner      uint32
	Title       string
	Description string
	Link        string
	Gid         string
}

func CreateIdea(conn *gorm.DB, uid string, title string, description string, link string, gid string) error {
	if err := conn.Create(&schemas.Ideas{Uid: uid, Title: title, Description: description, Link: link, Phase: 1, Gid: gid}).Error; err != nil {
		return err
	}
	return nil
}

func GetIdea(conn *gorm.DB, iid uint32) (*IdeaDetail, error) {
	var msg schemas.Ideas
	err := conn.Model(&schemas.Ideas{}).Where("iid=?", iid).Take(&msg).Error
	if err != nil {
		return &IdeaDetail{}, errors.New(errs.ErrIdeaNotFound)
	}
	return &IdeaDetail{Iid: msg.Iid,
		Uid:         msg.Uid,
		Phase:       msg.Phase,
		Winner:      msg.WinnerSid,
		Title:       msg.Title,
		Description: msg.Description,
		Link:        msg.Link,
		Gid:         msg.Gid}, nil
}

func GetIdeaList(conn *gorm.DB, uid string, phase uint32) ([]*IdeaDetail, error) {
	var msg []*schemas.Ideas
	var res []*IdeaDetail
	if uid != "" {
		err := conn.Model(&schemas.Ideas{}).Where("uid=?", uid).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrIdeaNotFound)
		}
	} else if phase != 0 {
		err := conn.Model(&schemas.Ideas{}).Where("phase=?", phase).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrIdeaNotFound)
		}
	} else {
		err := conn.Model(&schemas.Ideas{}).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrIdeaNotFound)
		}	
	}


	for _, element := range msg {
		res = append(res, &IdeaDetail{
			Iid:         element.Iid,
			Uid:         element.Uid,
			Phase:       element.Phase,
			Winner:      element.WinnerSid,
			Title:       element.Title,
			Description: element.Description,
			Link:        element.Link,
			Gid:         element.Gid})
	}
	return res, nil
}

func UpdateIdea(conn *gorm.DB, iid uint32, field uint32, content string) error {
	var temp *schemas.Ideas
	if err := conn.Model(&schemas.Ideas{}).Where("iid=?", iid).Take(&temp).Error; err != nil {
		return errors.New(errs.ErrIdeaNotFound)
	}

	switch field {
	case 1:
		if err := conn.Model(&schemas.Ideas{}).Where("iid=?", iid).
			Select("title").
			Updates(&schemas.Ideas{Title: content}).Error; err != nil {
			return err
		}
	case 2:
		if err := conn.Model(&schemas.Ideas{}).Where("iid=?", iid).
			Select("description").
			Updates(&schemas.Ideas{Description: content}).Error; err != nil {
			return err
		}
	case 3:
		if err := conn.Model(&schemas.Ideas{}).Where("iid=?", iid).
			Select("link").
			Updates(&schemas.Ideas{Link: content}).Error; err != nil {
			return err
		}
	}
	return nil
}

func CreateSolution(conn *gorm.DB, iid uint32, uid string, link string, gid string) error {
	if err := conn.Create(&schemas.Solutions{Iid: iid, Uid: uid, Link: link, Gid: gid}).Error; err != nil {
		return err
	}
	return nil
}

func GetSolution(conn *gorm.DB, sid uint32) (*pb.SolutionInfo, error) {
	var msg pb.SolutionInfo
	err := conn.Model(&schemas.Solutions{}).Where("sid=?", sid).Take(&msg).Error
	if err != nil {
		return &pb.SolutionInfo{}, errors.New(errs.ErrSolutionNotFound)
	}
	return &msg, nil
}

func GetSolutionList(conn *gorm.DB, iid uint32, uid string) ([]*pb.SolutionInfo, error) {
	var msg []schemas.Solutions
	var res []*pb.SolutionInfo
	if iid != 0 {
		err := conn.Model(&schemas.Solutions{}).Where("iid=?", iid).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrSolutionNotFound)
		}
	} else if uid != "" {
		err := conn.Model(&schemas.Solutions{}).Where("uid=?", uid).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrSolutionNotFound)
		}
	} else {
		err := conn.Model(&schemas.Solutions{}).Find(&msg).Error
		if err != nil || len(msg) == 0 {
			return res, errors.New(errs.ErrSolutionNotFound)
		}
	}


	for _, element := range msg {
		res = append(res, &pb.SolutionInfo{Sid: element.Sid,
			Iid:  element.Iid,
			Uid:  element.Uid,
			Link: element.Link,
			Gid:  element.Gid})
	}
	return res, nil
}

func UpdateSolution(conn *gorm.DB, sid uint32, link string) error {
	var temp *schemas.Solutions
	if err := conn.Model(&schemas.Solutions{}).Where("sid=?", sid).Take(&temp).Error; err != nil {
		return errors.New(errs.ErrSolutionNotFound)
	}

	if err := conn.Model(&schemas.Solutions{}).Where("sid=?", sid).
		Select("link").
		Updates(&schemas.Solutions{Link: link}).Error; err != nil {
		return err
	}
	return nil
}
