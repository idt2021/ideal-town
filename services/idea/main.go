package main

import (
	"context"
	gw "ideal-town/api/proto/v1/idea"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	idea "ideal-town/services/idea/internal/microserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":8080"
	httpServerEndpoint = ":8890"
)

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	// mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    //     w.Header().Set("Content-Type", "application/json")
    //     w.Write([]byte("{\"hello\": \"world\"}"))
    // })
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterIdeaHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	s := grpc.NewServer()
	gw.RegisterIdeaServer(s, &idea.IdeaServer{
		Agent: agent,
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}