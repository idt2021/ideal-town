package governdb

import (
	"errors"
	pb "ideal-town/api/proto/v1/government"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeGovernAgent()

func initUnsafeGovernAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "government",
	})
	resetTable(agent)

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Timer{})
}
func TestCreateTimer(t *testing.T) {
	tests := []struct {
		name     string
		category uint32
		id       uint32
		deadline time.Time
		expErr   error
	}{
		{"[v] valid", 1, 1, time.Now().AddDate(0, 0, 1), nil},
		{"[x] duplicated", 1, 1, time.Now().AddDate(0, 0, 1), errors.New(errs.ErrTimerAlreadyExist)},
	}

	defer resetTable(agent)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := CreateTimer(agent.Conn, tt.category, tt.id, tt.deadline)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestGetTimer(t *testing.T) {
	tests := []struct {
		name     string
		category uint32
		id       uint32
		res      *pb.Timer
		expErr   error
	}{
		{"[x] timer not found", 2, 2, &pb.Timer{}, errors.New(errs.ErrTimerNotFound)},
		{"[v] valid", 1, 1, &pb.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0, 0, 1).Format("2006-01-02 15:04")}, nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0, 0, 1)})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetTimer(agent.Conn, tt.category, tt.id)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, msg)
		})
	}
}

func TestUpdateTimer(t *testing.T) {
	tests := []struct {
		name     string
		category uint32
		id       uint32
		deadline time.Time
		expErr   error
	}{
		{"[x] timer not found", 2, 2, time.Now().AddDate(0, 1, 0), errors.New(errs.ErrTimerNotFound)},
		{"[v] valid", 1, 1, time.Now().AddDate(0, 1, 0), nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0, 0, 1)})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := UpdateTimer(agent.Conn, tt.category, tt.id, tt.deadline)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestDeleteTimer(t *testing.T) {
	tests := []struct {
		name     string
		category uint32
		id       uint32
		expErr   error
	}{
		{"[v] timer not found", 2, 2, nil},
		{"[v] valid", 1, 1, nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0, 0, 1)})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := DeleteTimer(agent.Conn, tt.category , tt.id)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
