package governdb

import (
	errs "ideal-town/pkg/errors"
	"errors"
	"time"
	pb "ideal-town/api/proto/v1/government"
	"ideal-town/pkg/schemas"

	"gorm.io/gorm"
)

func CreateTimer(conn *gorm.DB, category uint32, id uint32, deadline time.Time) error {
	if err := conn.Create(&schemas.Timer{Category: category, Id: id, Deadline: deadline}).Error; err != nil {
		return errors.New(errs.ErrTimerAlreadyExist)
	}
	return nil
}

func GetTimer(conn *gorm.DB, category uint32, id uint32) (*pb.Timer, error) {
	var msg *schemas.Timer
	if err := conn.Model(&schemas.Timer{}).Where(&schemas.Timer{Category: category, Id: id}).Take(&msg).Error; err != nil {
		return &pb.Timer{}, errors.New(errs.ErrTimerNotFound)
	}
	return &pb.Timer{Category: category, Id: id, Deadline: msg.Deadline.Format("2006-01-02 15:04")}, nil
}

func UpdateTimer(conn *gorm.DB, category uint32, id uint32, deadline time.Time) error {
	var msg *schemas.Timer
	if err := conn.Model(&schemas.Timer{}).Where(&schemas.Timer{Category: category, Id: id}).Take(&msg).Error; err != nil {
		return errors.New(errs.ErrTimerNotFound)
	}
	msg.Deadline = deadline
	if err := conn.Save(&msg).Error; err != nil {
		return err
	}
	return nil
}

func DeleteTimer(conn *gorm.DB, category uint32, id uint32) error {
	if err := conn.Model(&schemas.Timer{}).Where("id=?", id).Delete(&schemas.Timer{Category: category}).Error; err != nil {
		return err
	}
	return nil
}