package govern

import (
	"context"
	common "ideal-town/api/proto/v1/common"
	gov "ideal-town/api/proto/v1/government"
	// errs "ideal-town/pkg/errors"
	"ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/govern/internal/microserver/governdb"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GovServer struct{
	Agent *sqlmanager.Agent
	gov.UnimplementedGovernmentServer
}

func (s *GovServer) CreateTimer(ctx context.Context, in *gov.Timer) (*common.Empty, error){
	category := in.GetCategory()
	id := in.GetId()
	deadline, _ := time.Parse("2006-01-02 15:04", in.GetDeadline())
	if err := db.CreateTimer(s.Agent.Conn, category, id, deadline); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}
func (s *GovServer) GetTimer(ctx context.Context, in *gov.Timer) (*gov.Timer, error){
	msg, err := db.GetTimer(s.Agent.Conn, in.GetCategory(), in.GetId())
	if err != nil {
		return &gov.Timer{}, status.Error(codes.Aborted, err.Error())
	}
	return msg, status.Error(codes.OK, "")
}
func (s *GovServer) UpdateTimer(ctx context.Context, in *gov.Timer) (*common.Empty, error){
	deadline, _ := time.Parse("2006-01-02 15:04", in.GetDeadline())
	if err := db.UpdateTimer(s.Agent.Conn, in.GetCategory(), in.GetId(), deadline); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}
func (s *GovServer) DeleteTimer(ctx context.Context, in *gov.Timer) (*common.Empty, error){
	if _, err := s.GetTimer(context.Background(), in); err != nil {
		return &common.Empty{}, err
	}
	if err := db.DeleteTimer(s.Agent.Conn, in.GetCategory(), in.GetId()); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}
func (s *GovServer) GetIdeaState(ctx context.Context, in *gov.StateRequest) (*gov.StateReply, error) {
	return nil, nil
}
func (s *GovServer) GetSolState(ctx context.Context, in *gov.StateRequest) (*gov.StateReply, error) {
	return nil, nil
}
func (s *GovServer) GetPrState(ctx context.Context, in *gov.StateRequest) (*gov.StateReply, error) {
	return nil, nil
}
func (s *GovServer) GetFee(ctx context.Context, in *gov.FeeRequest) (*gov.FeeReply, error) {
	return nil, nil
}
func (s *GovServer) DistributeTradeRevenue(ctx context.Context, in *gov.TradeRevenuesRequest) (*common.Empty, error) {
	return nil, nil
}
func (s *GovServer) DistributePollReward(ctx context.Context, in *gov.PollRewardRequest) (*common.Empty, error) {
	return nil, nil
}
func (s *GovServer) DistributeChargeReward(streamServer gov.Government_DistributeChargeRewardServer) error {
	return nil
}
func (s *GovServer) DistributeSubscribeReward(ctx context.Context, in *gov.SubscibeRewardRequest) (*common.Empty, error) {
	return nil, nil
}
