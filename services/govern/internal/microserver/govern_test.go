package govern

import (
	"context"
	"time"
	gov "ideal-town/api/proto/v1/government"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	// "ideal-town/pkg/constants"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	"testing"

	"log"
	"net"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)
const DialTarget = "bufnet"

var dbManager = initUnsafeGovernAgent()

func initUnsafeGovernAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "government",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Timer{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)

	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "government",
	})

	gov.RegisterGovernmentServer(server, &GovServer{
		Agent: agent,
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, gov.GovernmentClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget,
		grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := gov.NewGovernmentClient(conn)
	return ctx, conn, client
}
func TestGovServer_CreateTimer(t *testing.T) {
	tests := []struct {
		name   string
		req    *gov.Timer
		expErr error
	}{
		{"[v] valid", &gov.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0,0,1).Format("2006-01-02 15:04")}, status.Error(codes.OK, "")},
		{"[x] timer already exist", &gov.Timer{Category: 1, Id: 1}, status.Error(codes.Aborted, errs.ErrTimerAlreadyExist)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CreateTimer(ctx, tt.req)
			assert.Equal(t, tt.expErr,err) 
		})
	}
}

func TestGovServer_GetTimer(t *testing.T) {
	deadline := time.Now().AddDate(0,0,1)
	tests := []struct {
		name   string
		req    *gov.Timer
		res    *gov.Timer
		expErr error
	}{
		{"[v] valid", &gov.Timer{Category: 1, Id: 1}, &gov.Timer{Category: 1, Id: 1, Deadline: deadline.Format("2006-01-02 15:04")}, status.Error(codes.OK, "")},
		{"[X] Timer not found", &gov.Timer{Category: 1, Id: 2}, &gov.Timer{}, status.Error(codes.Aborted, errs.ErrTimerNotFound)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: deadline})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetTimer(ctx, tt.req)
			assert.Equal(t, tt.expErr,err) 
			assert.Equal(t, tt.res.GetCategory(), msg.GetCategory())
			assert.Equal(t, tt.res.GetId(), msg.GetId())
		})
	}
}

func TestGovServer_UpdateTimer(t *testing.T) {
	tests := []struct {
		name   string
		req    *gov.Timer
		expErr error
	}{
		{"[v] valid", &gov.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0,1,0).Format("2006-01-02 15:04")}, status.Error(codes.OK, "")},
		{"[X] Timer not found", &gov.Timer{Category: 1, Id: 2}, status.Error(codes.Aborted, errs.ErrTimerNotFound)},

	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0,0,1)})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.UpdateTimer(ctx, tt.req)
			assert.Equal(t, tt.expErr,err) 
		})
	}
}

func TestGovServer_DeleteTimer(t *testing.T) {
	tests := []struct {
		name   string
		req    *gov.Timer
		expErr error
	}{
		{"[v] valid", &gov.Timer{Category: 1, Id: 1}, status.Error(codes.OK, "")},
		{"[x] Timer not found", &gov.Timer{Category: 1, Id: 2}, status.Error(codes.Aborted, errs.ErrTimerNotFound)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Timer{Category: 1, Id: 1, Deadline: time.Now().AddDate(0,0,1)})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.DeleteTimer(ctx, tt.req)
			assert.Equal(t, tt.expErr,err) 
		})
	}
}
// func TestGovServer_GetIdeaState(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.StateRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *gov.StateReply
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.GetIdeaState(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.GetIdeaState() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.GetIdeaState() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_GetSolState(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.StateRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *gov.StateReply
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.GetSolState(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.GetSolState() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.GetSolState() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_GetPrState(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.StateRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *gov.StateReply
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.GetPrState(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.GetPrState() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.GetPrState() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_GetFee(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.FeeRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *gov.FeeReply
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.GetFee(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.GetFee() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.GetFee() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_DistributeTradeRevenue(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.TradeRevenuesRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *common.Empty
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.DistributeTradeRevenue(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.DistributeTradeRevenue() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.DistributeTradeRevenue() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_DistributePollReward(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.PollRewardRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *common.Empty
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.DistributePollReward(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.DistributePollReward() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.DistributePollReward() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGovServer_DistributeChargeReward(t *testing.T) {
// 	type args struct {
// 		streamServer gov.Government_DistributeChargeRewardServer
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if err := tt.s.DistributeChargeReward(tt.args.streamServer); (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.DistributeChargeReward() error = %v, wantErr %v", err, tt.wantErr)
// 			}
// 		})
// 	}
// }

// func TestGovServer_DistributeSubscribeReward(t *testing.T) {
// 	type args struct {
// 		ctx context.Context
// 		in  *gov.SubscibeRewardRequest
// 	}
// 	tests := []struct {
// 		name    string
// 		s       *GovServer
// 		args    args
// 		want    *common.Empty
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.s.DistributeSubscribeReward(tt.args.ctx, tt.args.in)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("GovServer.DistributeSubscribeReward() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("GovServer.DistributeSubscribeReward() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }
