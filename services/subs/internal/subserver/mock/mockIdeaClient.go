package mock

import (
	"context"
	idea "ideal-town/api/proto/v1/idea"
	errs "ideal-town/pkg/errors"
	common "ideal-town/api/proto/v1/common"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IdeaClient struct {
	idea.UnimplementedIdeaServer
}

func (m *IdeaClient) GetIdeaPhase(ctx context.Context, in *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseReply, error) {
	if in.Id < 4 {
		return &idea.PhaseReply{Phase: 2}, status.Error(codes.OK, "")
	}
	if in.Id == 10 {
		return &idea.PhaseReply{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)
	}
	return &idea.PhaseReply{Phase: 0}, status.Error(codes.OK, "")
}

func (m *IdeaClient) GetSolutionPhase(ctx context.Context, in *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseReply, error) {
	return nil, nil
}
func (m *IdeaClient) CreateIdea(ctx context.Context, in *idea.IdeaInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByID(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfo, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByUser(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByPhase(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaWinner(ctx context.Context, in *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseRequest, error) {
	return nil, nil
}
func (m *IdeaClient) UpdateIdea(ctx context.Context, in *idea.UpdateRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) CreateSolution(ctx context.Context, in *idea.SolutionInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByID(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfo, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByUser(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByIdea(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) UpdateSolution(ctx context.Context, in *idea.UpdateRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
