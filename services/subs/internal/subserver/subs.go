package subs

import (
	"context"
	common "ideal-town/api/proto/v1/common"
	idea "ideal-town/api/proto/v1/idea"
	subs "ideal-town/api/proto/v1/subs"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/subs/internal/subserver/subsdb"
	"ideal-town/pkg/schemas"


	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SubServer struct {
	Agent *sqlmanager.Agent
	Idea 	idea.IdeaClient
	subs.UnimplementedSubscriptionServer
}

func (s *SubServer) Subscribe(ctx context.Context, in *subs.Subs) (*common.Empty, error) {
	iid := in.GetIid()
	uid := in.GetUid()
	phase, err := s.Idea.GetIdeaPhase(context.Background(), &idea.PhaseRequest{Id: iid})
	if err != nil {
		return &common.Empty{}, err
	}
	if phase.GetPhase() != 2 {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrStageNotReached)
	}
	if err := db.Subscribe(s.Agent.Conn, iid, uid); err != nil {
		return &common.Empty{}, err
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *SubServer) GetUserSubscription(ctx context.Context, in *subs.Subs) (*subs.SubsList, error) {
	uid := in.GetUid()
	res, err := db.GetSubscribeList(s.Agent.Conn, 0, uid)
	if err != nil {
		return &subs.SubsList{}, status.Error(codes.Aborted, err.Error())
	}

	return &subs.SubsList{Subs: res}, status.Error(codes.OK, "")
}

func (s *SubServer) GetIdeaSubscription(ctx context.Context, in *subs.Subs) (*subs.SubsList, error) {
	iid := in.GetIid()
	phase, err := s.Idea.GetIdeaPhase(context.Background(), &idea.PhaseRequest{Id: iid})
	if err != nil {
		return &subs.SubsList{}, err
	}
	if phase.GetPhase() != 2 {
		return &subs.SubsList{}, status.Error(codes.Aborted, errs.ErrStageNotReached)
	}
	res, err := db.GetSubscribeList(s.Agent.Conn, iid, "")
	if err != nil {
		return &subs.SubsList{}, status.Error(codes.Aborted, err.Error())
	}

	return &subs.SubsList{Subs: res}, status.Error(codes.OK, "")
}

func (s *SubServer) UnSubscribe(ctx context.Context, in *subs.Subs) (*common.Empty, error) {
	ssid := in.Ssid
	var msg	schemas.Subscriptions
	if err := s.Agent.Conn.Where(&schemas.Subscriptions{Ssid: ssid}).Take(&msg).Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrSubscriptionNotFound)
	}
	if err := db.UnSubscribe(s.Agent.Conn, ssid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	return &common.Empty{}, status.Error(codes.OK, "")
}