package subs

import (
	"context"
	common "ideal-town/api/proto/v1/common"
	subs "ideal-town/api/proto/v1/subs"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	mock "ideal-town/services/subs/internal/subserver/mock"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const DialTarget = "bufnet"

var dbManager = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Subscriptions{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	subs.RegisterSubscriptionServer(server, &SubServer{
		Idea: 	&mock.IdeaClient{},
		Agent:	agent,
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, subs.SubscriptionClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, 
        grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := subs.NewSubscriptionClient(conn)
	return ctx, conn, client
}

func TestSubServer_Subscribe(t *testing.T) {
	tests := []struct {
		name    string
		req	   *subs.Subs
		res    *common.Empty
		expErr error
	}{
		{"[x] idea not found", &subs.Subs{Iid: 10}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] idea at wrong stage", &subs.Subs{Iid: 4}, &common.Empty{}, status.Error(codes. Aborted, errs.ErrStageNotReached)},
		{"[v] valid", &subs.Subs{Iid: 2, Uid: "user"}, &common.Empty{},status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 1, Iid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 2, Iid: 1, Uid: "user1"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Subscribe(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestSubServer_GetUserSubscription(t *testing.T) {
	tests := []struct {
		name    string
		req	   	*subs.Subs
		res    	int
		expErr 	error
	}{
		{"[x] user has no subscription", &subs.Subs{Uid: "nobody"}, 0, status.Error(codes.Aborted, errs.ErrSubscriptionNotFound)},
		{"[v] valid", &subs.Subs{Uid: "user"}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 1, Iid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 2, Iid: 1, Uid: "user1"})
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 3, Iid: 2, Uid: "user"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetUserSubscription(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetSubs()))
		})
	}
}

func TestSubServer_GetIdeaSubscription(t *testing.T) {
	tests := []struct {
		name    string
		req	   	*subs.Subs
		res    	int
		expErr 	error
	}{
		{"[x] idea not found", &subs.Subs{Iid: 10}, 0, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] idea a wrong stage", &subs.Subs{Iid: 4}, 0, status.Error(codes.Aborted, errs.ErrStageNotReached)},
		{"[x] idea has no subscription", &subs.Subs{Iid: 3}, 0, status.Error(codes.Aborted, errs.ErrSubscriptionNotFound)},
		{"[v] valid", &subs.Subs{Iid: 1}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 1, Iid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 2, Iid: 1, Uid: "user1"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetIdeaSubscription(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetSubs()))
		})
	}
}

func TestSubServer_UnSubscribe(t *testing.T) {
	tests := []struct {
		name    string
		req	   *subs.Subs
		res    *common.Empty
		expErr error
	}{
		{"[x] subscription not found", &subs.Subs{Ssid: 10}, &common.Empty{}, status.Error(codes.Aborted, errs.ErrSubscriptionNotFound)},
		{"[v] valid", &subs.Subs{Ssid: 1}, &common.Empty{}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 1, Iid: 1, Uid: "user"})
	dbManager.Conn.Create(&schemas.Subscriptions{Ssid: 2, Iid: 1, Uid: "user1"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.UnSubscribe(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
