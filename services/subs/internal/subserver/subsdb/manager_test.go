package subsdb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	agent.Conn.Create(&schemas.Subscriptions{Iid: 1, Uid: "user"})
	agent.Conn.Create(&schemas.Subscriptions{Iid: 1, Uid: "user1"})
	// agent.Conn.Create(&schemas.Subscriptions{Iid: 2, Uid: "user"})

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Subscriptions{})
}

func TestSubscribe(t *testing.T) {
		tests := []struct {
		name    string
		iid    	uint32
		uid 	string
		expErr  error
	}{
		{"[v] valid", 2, "user", nil},
		{"[x] user has already subscribed this idea", 1, "user", errors.New(errs.ErrAlreadySubscribed)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Subscribe(agent.Conn, tt.iid, tt.uid)
			assert.Equal(t, tt.expErr, err)
		})
	}
}

func TestGetSubscribeList(t *testing.T) {
		tests := []struct {
		name    string
		iid    	uint32
		uid 	string
		res    	int
		expErr  error
	}{
		{"[v] valid", 1, "", 2, nil},
		{"[v] valid", 0, "user", 2, nil},
		{"[x] subscription not found(iid)", 3, "", 0, errors.New(errs.ErrSubscriptionNotFound)},
		{"[x] subscription not found(uid)", 0, "user2", 0, errors.New(errs.ErrSubscriptionNotFound)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := GetSubscribeList(agent.Conn, tt.iid, tt.uid)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg))
		})
	}
}

func TestUnSubscribe(t *testing.T) {
		tests := []struct {
		name    string
		ssid    uint32
		expErr  error
	}{
		{"[v] valid", 1, nil},
		{"[v] 亂刪", 10, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := UnSubscribe(agent.Conn, tt.ssid)
			assert.Equal(t, tt.expErr, err)			
		})
	}
}
