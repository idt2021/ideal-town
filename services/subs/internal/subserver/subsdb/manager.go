package subsdb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/subs"
	"ideal-town/pkg/schemas"

	"gorm.io/gorm"
)

func Subscribe(conn *gorm.DB, iid uint32, uid string) error {
	var msg schemas.Subscriptions
	if err := conn.Where(&schemas.Subscriptions{Iid: iid, Uid: uid}).Take(&msg).Error; err == nil {
		return errors.New(errs.ErrAlreadySubscribed)
	}
	if err := conn.Create(&schemas.Subscriptions{Iid: iid, Uid: uid}).Error; err != nil {
		return err
	}
	return nil
} 

func GetSubscribeList(conn *gorm.DB, iid uint32, uid string) ([]*pb.Subs, error) {
	var msg	[]schemas.Subscriptions
	var res []*pb.Subs
	if iid != 0 {
		result := conn.Where(&schemas.Subscriptions{Iid: iid}).Find(&msg)
		if err := result.Error; err != nil {
			return nil, errors.New(errs.ErrSubscriptionNotFound)
		}
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.Subs{
				Ssid: 	element.Ssid,
				Iid:    element.Iid,
				Uid:    element.Uid,
			})
		}
	} else {
		result := conn.Where(&schemas.Subscriptions{Uid: uid}).Find(&msg)
		if err := result.Error; err != nil {
			return nil, errors.New(errs.ErrSubscriptionNotFound)
		}
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.Subs{
				Ssid: 	element.Ssid,
				Iid:    element.Iid,
				Uid:    element.Uid,
			})
		}
	}

	if len(res) == 0 {
		return res, errors.New(errs.ErrSubscriptionNotFound)
	}
	return res, nil
}

func UnSubscribe(conn *gorm.DB, ssid uint32) error {
	if err := conn.Where("ssid=?", ssid).Delete(&schemas.Subscriptions{}).Error; err != nil {
		return err
	}
	return nil
} 