package bankdb

import (
	"errors"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "bank",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Banks{}, &schemas.Wallets{}, &schemas.Locks{})
}

func TestCreateWallet(t *testing.T) {
	tests := []struct {
		name string
		uid  string
		err  error
	}{
		{"[v] Valid", "a", nil},
		{"[x] user exist", "a", errors.New(errs.ErrUserAlreadyExist)},
	}

	resetTable(agent)
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := CreateWallet(agent.Conn, tt.uid)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestGetIDC(t *testing.T) {
	tests := []struct {
		name string
		uid  string
		idc  float32
		err  error
	}{
		{"[v] Valid", "a", 11.25, nil},
		{"[x] user not found", "bbb", 0, errors.New(errs.ErrUserNotFound)},
	}
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 11.25})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			idc, err := GetIDC(agent.Conn, tt.uid)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, tt.idc, idc)
		})
	}
}

func TestWithdraw(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		remark string
		action uint32
		take   float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", "remark", constants.Action_Withdraw, 30, 70, nil},
		{"[v] Valid", "a", "remark", constants.Action_Fee, 40, 30, nil},
		{"[x] idc not enough", "a", "remark", constants.Action_Withdraw, 50, 30, errors.New(errs.ErrNoEnoughIDC)},
		{"[x] idc negative", "a", "remark", constants.Action_Withdraw, -50, 30, errors.New(errs.ErrExpectedPositive)},
		{"[x] Action_Deposit not allow", "a", "remark", constants.Action_Deposit, 50, 30, errors.New(errs.ErrInvalidAction)},
		{"[x] Action_Lock not allow", "a", "remark", constants.Action_Lock, 50, 30, errors.New(errs.ErrInvalidAction)},
		{"[x] user not found", "b", "remark", constants.Action_Withdraw, 50, 30, errors.New(errs.ErrUserNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 100})

	var wallet schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Withdraw(agent.Conn, tt.uid, tt.remark, tt.action, tt.take)
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&wallet)
			assert.Equal(t, tt.remain, wallet.Idc)
		})
	}
}

func TestDeposit(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		remark string
		action uint32
		idc    float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", "remark", constants.Action_Deposit, 70, 72, nil},
		{"[v] Valid", "a", "remark", constants.Action_Revenue, 2, 74, nil},
		{"[v] Valid", "a", "remark", constants.Action_Reward, 1, 75, nil},
		{"[x] idc negative", "a", "remark", constants.Action_Deposit, -50, 75, errors.New(errs.ErrExpectedPositive)},
		{"[x] user not found", "xxx", "remark", constants.Action_Deposit, 50, 75, errors.New(errs.ErrUserNotFound)},
		{"[x] action not allow", "b", "remark", constants.Action_Withdraw, 50, 75, errors.New(errs.ErrInvalidAction)},
		{"[x] action not allow", "b", "remark", constants.Action_Lock, 50, 75, errors.New(errs.ErrInvalidAction)},
	}
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 2})

	var wallet schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Deposit(agent.Conn, tt.uid, tt.remark, tt.action, tt.idc)
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&wallet)
			assert.Equal(t, tt.remain, wallet.Idc)
		})
	}
}

func TestLock(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		idc    float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 11.25, 11.25, nil},
		{"[x] Valid", "a", 10000, 11.25, errors.New(errs.ErrNoEnoughIDC)},
		{"[x] idc negative", "a", -100, 11.25, errors.New(errs.ErrExpectedPositive)},
		{"[x] user not found", "b", 10, 11.25, errors.New(errs.ErrUserNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 22.5})

	var check schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Lock(agent.Conn, tt.uid, tt.idc)

			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)
		})
	}
}

func TestUnlock(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		lid    uint32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 0, 150, nil},
		{"[x] lock not found", "b", 0, 150, errors.New(errs.ErrLocksNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 150})

	lid, _ := Lock(agent.Conn, "a", 100)
	tests[0].lid = lid

	var check schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Unlock(agent.Conn, tt.lid)
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)
		})
	}
}

func TestConfirm(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		lid    uint32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 0, 50, nil},
		{"[x] lock not found", "b", 0, 50, errors.New(errs.ErrLocksNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 150})

	lid, _ := Lock(agent.Conn, "a", 100)
	tests[0].lid = lid

	var check schemas.Wallets
	var record schemas.Banks
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Confirm(agent.Conn, tt.lid)
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)

			agent.Conn.Where("uid=?", tt.uid).Take(&record)
			assert.Equal(t, constants.Action_Buy, record.Action)
		})
	}
}

func TestGetBankRecords(t *testing.T) {
	now := time.Now()
	tests := []struct {
		name     string
		page     uint32
		pageSize uint32
		uid      string
		records  []BankRecord
		err      error
	}{
		{"[v] Valid", 0, 0, "a", []BankRecord{
			{now, constants.Action_Deposit, 10},
			{now, constants.Action_Buy, -10},
			{now, constants.Action_Sell, 10},
			{now, constants.Action_Sell, 15},
			{now, constants.Action_Withdraw, -15}}, nil},
		{"[v] Valid", 1, 3, "a", []BankRecord{
			{now, constants.Action_Deposit, 10},
			{now, constants.Action_Buy, -10},
			{now, constants.Action_Sell, 10}}, nil},
		{"[v] Valid", 1, 4, "a", []BankRecord{{now, constants.Action_Withdraw, -15}}, nil},
		{"[v] Valid", 1, 3, "a", []BankRecord{{now, constants.Action_Sell, 15}}, nil},
		{"[x] user not found", 1, 0, "b", []BankRecord{}, errors.New(errs.ErrUserNotFound)},
	}
	defer resetTable(agent)

	CreateWallet(agent.Conn, "a")
	Deposit(agent.Conn, "a", "yoyo", constants.Action_Deposit, 10) // Deposit
	lid, _ := Lock(agent.Conn, "a", 10)                            // lock
	Confirm(agent.Conn, lid)                                       // Buy
	Deposit(agent.Conn, "a", "", constants.Action_Sell, 10)        // Sell
	Deposit(agent.Conn, "a", "", constants.Action_Sell, 15)        // Sell
	Withdraw(agent.Conn, "a", "", constants.Action_Withdraw, 15)   // withdraw

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := GetBankRecords(agent.Conn, tt.uid, tt.page, tt.pageSize)
			assert.Equal(t, tt.err, err)
			// assert.Equal(t, tt.records, r)
		})
	}
}
