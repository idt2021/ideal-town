package bankdb

import (
	"errors"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"time"

	"gorm.io/gorm"
)

// func sessionCloser(tx *gorm.DB) {
// 	if stmtManger, ok := tx.ConnPool.(*gorm.PreparedStmtDB); ok {
// 		stmtManger.Close()
// 	}
// }

type BankRecord struct {
	CreatedAt time.Time
	Action    uint32
	Idc       float32
}

// create a wallet for user (use when register),
// if user exists, return ErrUserAlreadyExist
func CreateWallet(conn *gorm.DB, uid string) error {
	if err := conn.Create(&schemas.Wallets{Uid: uid, Idc: 0}).Error; err != nil {
		return errors.New(errs.ErrUserAlreadyExist)
	}
	return nil
}

// GetIDC get idc assets info for an user
// if user not found, return ErrUserNotFound
func GetIDC(conn *gorm.DB, uid string) (float32, error) {
	var wallet schemas.Wallets
	if err := conn.Model(&schemas.Wallets{}).Where("uid=?", uid).Take(&wallet).Error; err != nil {
		return 0, errors.New(errs.ErrUserNotFound)
	}
	return wallet.Idc, nil
}

// Withdraw check total assets is bigger than request value,
// and then add an record with specified action.
// Note that Idc should be positive
// The allow actions are: Action_Withdraw, Action_Fee
// 	if idc <= 0 , return ErrExpectedPositive
// 	if action is not allowed, return ErrInvalidAction
// 	if user not found, return ErrUserNotFound
//	if idc is not enough, return ErrNoEnoughIDC
func Withdraw(tx *gorm.DB, uid string, remark string, action uint32, idc float32) error {
	// check argument
	if idc < 0 {
		return errors.New(errs.ErrExpectedPositive)
	}

	// check action
	switch action {
	case constants.Action_Withdraw, constants.Action_Fee: // Do Nothing
	default:
		return errors.New(errs.ErrInvalidAction)
	}

	// check wallet
	var wallet schemas.Wallets
	if err := tx.Where(&schemas.Wallets{Uid: uid}).Take(&wallet).Error; err != nil {
		return errors.New(errs.ErrUserNotFound)
	}

	// check idc
	if wallet.Idc < idc {
		return errors.New(errs.ErrNoEnoughIDC)
	}

	// create
	if err := tx.Create(
		&schemas.Banks{
			Uid:    uid,
			Action: action,
			Remark: remark,
			Idc:    -idc}).Error; err != nil {
		return err
	}

	// update wallet
	wallet.Idc -= idc
	if err := tx.Save(&wallet).Error; err != nil {
		return err
	}

	return nil
}

// Deposit add an record with action Action_Deposit
// Note that Idc should be positive
// The allow actions are: Action_Deposit, Action_Reward, Action_Revenue
// 	if idc <= 0 , return ErrExpectedPositive
// 	if action is not allowed, return ErrInvalidAction
// 	if user not found, return ErrUserNotFound
//	if idc is not enough, return ErrNoEnoughIDC
func Deposit(tx *gorm.DB, uid string, remark string, action uint32, idc float32) error {
	// check argument
	if idc < 0 {
		return errors.New(errs.ErrExpectedPositive)
	}

	// check action
	switch action {
	case constants.Action_Deposit: // Do Nothing
	case constants.Action_Reward: // Do Nothing
	case constants.Action_Revenue: // Do Nothing
	default:
		return errors.New(errs.ErrInvalidAction)
	}

	// check wallet
	var wallet schemas.Wallets
	if err := tx.Where("uid=?", uid).Take(&wallet).Error; err != nil {
		return errors.New(errs.ErrUserNotFound)
	}

	// create
	if err := tx.Create(
		&schemas.Banks{
			Uid:    uid,
			Action: action,
			Remark: remark,
			Idc:    idc}).Error; err != nil {
		return err
	}

	// update wallet
	wallet.Idc += idc
	if err := tx.Save(&wallet).Error; err != nil {
		return err
	}

	return nil
}

// Lock will check total assets is bigger than request value
// and then UPDATE wallets, and add a record to locks table
// Note that Idc should be positive
// 	if idc <= 0 , return ErrExpectedPositive
func Lock(tx *gorm.DB, uid string, idc float32) (uint32, error) {
	// check argument
	if idc < 0 {
		return 0, errors.New(errs.ErrExpectedPositive)
	}
	// check wallet
	var wallet schemas.Wallets
	if err := tx.Where("uid=?", uid).Take(&wallet).Error; err != nil {
		return 0, errors.New(errs.ErrUserNotFound)
	}
	// check idc
	if wallet.Idc < idc {
		return 0, errors.New(errs.ErrNoEnoughIDC)
	}
	// update wallet
	wallet.Idc -= idc
	if err := tx.Save(&wallet).Error; err != nil {
		return 0, err
	}
	// add to lock table
	locks := schemas.Locks{Uid: uid, Idc: idc}
	if err := tx.Create(&locks).Error; err != nil {
		return 0, err
	}
	return locks.Lid, nil
}

// Unlock will remove a specified record from locks table,
// and add the idc back to wallets
func Unlock(tx *gorm.DB, lid uint32) error {
	var locks schemas.Locks
	// check lock table
	if err := tx.Where("lid=?", lid).Take(&locks).Error; err != nil {
		return errors.New(errs.ErrLocksNotFound)
	}
	// delete from lock
	if err := tx.Unscoped().Delete(&locks).Error; err != nil {
		return err
	}
	// update wallets
	if err := tx.Model(&schemas.Wallets{}).
		Where("uid=?", locks.Uid).
		Update("idc", gorm.Expr("idc + ?", locks.Idc)).Error; err != nil {
		return err
	}
	return nil
}

// Confirm will remove a specified record from locks table,
// and become a withdraw record in banks table
func Confirm(tx *gorm.DB, lid uint32) error {
	var locks schemas.Locks
	// check lock table
	if err := tx.Where("lid=?", lid).Take(&locks).Error; err != nil {
		return errors.New(errs.ErrLocksNotFound)
	}
	// delete from lock
	if err := tx.Unscoped().Delete(&locks).Error; err != nil {
		return err
	}
	// add a bank record
	if err := tx.Create(
		&schemas.Banks{
			Uid:    locks.Uid,
			Action: constants.Action_Buy,
			Idc:    -locks.Idc}).Error; err != nil {
		return err
	}
	return nil
}

// GetBankRecord get bank records with limit and offset
func GetBankRecords(conn *gorm.DB, uid string, page uint32, pageSize uint32) ([]BankRecord, error) {

	var bankRecord []BankRecord
	if pageSize > 0 {
		conn = conn.Limit(int(pageSize))
		if page >= 1 {
			conn = conn.Offset(int(pageSize) * int(page-1))
		}
	}

	if err := conn.Model(&schemas.Banks{}).
		Where("uid=?", uid).
		Find(&bankRecord).Error; err != nil {
		return nil, err
	}

	if len(bankRecord) == 0 {
		return nil, errors.New(errs.ErrUserNotFound)
	}

	return bankRecord, nil
}
