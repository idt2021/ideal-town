// package bankserver dedicate to control the gRPC flow of bank service
package bankserver

import (
	"context"
	pb "ideal-town/api/proto/v1/bank"
	"ideal-town/api/proto/v1/common"
	"ideal-town/pkg/shared/sqlmanager"
	bkdb "ideal-town/services/bank/internal/microserver/bankdb"
	"log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// bankServer handles requests and responses,
// and calls bankdb package to implement
// the database operations.
type BankServer struct {
	Agent *sqlmanager.Agent
	pb.UnimplementedBankServer
}

func (b *BankServer) CreateWallet(ctx context.Context, req *pb.IDCRequest) (*common.Empty, error) {

	log.Printf("Request received, create, uid: %s", req.GetUid())

	err := bkdb.CreateWallet(b.Agent.Conn, req.Uid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
func (b *BankServer) GetIDC(ctx context.Context, req *pb.IDCRequest) (*pb.IDCReply, error) {
	log.Printf("Request received, get idc, uid: %s", req.GetUid())

	idc, err := bkdb.GetIDC(b.Agent.Conn, req.Uid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.IDCReply{Idc: idc}, nil
}

func (b *BankServer) Withdraw(ctx context.Context, req *pb.BankRequest) (*common.Empty, error) {
	log.Printf("Request received, withdraw %f from uid: %s", req.GetIdc(), req.GetUid())

	err := bkdb.Withdraw(b.Agent.Conn, req.Uid, req.Remark, uint32(req.Action), req.Idc)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

func (b *BankServer) Deposit(ctx context.Context, req *pb.BankRequest) (*common.Empty, error) {
	log.Printf("Request received, deposit %f from uid: %s", req.GetIdc(), req.GetUid())

	err := bkdb.Deposit(b.Agent.Conn, req.Uid, req.Remark, uint32(req.Action), req.Idc)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

// Lock will UPDATE wallets directly, without leaving any records in banks
func (b *BankServer) Lock(ctx context.Context, req *pb.LockRequest) (*pb.LidInfo, error) {
	log.Printf("Request received, lock %f from uid: %s", req.GetIdc(), req.GetUid())

	lid, err := bkdb.Lock(b.Agent.Conn, req.Uid, req.Idc)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.LidInfo{Lid: lid}, nil
}

// Unlock UPDATE the wallets
func (b *BankServer) Unlock(ctx context.Context, req *pb.LidInfo) (*common.Empty, error) {
	log.Printf("Request received, unlock lid: %d", req.GetLid())

	err := bkdb.Unlock(b.Agent.Conn, req.Lid)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

func (b *BankServer) Confirm(ctx context.Context, req *pb.LidInfo) (*common.Empty, error) {
	log.Printf("Request received, confirm lid: %d", req.GetLid())

	err := bkdb.Confirm(b.Agent.Conn, req.Lid)
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
