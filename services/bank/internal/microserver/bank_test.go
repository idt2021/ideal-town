package bankserver

import (
	"context"
	"ideal-town/api/proto/v1/bank"
	pb "ideal-town/api/proto/v1/bank"
	"ideal-town/api/proto/v1/common"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	schemas "ideal-town/pkg/schemas"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const ConnByteSize = 1024 * 1024
const DialTarget = ""

var agent = initUnsafeBankAgent()

func initUnsafeBankAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "bank",
	})
	resetTable(agent)
	return agent
}
func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Banks{}, &schemas.Wallets{}, &schemas.Locks{})
}

// Create dialer with db connection
func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "bank",
	})
	pb.RegisterBankServer(server, &BankServer{
		Agent: agent,
	})
	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()
	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, pb.BankClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pb.NewBankClient(conn)
	return ctx, conn, client
}
func TestBankServer_CreateWallet(t *testing.T) {
	tests := []struct {
		name string
		uid  string
		err  error
	}{
		{"[v] Valid", "a", nil},
		{"[x] user exist", "a", status.Error(codes.Aborted, errs.ErrUserAlreadyExist)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	resetTable(agent)
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CreateWallet(ctx, &bank.IDCRequest{Uid: tt.uid})
			assert.Equal(t, tt.err, err)
		})
	}
}
func TestBankServer_GetIDC(t *testing.T) {
	tests := []struct {
		name string
		uid  string
		idc  float32
		err  error
	}{
		{"[v] Valid", "a", 11.25, nil},
		{"[x] user not found", "bbb", 0, status.Error(codes.Aborted, errs.ErrUserNotFound)},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()

	resetTable(agent)
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 11.25})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reply, err := client.GetIDC(ctx, &pb.IDCRequest{Uid: tt.uid})
			assert.Equal(t, tt.err, err)
			if err == nil {
				assert.Equal(t, tt.idc, reply.Idc)
			}

		})
	}
}

func TestBankServer_Withdraw(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		remark string
		action common.Action
		take   float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", "remark", common.Action_Withdraw, 30, 70, nil},
		{"[v] Valid", "a", "remark", common.Action_Fee, 40, 30, nil},
		{"[x] idc not enough", "a", "remark", common.Action_Withdraw, 50, 30, status.Error(codes.Aborted, errs.ErrNoEnoughIDC)},
		{"[x] idc negative", "a", "remark", common.Action_Withdraw, -50, 30, status.Error(codes.Aborted, errs.ErrExpectedPositive)},
		{"[x] Action_Deposit not allow", "a", "remark", common.Action_Deposit, 50, 30, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] Action_Lock not allow", "a", "remark", common.Action_Lock, 50, 30, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] user not found", "b", "remark", common.Action_Withdraw, 50, 30, status.Error(codes.Aborted, errs.ErrUserNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	resetTable(agent)
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 100})

	var wallet schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Withdraw(ctx, &pb.BankRequest{
				Uid:    tt.uid,
				Remark: tt.remark,
				Action: tt.action,
				Idc:    tt.take,
			})
			assert.Equal(t, tt.err, err)
			if err == nil {
				agent.Conn.Where("uid=?", tt.uid).Take(&wallet)
				assert.Equal(t, tt.remain, wallet.Idc)
			}

		})
	}
}

func TestBankServer_Deposit(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		remark string
		action common.Action
		idc    float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", "remark", common.Action_Deposit, 70, 72, nil},
		{"[v] Valid", "a", "remark", common.Action_Revenue, 2, 74, nil},
		{"[v] Valid", "a", "remark", common.Action_Reward, 1, 75, nil},
		{"[x] idc negative", "a", "remark", common.Action_Deposit, -50, 75, status.Error(codes.Aborted, errs.ErrExpectedPositive)},
		{"[x] user not found", "xxx", "remark", common.Action_Deposit, 50, 75, status.Error(codes.Aborted, errs.ErrUserNotFound)},
		{"[x] action not allow", "b", "remark", common.Action_Withdraw, 50, 75, status.Error(codes.Aborted, errs.ErrInvalidAction)},
		{"[x] action not allow", "b", "remark", common.Action_Lock, 50, 75, status.Error(codes.Aborted, errs.ErrInvalidAction)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 2})

	var wallet schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Deposit(ctx, &pb.BankRequest{
				Uid:    tt.uid,
				Remark: tt.remark,
				Action: tt.action,
				Idc:    tt.idc,
			})
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&wallet)
			assert.Equal(t, tt.remain, wallet.Idc)
		})
	}
}

func TestBankServer_Lock(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		idc    float32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 11.25, 11.25, nil},
		{"[x] Valid", "a", 10000, 11.25, status.Error(codes.Aborted, errs.ErrNoEnoughIDC)},
		{"[x] idc negative", "a", -100, 11.25, status.Error(codes.Aborted, errs.ErrExpectedPositive)},
		{"[x] user not found", "b", 10, 11.25, status.Error(codes.Aborted, errs.ErrUserNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 22.5})

	var check schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Lock(ctx, &bank.LockRequest{Uid: tt.uid, Idc: tt.idc})

			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)
		})
	}
}

func TestBankServer_Unlock(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		lid    uint32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 0, 150, nil},
		{"[x] lock not found", "b", 0, 150, status.Error(codes.Aborted, errs.ErrLocksNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 150})

	lidinfo, _ := client.Lock(ctx, &bank.LockRequest{Uid: "a", Idc: 100})
	tests[0].lid = lidinfo.Lid

	var check schemas.Wallets
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Unlock(ctx, &bank.LidInfo{Lid: tt.lid})
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)
		})
	}
}

func TestBankServer_Confirm(t *testing.T) {
	tests := []struct {
		name   string
		uid    string
		lid    uint32
		remain float32
		err    error
	}{
		{"[v] Valid", "a", 0, 50, nil},
		{"[x] lock not found", "b", 0, 50, status.Error(codes.Aborted, errs.ErrLocksNotFound)},
	}
	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(agent)
	agent.Conn.Create(&schemas.Wallets{Uid: "a", Idc: 150})

	lidinfo, _ := client.Lock(ctx, &bank.LockRequest{Uid: "a", Idc: 100})
	tests[0].lid = lidinfo.Lid

	var check schemas.Wallets
	var record schemas.Banks
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.Confirm(ctx, &bank.LidInfo{Lid: tt.lid})
			assert.Equal(t, tt.err, err)

			agent.Conn.Where("uid=?", tt.uid).Take(&check)
			assert.Equal(t, tt.remain, check.Idc)

			agent.Conn.Where("uid=?", tt.uid).Take(&record)
			assert.Equal(t, constants.Action_Buy, record.Action)
		})
	}
}
