package main

import (
	"context"
	gw "ideal-town/api/proto/v1/bank"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	bank "ideal-town/services/bank/internal/microserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":5050"
	httpServerEndpoint = ":8888"
)

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithInsecure()}
	if err := gw.RegisterBankHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "bank",
	})
	s := grpc.NewServer()
	gw.RegisterBankServer(s, &bank.BankServer{
		Agent: agent,
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}