package pollingdb

import (
	"errors"
	"ideal-town/api/proto/v1/government"
	pb "ideal-town/api/proto/v1/polling"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"ideal-town/services/polling/constant"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Tests:
// - GetIdeas
// - GetIdeaByIid
// - GetIdeaByUid
var agent = initUnsafePollingAgent()

func initUnsafePollingAgent() *PollingAgent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	agent.ResetDB()
	return &PollingAgent{Agent: *agent}
}

func resetTable(agent *PollingAgent) {
	agent.ResetDB(&schemas.Pollings{}, &schemas.Ideas{}, &schemas.Solutions{})
}

func TestPollingAgent_Pollings(t *testing.T) {
	type args struct {
		polling []schemas.Pollings
		iid     uint32
		sid     uint32
		uid     string
	}
	tests := []struct {
		name string
		args args
		got  [][]pb.PollingInfo //0:all 1:uid 2:iid 3:sid
		err  error
	}{
		{
			"[v] Valid",
			args{
				polling: []schemas.Pollings{
					{
						Pid:    1,
						Uid:    "a",
						Iid:    1,
						Sid:    2,
						Target: constant.IDEA,
						Idc:    30,
					}, {
						Pid:    2,
						Uid:    "b",
						Iid:    2,
						Sid:    2,
						Target: constant.SOLUTION,
						Idc:    20,
					}, {
						Pid:    3,
						Uid:    "a",
						Iid:    2,
						Sid:    3,
						Target: constant.SOLUTION,
						Idc:    10},
				},
				iid: 2,
				uid: "a",
				sid: 3,
			},
			[][]pb.PollingInfo{
				{{
					Pid:    1,
					Uid:    "a",
					Iid:    1,
					Sid:    2,
					Target: constant.IDEA,
					Idc:    30,
				}, {
					Pid:    2,
					Uid:    "b",
					Iid:    2,
					Sid:    2,
					Target: constant.SOLUTION,
					Idc:    20,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    2,
					Uid:    "b",
					Iid:    2,
					Sid:    2,
					Target: constant.SOLUTION,
					Idc:    20,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    1,
					Uid:    "a",
					Iid:    1,
					Sid:    2,
					Target: constant.IDEA,
					Idc:    30,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
			},
			nil,
		},
	}
	resetTable(agent)
	defer resetTable(agent)

	// Test: GetPolling
	for _, tt := range tests {
		defer resetTable(agent)
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(tt.args.polling)
			got, err := agent.GetPollings()
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[0]), len(got))

			got, err = agent.GetPollingByUid(tt.args.uid)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[1]), len(got))
			got, err = agent.GetPollingByIid(tt.args.iid)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[2]), len(got))
			got, err = agent.GetPollingBySid(tt.args.sid)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[3]), len(got))

		})
	}
}
func TestPollingAgent_Deposit(t *testing.T) {
	type args struct {
		target int32
		id     uint32
		idc    uint32
		uid    string
	}
	tests := []struct {
		name string
		args args
		err  error
	}{
		{
			name: "[v]valid withdraw from idea",
			args: args{
				target: constant.IDEA,
				id:     2,
				idc:    90,
				uid:    "b",
			},
			err: nil,
		}, {
			name: "[v]valid withdraw from solution",
			args: args{
				target: constant.SOLUTION,
				id:     5,
				idc:    20,
				uid:    "c",
			},
			err: nil,
		},
	}
	defer resetTable(agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resetTable(agent)
			err := agent.Deposit(tt.args.target, tt.args.id, tt.args.idc, tt.args.uid)
			assert.Equal(t, tt.err, err)
			if err == nil {
				got, err := agent.GetPollings()
				assert.Equal(t, tt.err, err)
				assert.Equal(t, tt.args.target, got[0].Target)
				if tt.args.target == constant.IDEA {
					assert.Equal(t, tt.args.id, got[0].Iid)
				}
				if tt.args.target == constant.SOLUTION {
					assert.Equal(t, tt.args.id, got[0].Sid)
				}
				assert.Equal(t, int32(tt.args.idc), got[0].Idc)
				assert.Equal(t, tt.args.uid, got[0].Uid)
			}

		})
	}
}
func TestPollingAgent_Withdraw(t *testing.T) {
	type args struct {
		target int32
		id     uint32
		idc    uint32
		uid    string
	}
	tests := []struct {
		name string
		args args
		err  error
	}{
		{
			name: "[v]valid withdraw from idea",
			args: args{
				target: constant.IDEA,
				id:     2,
				idc:    90,
				uid:    "b",
			},
			err: nil,
		}, {
			name: "[v]valid withdraw from solution",
			args: args{
				target: constant.SOLUTION,
				id:     5,
				idc:    20,
				uid:    "c",
			},
			err: nil,
		},
		{
			name: "[x] idc not enough in idea",
			args: args{
				target: constant.IDEA,
				id:     2,
				idc:    100,
				uid:    "b",
			},
			err: errors.New(errs.ErrNoEnoughIDC),
		},
		{
			name: "[x] idc not enough in solution",
			args: args{
				target: constant.SOLUTION,
				id:     5,
				idc:    30,
				uid:    "c",
			},
			err: errors.New(errs.ErrNoEnoughIDC),
		},
	}
	defer resetTable(agent)
	resetTable(agent)
	agent.Deposit(constant.IDEA, 2, 150, "b")
	agent.Deposit(constant.SOLUTION, 5, 40, "c")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := agent.Withdraw(tt.args.target, tt.args.id, tt.args.idc, tt.args.uid)
			assert.Equal(t, tt.err, err)
			if err == nil {
				got, err := agent.GetPollings()
				assert.Nil(t, err)
				assert.NotZero(t, len(got))
			}

		})
	}
}

func TestPollingAgent_GetPollratesAndUpdateWinner(t *testing.T) {
	type args struct {
		iid uint32
	}
	type got struct {
		total    uint32
		pollrate []government.PollRate
	}
	tests := []struct {
		name string
		args args
		got  got
		err  error
	}{
		{
			name: "[v] valid",
			args: args{
				iid: 1,
			},
			got: got{
				total: 250,
				pollrate: []government.PollRate{
					{
						Uid: "d",
						Idc: 110,
					},
					{
						Uid: "e",
						Idc: 90,
					},
					{
						Uid: "f",
						Idc: 50,
					},
				},
			},
			err: nil,
		},
		{
			name: "[v]valid no polling data",
			args: args{
				iid: 3,
			},
			got: got{
				total:    0,
				pollrate: []government.PollRate{},
			},
			err: nil,
		},
	}
	defer resetTable(agent)
	// Poll
	// 1: 110
	// 2: 90
	// 3: 50
	agent.Deposit(2, 1, 40, "a")
	agent.Deposit(2, 2, 30, "a")
	agent.Deposit(2, 1, 10, "b")
	agent.Deposit(2, 1, 60, "c")
	agent.Deposit(2, 2, 60, "c")
	agent.Deposit(2, 3, 50, "c")
	agent.Conn.Create(&schemas.Solutions{Sid: 1, Iid: 1, Uid: "d"})
	agent.Conn.Create(&schemas.Solutions{Sid: 2, Iid: 1, Uid: "e"})
	agent.Conn.Create(&schemas.Solutions{Sid: 3, Iid: 1, Uid: "f"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			total, got, err := agent.GetPollratesAndUpdateWinner(tt.args.iid)
			assert.Equal(t, tt.err, err)
			if err == nil {
				assert.Equal(t, tt.got.total, total)
				assert.Equal(t, len(tt.got.pollrate), len(got))
			}

		})
	}
}
func TestPollingAgent_ChangePhaseToP2T(t *testing.T) {
	tests := []struct {
		name string
		iid  uint32
		err  error
	}{
		{
			"[v] valid",
			1,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := agent.ChangePhaseToP2T(tt.iid)
			assert.Equal(t, tt.err, err)
		})
	}
}
