package pollingdb

import (
	"errors"
	gov "ideal-town/api/proto/v1/government"
	pb "ideal-town/api/proto/v1/polling"
	"ideal-town/pkg/constants"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	// "ideal-town/pkg/shared/sqlmanager"
	"ideal-town/services/polling/constant"

	"gorm.io/gorm"
)

func GetPollings(conn *gorm.DB) ([]*pb.PollingInfo, error) {
	var pollings []schemas.Pollings
	var pollingInfo []*pb.PollingInfo
	result := conn.Find(&pollings)
	if err := result.Error; err != nil {
		return nil, result.Error
	}
	for _, element := range pollings {
		// index is the index where we are
		// element is the element from someSlice for where we are
		pollingInfo = append(pollingInfo, &pb.PollingInfo{
			Pid:    element.Pid,
			Iid:    element.Iid,
			Sid:    element.Sid,
			Uid:    element.Uid,
			Target: element.Target,
			Idc:    element.Idc,
		})
	}
	return pollingInfo, nil
}
func GetPollingByIid(conn *gorm.DB, iid uint32) ([]*pb.PollingInfo, error) {
	var pollings []schemas.Pollings
	var pollingInfo []*pb.PollingInfo
	result := conn.Where(&schemas.Pollings{Iid: iid}).Find(&pollings)
	if err := result.Error; err != nil {
		return nil, result.Error
	}
	for _, element := range pollings {
		// index is the index where we are
		// element is the element from someSlice for where we are
		pollingInfo = append(pollingInfo, &pb.PollingInfo{
			Pid:    element.Pid,
			Iid:    element.Iid,
			Sid:    element.Sid,
			Uid:    element.Uid,
			Target: element.Target,
			Idc:    element.Idc,
		})
	}
	return pollingInfo, nil
}
func GetPollingBySid(conn *gorm.DB, sid uint32) ([]*pb.PollingInfo, error) {
	var pollings []schemas.Pollings
	var pollingInfo []*pb.PollingInfo
	result := conn.Where(&schemas.Pollings{Sid: sid}).Find(&pollings)
	if err := result.Error; err != nil {
		return nil, result.Error
	}
	for _, element := range pollings {
		// index is the index where we are
		// element is the element from someSlice for where we are
		pollingInfo = append(pollingInfo, &pb.PollingInfo{
			Pid:    element.Pid,
			Iid:    element.Iid,
			Sid:    element.Sid,
			Uid:    element.Uid,
			Target: element.Target,
			Idc:    element.Idc,
		})
	}
	return pollingInfo, nil
}

// get polling info by investor
func GetPollingByUid(conn *gorm.DB, uid string) ([]*pb.PollingInfo, error) {
	var pollings []schemas.Pollings
	var pollingInfo []*pb.PollingInfo
	result := conn.Where(&schemas.Pollings{Uid: uid}).Find(&pollings)
	if err := result.Error; err != nil {
		return nil, result.Error
	}
	for _, element := range pollings {
		// index is the index where we are
		// element is the element from someSlice for where we are
		pollingInfo = append(pollingInfo, &pb.PollingInfo{
			Pid:    element.Pid,
			Iid:    element.Iid,
			Sid:    element.Sid,
			Uid:    element.Uid,
			Target: element.Target,
			Idc:    element.Idc,
		})
	}
	return pollingInfo, nil
}

func Withdraw(conn *gorm.DB, target int32, id uint32, idc float32, uid string) error {
	// conn.Create(&schemas.Pollings{})

	switch target {
	case constant.IDEA:
		// 從Idea提出
		poll := &schemas.Pollings{
			Iid:    id,
			Sid:    0,
			Uid:    uid,
			Target: constant.IDEA,
			Idc:    -idc,
		}
		tx := conn.Begin()
		// 檢查餘額
		var remain int
		if err := tx.Model(&poll).Select("SUM(idc)").
			Where("iid=? AND uid=?", poll.Iid, poll.Uid).
			Scan(&remain).Error; err != nil {
			tx.Rollback()
			return err
		}
		// idea餘額不足
		if remain < int(idc) {
			tx.Rollback()
			return errors.New(errs.ErrNoEnoughIDC)
		}
		// 加入負的餘額
		if err := tx.Create(&poll).Error; err != nil {
			tx.Rollback()
			return err
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}

		return nil

	case constant.SOLUTION:
		// 從Solution提出

		poll := &schemas.Pollings{
			Iid:    0,
			Sid:    id,
			Uid:    uid,
			Target: constant.SOLUTION,
			Idc:    idc,
		}
		tx := conn.Begin()
		// 檢查餘額
		var remain int
		if err := tx.Model(&poll).Select("SUM(idc)").
			Where("sid=? AND uid=?", poll.Sid, poll.Uid).
			Scan(&remain).Error; err != nil {
			tx.Rollback()
			return err
		}
		// sol餘額不足
		if remain < int(-poll.Idc) {
			tx.Rollback()
			return errors.New(errs.ErrNoEnoughIDC)
		}
		if err := tx.Create(&poll).Error; err != nil {
			tx.Rollback()
			return err
		}

		if err := tx.Commit().Error; err != nil {
			return err
		}

		return nil
	default:
		return nil
	}
}
func Deposit(conn *gorm.DB, target int32, id uint32, idc float32, uid string) error {
	switch target {
	case constant.IDEA:
		// 存入Idea
		poll := &schemas.Pollings{
			Iid:    id,
			Sid:    0,
			Uid:    uid,
			Target: constant.IDEA,
			Idc:    idc,
		}
		if err := conn.Create(&poll).Error; err != nil {
			return err
		}
		return nil

	case constant.SOLUTION:
		// Search iid of this sid
		var iid uint32
		if err := conn.Model(&schemas.Pollings{}).
			Select("iid").
			Where("sid = ?", id).Scan(&iid).Error; err != nil {
			return err
		}
		// 存入Sol
		poll := &schemas.Pollings{
			Iid:    iid,
			Sid:    id,
			Uid:    uid,
			Target: constant.SOLUTION,
			Idc:    idc,
		}
		if err := conn.Create(&poll).Error; err != nil {
			return err
		}
		return nil
	default:
		return nil
	}
}

// given iid
// 	1. get all polls(idc) of all solutions
// 	2. calculate sum of all idc
//  3. update winner's sid in idea table
func GetPollratesAndUpdateWinner(conn *gorm.DB, iid uint32) (float32, []*gov.PollRate, error) {

	rates := []*gov.PollRate{}
	rows, err := conn.Model(&schemas.Pollings{}).
		Select("SUM(pollings.idc) as total, solutions.uid as uid").
		Where("solutions.iid=?", iid).
		Group("uid").Order("total DESC").
		Joins("left join solutions on pollings.sid = solutions.sid").Rows()

	if err != nil {
		return 0, nil, err
	}

	defer rows.Close()
	var sids []uint32
	var sum float32
	for rows.Next() {
		var uid string
		var sid uint32
		var idc float32
		rows.Scan(&idc, &uid)
		sum = sum + idc
		rates = append(rates, &gov.PollRate{
			Uid: uid,
			Idc: idc,
		})
		sids = append(sids, sid)
	}
	var winnerSid uint32 = 0
	if len(sids) != 0 {
		winnerSid = sids[0]
	}

	if err := conn.Model(&schemas.Ideas{}).Where("iid = ?", iid).
		Select("winnerSid", "phase").
		Updates(&schemas.Ideas{WinnerSid: winnerSid, Phase: constants.Phase_Trading}).Error; err != nil {
		return 0, nil, err
	}

	return sum, rates, nil
}

func ChangePhaseToP2T(conn *gorm.DB, iid uint32) error {
	if err := conn.Model(&schemas.Ideas{}).Where("iid = ?", iid).
		Select("phase").
		Updates(&schemas.Ideas{Phase: constants.Phase_Trading}).Error; err != nil {
		return err
	}
	return nil
}
