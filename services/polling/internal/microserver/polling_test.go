package pollingserver

import (
	"context"
	pb "ideal-town/api/proto/v1/polling"
	errs "ideal-town/pkg/errors"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"ideal-town/services/polling/constant"
	mock "ideal-town/services/polling/internal/microserver/mock"
	"ideal-town/services/polling/internal/microserver/pollingdb"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const ConnByteSize = 1024 * 1024
const DialTarget = ""

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent{
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}
func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Pollings{}, &schemas.Ideas{}, &schemas.Solutions{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()

	pb.RegisterPollingServer(server, &PollingServer{
		Bank:       &mock.BankClient{},
		Government: &mock.GovernmentClient{},
		Idea:       &mock.IdeaClient{},
		Agent:  agent,
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createPollingClient(t *testing.T) (context.Context, *grpc.ClientConn, pb.PollingClient) {
	resetTable(agent)
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pb.NewPollingClient(conn)
	return ctx, conn, client
}

func TestPollingServer_Pollings(t *testing.T) {
	type args struct {
		polling []schemas.Pollings
		iid     uint32
		sid     uint32
		uid     string
	}
	tests := []struct {
		name string
		args args
		got  [][]pb.PollingInfo //0:all 1:uid 2:iid 3:sid
		err  error
	}{
		{
			"[v] Valid",
			args{
				polling: []schemas.Pollings{
					{
						Pid:    1,
						Uid:    "a",
						Iid:    1,
						Sid:    2,
						Target: constant.IDEA,
						Idc:    30,
					}, {
						Pid:    2,
						Uid:    "b",
						Iid:    2,
						Sid:    2,
						Target: constant.SOLUTION,
						Idc:    20,
					}, {
						Pid:    3,
						Uid:    "a",
						Iid:    2,
						Sid:    3,
						Target: constant.SOLUTION,
						Idc:    10},
				},
				iid: 2,
				uid: "a",
				sid: 3,
			},
			[][]pb.PollingInfo{
				{{
					Pid:    1,
					Uid:    "a",
					Iid:    1,
					Sid:    2,
					Target: constant.IDEA,
					Idc:    30,
				}, {
					Pid:    2,
					Uid:    "b",
					Iid:    2,
					Sid:    2,
					Target: constant.SOLUTION,
					Idc:    20,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    2,
					Uid:    "b",
					Iid:    2,
					Sid:    2,
					Target: constant.SOLUTION,
					Idc:    20,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    1,
					Uid:    "a",
					Iid:    1,
					Sid:    2,
					Target: constant.IDEA,
					Idc:    30,
				}, {
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
				{{
					Pid:    3,
					Uid:    "a",
					Iid:    2,
					Sid:    3,
					Target: constant.SOLUTION,
					Idc:    10},
				},
			},
			nil,
		},
	}
	ctx, conn, client := createPollingClient(t)
	defer conn.Close()
	resetTable(&agent.Agent)
	defer resetTable(&agent.Agent)

	// Test: GetPolling
	for _, tt := range tests {
		resetTable(&agent.Agent)
		defer resetTable(&agent.Agent)
		t.Run(tt.name, func(t *testing.T) {
			agent.Conn.Create(tt.args.polling)
			got, err := client.GetPolling(ctx, &pb.PollingRequest{})
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[0]), len(got.PollingInfo))

			got, err = client.GetPollingByUid(ctx, &pb.PollingRequest{Uid: tt.args.uid})
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[1]), len(got.PollingInfo))
			got, err = client.GetPollingByIid(ctx, &pb.PollingRequest{Iid: tt.args.iid})
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[2]), len(got.PollingInfo))
			got, err = client.GetPollingBySid(ctx, &pb.PollingRequest{Sid: tt.args.sid})
			assert.Equal(t, tt.err, err)
			assert.Equal(t, len(tt.got[3]), len(got.PollingInfo))

		})
	}
}
func TestPollingServer_TransferW2I(t *testing.T) {

	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Wallet doesn't have enough money",
			"BadMan",
			100000,
			1,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			2,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			2,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			2,
			10,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}

	ctx, conn, client := createPollingClient(t)
	resetTable(&agent.Agent)
	defer conn.Close()
	defer resetTable(&agent.Agent)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}
			_, err := client.TransferW2I(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}
func TestPollingServer_TransferW2S(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Wallet doesn't have enough money",
			"BadMan",
			100000,
			1,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			2,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			2,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			2,
			10,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	resetTable(&agent.Agent)
	defer conn.Close()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferW2S(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}
func TestPollingServer_TransferI2S(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Idea doesn't have enough money",
			"BadMan",
			1000,
			1,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			2,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			2,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			10,
			10,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	resetTable(&agent.Agent)
	ctx, conn, client := createPollingClient(t)
	defer conn.Close()

	agent.Deposit(constant.IDEA, 1, 900, "Badman")
	agent.Deposit(constant.IDEA, 2, 25, "B")

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferI2S(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestPollingServer_TransferI2W(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Idea doesn't have enough money",
			"BadMan",
			1000,
			1,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			2,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			2,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			11,
			2,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	resetTable(&agent.Agent)
	defer conn.Close()

	agent.Deposit(constant.IDEA, 1, 900, "Badman")
	agent.Deposit(constant.IDEA, 2, 25, "B")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferI2W(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestPollingServer_TransferS2W(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Idea doesn't have enough money",
			"BadMan",
			1000,
			1,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			2,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			2,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			11,
			2,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	resetTable(&agent.Agent)
	defer conn.Close()

	agent.Deposit(constant.SOLUTION, 1, 900, "Badman")
	agent.Deposit(constant.SOLUTION, 2, 25, "B")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferS2W(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestPolling_TransferS2Winner(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Idea doesn't have enough money",
			"BadMan",
			1000,
			10,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			12,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			12,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			8,
			1,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	defer conn.Close()
	resetTable(&agent.Agent)

	agent.Deposit(constant.SOLUTION, 10, 900, "Badman")
	agent.Deposit(constant.SOLUTION, 12, 25, "B")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferS2Winner(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestPolling_TransferI2Winner(t *testing.T) {
	tests := []struct {
		name string
		// test data
		uid   string
		idc   float32
		srcId uint32
		dstId uint32
		//
		err error
	}{
		{
			"[x] Idea doesn't have enough money",
			"BadMan",
			1000,
			10,
			2,
			status.Error(codes.Aborted, errs.ErrNoEnoughIDC),
		},
		{
			"[x] negative IDC",
			"A",
			-20.3,
			12,
			3,
			status.Error(codes.Aborted, errs.ErrExpectedPositive),
		},
		{
			"[v] valid request",
			"B",
			20.3,
			12,
			3,
			status.Error(codes.OK, ""),
		},
		{
			"[x] invalid stage",
			"B",
			20.3,
			8,
			1,
			status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	resetTable(&agent.Agent)
	defer conn.Close()

	agent.Deposit(constant.IDEA, 10, 900, "Badman")
	agent.Deposit(constant.IDEA, 12, 25, "B")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransferRequest{
				Uid:   tt.uid,
				Idc:   tt.idc,
				SrcId: tt.srcId,
				DstId: tt.dstId,
			}

			_, err := client.TransferI2Winner(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}

func TestPolling_TransitionToTrading(t *testing.T) {
	tests := []struct {
		name string
		// test data
		iid uint32
		//
		err error
	}{
		{
			name: "[v] valid",
			iid:  12,
			err:  nil,
		},
		{
			name: "[v] phase not correct",
			iid:  5,
			err:  status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
		{
			name: "[v] phase not correct",
			iid:  11,
			err:  status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}

	ctx, conn, client := createPollingClient(t)
	defer conn.Close()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransitionRequest{
				Iid: tt.iid,
			}

			_, err := client.TransitionToTrading(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}
func TestPolling2TradingServer_TransitionToBeforeTrading(t *testing.T) {
	tests := []struct {
		name string
		// test data
		iid uint32
		//
		err error
	}{
		{
			name: "[v] valid",
			iid:  5,
			err:  nil,
		},
		{
			name: "[v] phase not correct",
			iid:  10,
			err:  status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
		{
			name: "[v] phase not correct",
			iid:  11,
			err:  status.Error(codes.Aborted, errs.ErrStageNotReached),
		},
	}
	ctx, conn, client := createPollingClient(t)
	defer conn.Close()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := &pb.TransitionRequest{
				Iid: tt.iid,
			}

			_, err := client.TransitionToBeforeTrading(ctx, request)
			assert.Equal(t, tt.err, err)
		})
	}
}
