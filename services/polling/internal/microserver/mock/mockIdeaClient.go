package mock

import (
	"context"
	common "ideal-town/api/proto/v1/common"
	idea "ideal-town/api/proto/v1/idea"
	"ideal-town/pkg/constants"

	"google.golang.org/grpc"
)

type IdeaClient struct {
}

func (*IdeaClient) GetIdeaPhase(ctx context.Context, req *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseReply, error) {
	switch req.Id {
	case 10:
		return &idea.PhaseReply{Phase: constants.Phase_P2T}, nil
	case 12:
		return &idea.PhaseReply{Phase: constants.Phase_P2T}, nil
	case 11:
		return &idea.PhaseReply{Phase: constants.Phase_Trading}, nil
	default:
		return &idea.PhaseReply{Phase: constants.Phase_Polling}, nil
	}
}
func (*IdeaClient) GetSolutionPhase(ctx context.Context, req *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseReply, error) {
	switch req.Id {
	case 10:
		return &idea.PhaseReply{Phase: constants.Phase_P2T}, nil
	case 12:
		return &idea.PhaseReply{Phase: constants.Phase_P2T}, nil
	case 11:
		return &idea.PhaseReply{Phase: constants.Phase_Trading}, nil
	default:
		return &idea.PhaseReply{Phase: constants.Phase_Polling}, nil
	}
}

func (m *IdeaClient) CreateIdea(ctx context.Context, in *idea.IdeaInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByID(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfo, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByUser(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaInfoByPhase(ctx context.Context, in *idea.IdeaRequest, opts ...grpc.CallOption) (*idea.IdeaInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetIdeaWinner(ctx context.Context, in *idea.PhaseRequest, opts ...grpc.CallOption) (*idea.PhaseRequest, error) {
	return nil, nil
}
func (m *IdeaClient) UpdateIdea(ctx context.Context, in *idea.UpdateRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) CreateSolution(ctx context.Context, in *idea.SolutionInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByID(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfo, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByUser(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) GetSolutionInfoByIdea(ctx context.Context, in *idea.SolutionRequest, opts ...grpc.CallOption) (*idea.SolutionInfoList, error) {
	return nil, nil
}
func (m *IdeaClient) UpdateSolution(ctx context.Context, in *idea.UpdateRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
