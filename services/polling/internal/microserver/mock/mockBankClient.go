package mock

import (
	"context"
	bk "ideal-town/api/proto/v1/bank"
	"ideal-town/api/proto/v1/common"
	errs "ideal-town/pkg/errors"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BankClient struct {
}

func (m *BankClient) CreateWallet(ctx context.Context, in *bk.IDCRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

func (m *BankClient) GetIDC(ctx context.Context, in *bk.IDCRequest, opts ...grpc.CallOption) (*bk.IDCReply, error) {
	return &bk.IDCReply{Idc: 0}, nil
}

func (m *BankClient) Deposit(ctx context.Context, in *bk.BankRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

func (m *BankClient) Withdraw(ctx context.Context, in *bk.BankRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	return &common.Empty{}, nil
}

func (m *BankClient) Lock(ctx context.Context, in *bk.LockRequest, opts ...grpc.CallOption) (*bk.LidInfo, error) {
	switch in.Uid {
	case "BadMan":
		return nil, status.Error(codes.Aborted, errs.ErrNoEnoughIDC)
	}
	return nil, nil
}

func (m *BankClient) Unlock(ctx context.Context, in *bk.LidInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}

func (m *BankClient) Confirm(ctx context.Context, in *bk.LidInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	return nil, nil
}
