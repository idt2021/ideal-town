package pollingserver

import (
	"context"
	bk "ideal-town/api/proto/v1/bank"
	"ideal-town/api/proto/v1/common"
	gov "ideal-town/api/proto/v1/government"
	pi "ideal-town/api/proto/v1/idea"
	pb "ideal-town/api/proto/v1/polling"
	"ideal-town/pkg/constants"
	"ideal-town/pkg/errors"
	"ideal-town/pkg/shared/sqlmanager"
	"ideal-town/services/polling/constant"
	pdb "ideal-town/services/polling/internal/microserver/pollingdb"
	"log"

	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/*
mock:
	- Bank
		rpc Withdraw(BankRequest) returns (BankReply) {}
		rpc Deposit(BankRequest) returns (BankReply) {}
*/
type PollingServer struct {
	Agent 	   *sqlmanager.Agent
	Bank       bk.BankClient
	Government gov.GovernmentClient
	Idea       pi.IdeaClient
	pb.UnimplementedPollingServer
}

func (p *PollingServer) GetPolling(ctx context.Context, in *pb.PollingRequest) (*pb.PollingReply, error) {
	got, err := pdb.GetPollings(p.Agent.Conn)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.PollingReply{PollingInfo: got}, nil
}
func (p *PollingServer) GetPollingByIid(ctx context.Context, in *pb.PollingRequest) (*pb.PollingReply, error) {
	got, err := pdb.GetPollingByIid(p.Agent.Conn, in.Iid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.PollingReply{PollingInfo: got}, nil
}
func (p *PollingServer) GetPollingBySid(ctx context.Context, in *pb.PollingRequest) (*pb.PollingReply, error) {
	got, err := pdb.GetPollingBySid(p.Agent.Conn, in.Sid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.PollingReply{PollingInfo: got}, nil
}
func (p *PollingServer) GetPollingByUid(ctx context.Context, in *pb.PollingRequest) (*pb.PollingReply, error) {
	got, err := pdb.GetPollingByUid(p.Agent.Conn, in.Uid)
	if err != nil {
		return nil, status.Error(codes.Aborted, err.Error())
	}
	return &pb.PollingReply{PollingInfo: got}, nil
}

// polling階段才可以使用
func (p *PollingServer) TransferW2I(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	log.Printf("Request received, transfer from %s to idea %d", req.GetUid(), req.GetDstId())

	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.DstId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}

	tx := p.Agent.Conn.Begin()
	{
		request := &bk.BankRequest{
			Uid:    req.Uid,
			Action: common.Action_Withdraw,
			Remark: strconv.Itoa(int(req.DstId)),
			Idc:    req.Idc,
		}
		tid, err := p.Bank.Lock(context.Background(), &bk.LockRequest{Uid: req.Uid, Idc: req.Idc})
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, errors.ErrNoEnoughIDC)
		}
		_, err = p.Bank.Withdraw(ctx, request)
		if err != nil {
			tx.Rollback()
			p.Bank.Unlock(context.Background(), &bk.LidInfo{Lid: tid.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		err = pdb.Deposit(p.Agent.Conn, constant.IDEA, req.DstId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			p.Bank.Unlock(context.Background(), &bk.LidInfo{Lid: tid.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	return &common.Empty{}, nil

}
func (p *PollingServer) TransferW2S(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	log.Printf("Request received, transfer from %s to solution %d", req.GetUid(), req.GetDstId())

	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}

	reply, err := p.Idea.GetSolutionPhase(ctx, &pi.PhaseRequest{Id: req.DstId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		request := &bk.BankRequest{
			Uid:    req.Uid,
			Action: common.Action_Withdraw,
			Remark: strconv.Itoa(int(req.DstId)),
			Idc:    req.Idc,
		}
		tid, err := p.Bank.Lock(context.Background(), &bk.LockRequest{Uid: req.Uid, Idc: req.Idc})
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, errors.ErrNoEnoughIDC)
		}

		_, err = p.Bank.Withdraw(ctx, request)
		if err != nil {
			tx.Rollback()
			p.Bank.Unlock(context.Background(), &bk.LidInfo{Lid: tid.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		err = pdb.Deposit(p.Agent.Conn, constant.SOLUTION, req.DstId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			p.Bank.Unlock(context.Background(), &bk.LidInfo{Lid: tid.GetLid()})
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	return &common.Empty{}, nil
}

func (p *PollingServer) TransferI2S(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.SrcId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		err := pdb.Withdraw(p.Agent.Conn, constant.IDEA, req.SrcId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		err = pdb.Deposit(p.Agent.Conn, constant.SOLUTION, req.DstId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}

func (p *PollingServer) TransferI2W(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.SrcId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase == constants.Phase_Trading {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		err := pdb.Withdraw(p.Agent.Conn, constant.IDEA, req.SrcId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		feeRequest := &gov.FeeRequest{
			Spend: req.Idc,
		}
		reply, err := p.Government.GetFee(ctx, feeRequest)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		request := &bk.BankRequest{
			Uid:    req.Uid,
			Action: common.Action_Deposit,
			Remark: strconv.Itoa(int(req.SrcId)),
			Idc:    req.Idc - reply.GetFee(),
		}
		_, err = p.Bank.Deposit(ctx, request)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
func (p *PollingServer) TransferS2W(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetSolutionPhase(ctx, &pi.PhaseRequest{Id: req.SrcId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase == constants.Phase_Trading {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		err := pdb.Withdraw(p.Agent.Conn, constant.SOLUTION, req.SrcId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		feeRequest := &gov.FeeRequest{
			Spend: req.Idc,
		}
		reply, err := p.Government.GetFee(ctx, feeRequest)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		request := &bk.BankRequest{
			Uid:    req.Uid,
			Action: common.Action_Deposit,
			Remark: strconv.Itoa(int(req.SrcId)),
			Idc:    req.Idc - reply.Fee,
		}
		_, err = p.Bank.Deposit(ctx, request)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
func (p *PollingServer) TransferS2Winner(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetSolutionPhase(ctx, &pi.PhaseRequest{Id: req.SrcId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_P2T {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		err := pdb.Withdraw(p.Agent.Conn, constant.SOLUTION, req.SrcId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		err = pdb.Deposit(p.Agent.Conn, constant.SOLUTION, req.DstId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
func (p *PollingServer) TransferI2Winner(ctx context.Context, req *pb.TransferRequest) (*common.Empty, error) {
	if req.Idc < 0 {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrExpectedPositive)
	}
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.SrcId})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_P2T {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{
		err := pdb.Withdraw(p.Agent.Conn, constant.IDEA, req.SrcId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
		err = pdb.Deposit(p.Agent.Conn, constant.SOLUTION, req.DstId, req.Idc, req.Uid)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
}
func (p *PollingServer) TransitionToTrading(ctx context.Context, req *pb.TransitionRequest) (*common.Empty, error) {
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.Iid})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_P2T {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	tx := p.Agent.Conn.Begin()
	{

		total, pollrates, err := pdb.GetPollratesAndUpdateWinner(p.Agent.Conn, req.GetIid())
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}

		request := &gov.PollRewardRequest{
			Total:    total,
			Pollrate: pollrates,
		}
		_, err = p.Government.DistributePollReward(ctx, request)
		if err != nil {
			tx.Rollback()
			return &common.Empty{}, status.Error(codes.Aborted, err.Error())
		}
	}
	// End of transaction
	if err := tx.Commit().Error; err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, nil
	//

}
func (p *PollingServer) TransitionToBeforeTrading(ctx context.Context, req *pb.TransitionRequest) (*common.Empty, error) {
	reply, err := p.Idea.GetIdeaPhase(ctx, &pi.PhaseRequest{Id: req.Iid})
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	if err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	if reply.Phase != constants.Phase_Polling {
		return &common.Empty{}, status.Error(codes.Aborted, errors.ErrStageNotReached)
	}
	pdb.ChangePhaseToP2T(p.Agent.Conn, req.Iid)
	return &common.Empty{}, nil
}
