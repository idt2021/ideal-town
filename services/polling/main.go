package main

import (
	"context"
	"ideal-town/api/proto/v1/bank"
	"ideal-town/api/proto/v1/idea"
	gw "ideal-town/api/proto/v1/polling"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	mock "ideal-town/services/polling/internal/microserver/mock"
	// bank "ideal-town/services/bank/internal/microserver"
	// idea "ideal-town/services/idea/internal/microserver"
	poll "ideal-town/services/polling/internal/microserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":9889"
	httpServerEndpoint = ":10001"
)

func NewBankClient() (bank.BankClient) {
	conn, err := grpc.Dial("localhost:5050", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
	log.Fatalf("failed to dial: %v", err)
	}

	// defer conn.Close()

	client := bank.NewBankClient(conn)
	return client
}

func NewIdeaClient() (idea.IdeaClient) {
	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
	log.Fatalf("failed to dial: %v", err)
	}

	// defer conn.Close()

	client := idea.NewIdeaClient(conn)

	return client
}

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	// mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    //     w.Header().Set("Content-Type", "application/json")
    //     w.Write([]byte("{\"hello\": \"world\"}"))
    // })
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterPollingHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	s := grpc.NewServer()
	IClient := NewIdeaClient()
	BClient := NewBankClient()
	gw.RegisterPollingServer(s, &poll.PollingServer{
		Agent: agent,
		Idea: IClient, 
		Bank: BClient,
		Government: &mock.GovernmentClient{},
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}