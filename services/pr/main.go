package main

import (
	"context"
	"ideal-town/api/proto/v1/idea"
	gw "ideal-town/api/proto/v1/pr"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	pr "ideal-town/services/pr/internal/prserver"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	// "ideal-town/pkg/schemas"
)

const (
	grpcServerEndpoint = ":7007"
	httpServerEndpoint = ":10101"
)

func NewIdeaClient() idea.IdeaClient {
	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
	}

	// defer conn.Close()

	client := idea.NewIdeaClient(conn)

	return client
}

func httpServer() {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	handler := cors.Default().Handler(mux)
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterPullRequestHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		log.Fatalln(err.Error())
	}
	log.Fatalln(http.ListenAndServe(httpServerEndpoint, handler))
}

func grpcServer() {
	ln, err := net.Listen("tcp", grpcServerEndpoint)
	if err != nil {
		log.Fatalln(err.Error())
	}
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "root",
		Password: "idtps",
		Host:     "140.113.67.110",
		Port:     "5211",
		DBName:   "geopolitical",
	})
	s := grpc.NewServer()
	IClient := NewIdeaClient()
	gw.RegisterPullRequestServer(s, &pr.PRServer{
		Agent: agent,
		Idea:  IClient,
	})
	log.Fatalln(s.Serve(ln))
}

func main() {
	go grpcServer()
	httpServer()
}
