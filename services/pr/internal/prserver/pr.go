package pr

import (
	"context"
	"log"
	common "ideal-town/api/proto/v1/common"
	idea "ideal-town/api/proto/v1/idea"
	pr "ideal-town/api/proto/v1/pr"
	errs "ideal-town/pkg/errors"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	db "ideal-town/services/pr/internal/prserver/prdb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PRServer struct {
	Agent *sqlmanager.Agent
	Idea  idea.IdeaClient
	pr.UnimplementedPullRequestServer
}

func (s *PRServer) CreatePR(ctx context.Context, in *pr.PRRequest) (*common.Empty, error) {
	iid := in.GetIid()
	uid := in.GetUid()
	link := in.GetLink()
	gid := in.GetGid()
	phase, err := s.Idea.GetIdeaPhase(context.Background(), &idea.PhaseRequest{Id: iid})
	if err != nil {
		return &common.Empty{}, err
	}
	if phase.GetPhase() != 2 {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrStageNotReached)
	}
	if gid == "" {
		return &common.Empty{}, status.Error(codes.Aborted, errs.ErrMustHaveGitlabID)
	}
	err = db.CreatePR(s.Agent.Conn, iid, uid, link, gid)
	if err != nil {
		return &common.Empty{}, err
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *PRServer) GetPRByIdea(ctx context.Context, in *pr.GetPR) (*pr.PRList, error) {
	log.Printf("Request received, iid: %d", in.GetIid())

	iid := in.GetIid()
	res, err := db.GetPRList(s.Agent.Conn, iid, "")
	if err != nil {
		return &pr.PRList{}, status.Error(codes.Aborted, err.Error())
	}

	return &pr.PRList{Pr: res}, status.Error(codes.OK, "")
}

func (s *PRServer) GetPRByUser(ctx context.Context, in *pr.GetPR) (*pr.PRList, error) {
	uid := in.GetUid()
	res, err := db.GetPRList(s.Agent.Conn, 0, uid)
	if err != nil {
		return &pr.PRList{}, status.Error(codes.Aborted, err.Error())
	}

	return &pr.PRList{Pr: res}, status.Error(codes.OK, "")
}

func (s *PRServer) GetPRByID(ctx context.Context, in *pr.GetPR) (*pr.PR, error) {
	rid := in.Rid
	res, err := db.GetPR(s.Agent.Conn, rid)
	if err != nil {
		return &pr.PR{}, status.Error(codes.Aborted, err.Error())
	}

	return res, status.Error(codes.OK, "")
}

func (s *PRServer) UpdatePR(ctx context.Context, in *pr.PR) (*common.Empty, error) {
	rid := in.GetRid()
	if err := db.UpdatePR(s.Agent.Conn, rid, in.GetLink()); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}
	return &common.Empty{}, status.Error(codes.OK, "")
}

func (s *PRServer) DeletePR(ctx context.Context, in *pr.GetPR) (*common.Empty, error) {
	rid := in.Rid
	if _, err := s.GetPRByID(context.Background(), in); err != nil {
		return &common.Empty{}, err
	}
	if err := db.DeletePR(s.Agent.Conn, rid); err != nil {
		return &common.Empty{}, status.Error(codes.Aborted, err.Error())
	}

	return &common.Empty{}, status.Error(codes.OK, "")
}
