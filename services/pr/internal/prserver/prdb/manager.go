package prdb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/pr"
	"ideal-town/pkg/schemas"

	"gorm.io/gorm"
)

func CreatePR(conn *gorm.DB, iid uint32, uid string, link string, gid string) error {
	if err := conn.Create(&schemas.Prs{Iid: iid, Uid: uid, Link: link, Gid: gid}).Error; err != nil {
		return err
	}
	return nil
}

func GetPR(conn *gorm.DB, rid uint32) (*pb.PR, error) {
	var result pb.PR
	var msg schemas.Prs
	if err := conn.Where(&schemas.Prs{Rid: rid}).Take(&msg).Error; err != nil {
		return nil, errors.New(errs.ErrPullRequestNotFound)
	}
	result = pb.PR{Rid: msg.Rid, Iid: msg.Iid, Uid: msg.Uid, Link: msg.Link, Gid: msg.Gid}
	return &result, nil
}

func GetPRList(conn *gorm.DB, iid uint32, uid string) ([]*pb.PR, error) {
	var msg []schemas.Prs
	var res []*pb.PR
	if iid != 0 {
		conn.Where(&schemas.Prs{Iid: iid}).Find(&msg)
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.PR{
				Rid:    element.Rid,
				Iid:    element.Iid,
				Uid:    element.Uid,
				Link:   element.Link,
				Gid: 	element.Gid,
			})
		}
	} else {
		conn.Where(&schemas.Prs{Uid: uid}).Find(&msg)
		for _, element := range msg {
			// index is the index where we are
			// element is the element from someSlice for where we are
			res = append(res, &pb.PR{
				Rid:    element.Rid,
				Iid:    element.Iid,
				Uid:    element.Uid,
				Link:   element.Link,
				Gid: 	element.Gid,
			})
		}
	}
	return res, nil
}

func UpdatePR(conn *gorm.DB, rid uint32, content string) error {
	var temp *schemas.Prs
	if err := conn.Model(&schemas.Prs{}).Where("rid=?", rid).Take(&temp).Error; err != nil {
		return errors.New(errs.ErrPullRequestNotFound)
	}
	if err := conn.Model(&schemas.Prs{}).Where("rid=?", rid).
		Select("link").
		Updates(&schemas.Prs{Link: content}).Error; err != nil {
		return err
	}
	return nil
}

func DeletePR(conn *gorm.DB, rid uint32) error{
	if err := conn.Where("rid=?", rid).Delete(&schemas.Prs{}).Error; err != nil {
		return err
	}
	return nil
}