package prdb

import (
	"errors"
	errs "ideal-town/pkg/errors"
	pb "ideal-town/api/proto/v1/pr"
	"ideal-town/pkg/schemas"
	"ideal-town/pkg/shared/sqlmanager"
	"testing"

	"github.com/stretchr/testify/assert"
)

var agent = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	defer resetTable(agent)

	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Prs{})
}

func TestCreatePR(t *testing.T) {
	tests := []struct {
		name    string
		iid     uint32
		uid     string
		link	string
		expErr  error
	}{
		{"[v] valid", 2, "user", "a gitlab link", nil},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})
	
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Run(tt.name, func(t *testing.T) {
				err := CreatePR(agent.Conn, tt.iid, tt.uid, tt.link)
				assert.Equal(t, tt.expErr, err)
			})
		})
	}
}

func TestGetPR(t *testing.T) {
	tests := []struct {
		name    string
		rid		uint32
		res     *pb.PR
		expErr 	error
	}{
		{"[v] valid", 1, &pb.PR{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"}, nil},
		{"[x] pull request not found", 10, nil, errors.New(errs.ErrPullRequestNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Run(tt.name, func(t *testing.T) {
				msg, err := GetPR(agent.Conn, tt.rid)
				assert.Equal(t, tt.expErr, err)
				assert.Equal(t, tt.res, msg)
			})
		})
	}
}

func TestGetPRList(t *testing.T) {
	tests := []struct {
		name    string
		iid 	uint32
		uid 	string
		res    	int
		expErr 	error
	}{
		{"[v] valid", 1, "", 2, nil},
		{"[v] valid", 0, "user", 2, nil},
		{"[x] pull request not found(idea)", 10, "", 0, errors.New(errs.ErrPullRequestNotFound)},
		{"[x] pull request not found(user)", 0, "user3", 0, errors.New(errs.ErrPullRequestNotFound)},
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Prs{Rid: 3, Iid: 2, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Run(tt.name, func(t *testing.T) {
				msg, err := GetPRList(agent.Conn, tt.iid, tt.uid)
				assert.Equal(t, tt.expErr, err)
				assert.Equal(t, tt.res, len(msg))
			})
		})
	}
}

func TestDeletePR(t *testing.T) {
	tests := []struct {
		name    string
		rid     uint32
		expErr 	error
	}{
		{"[v] valid", 1, nil},
		{"[v] 亂刪", 10, nil}, //除非db有問題 刪不存在的也不會報錯
	}

	defer resetTable(agent)
	agent.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	agent.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Run(tt.name, func(t *testing.T) {
				err := DeletePR(agent.Conn, tt.rid)
				assert.Equal(t, tt.expErr, err)
			})
		})
	}
}
