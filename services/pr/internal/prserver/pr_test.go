package pr

import (
	"context"
	// common "ideal-town/api/proto/v1/common"
	pr "ideal-town/api/proto/v1/pr"
	errs "ideal-town/pkg/errors"
	mock "ideal-town/services/pr/internal/prserver/mock"
	sqlmanager "ideal-town/pkg/shared/sqlmanager"
	"ideal-town/pkg/schemas"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

const DialTarget = "bufnet"

var dbManager = initUnsafeTradeAgent()

func initUnsafeTradeAgent() *sqlmanager.Agent {
	agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	resetTable(agent)
	return agent
}

func resetTable(agent *sqlmanager.Agent) {
	agent.ResetDB(&schemas.Prs{})
}

func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		Username: "test",
		Password: "test",
		Host:     "140.113.67.121",
		Port:     "3306",
		DBName:   "geopolitical",
	})
	pr.RegisterPullRequestServer(server, &PRServer{
		Idea: 	&mock.IdeaClient{},
		Agent:	agent,
	})

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, pr.PullRequestClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, 
        grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pr.NewPullRequestClient(conn)
	return ctx, conn, client
}


func TestPRServer_CreatePR(t *testing.T) {
	tests := []struct {
		name   string
		req    *pr.PRRequest
		expErr error
	}{
		{"[x] idea not found", &pr.PRRequest{Iid: 10}, status.Error(codes.Aborted, errs.ErrIdeaNotFound)},
		{"[x] idea at wrong stage", &pr.PRRequest{Iid: 4}, status.Error(codes.Aborted, errs.ErrStageNotReached)},
		{"[v] valid", &pr.PRRequest{Iid: 2, Uid: "user", Link: "a gitlab link"}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	dbManager.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.CreatePR(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			// assert.Equal(t, tt.res, msg)
		})
	}
}

func TestPRServer_GetPRByIdea(t *testing.T) {
	

	tests := []struct {
		name   string
		req    *pr.GetPR
		res    int
		expErr error
	}{
		{"[x] idea has no pull request", &pr.GetPR{Iid: 10}, 0, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[v] valid", &pr.GetPR{Iid: 1}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetPRByIdea(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetPr()))
		})
	}
}

func TestPRServer_GetPRByUser(t *testing.T) {
	tests := []struct {
		name   string
		req    *pr.GetPR
		res    int
		expErr error
	}{
		{"[x] user has no pull request", &pr.GetPR{Uid: "nobody"}, 0, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[v] valid", &pr.GetPR{Uid: "user"}, 2, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 3, Iid: 2, Uid: "user", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetPRByUser(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res, len(msg.GetPr()))
		})
	}
}

func TestPRServer_GetPRByID(t *testing.T) {
	tests := []struct {
		name   string
		req    *pr.GetPR
		res    *pr.PR
		expErr error
	}{
		{"[x] pull request not found", &pr.GetPR{Rid: 10}, &pr.PR{}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[v] valid", &pr.GetPR{Rid: 1}, &pr.PR{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)

	dbManager.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msg, err := client.GetPRByID(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
			assert.Equal(t, tt.res.GetRid(), msg.GetRid())
		})
	}
}

func TestPRServer_DeletePR(t *testing.T) {
	tests := []struct {
		name   string
		req    *pr.GetPR
		expErr error
	}{
		{"[x] pull request not found", &pr.GetPR{Rid: 10}, status.Error(codes.Aborted, errs.ErrPullRequestNotFound)},
		{"[v] valid", &pr.GetPR{Rid: 1}, status.Error(codes.OK, "")},
	}

	ctx, conn, client := createConn(t)
	defer conn.Close()
	defer resetTable(dbManager)
	
	dbManager.Conn.Create(&schemas.Prs{Rid: 1, Iid: 1, Uid: "user", Link: "a gitlab link"})
	dbManager.Conn.Create(&schemas.Prs{Rid: 2, Iid: 1, Uid: "user2", Link: "a gitlab link"})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.DeletePR(ctx, tt.req)
			assert.Equal(t, tt.expErr, err)
		})
	}
}
