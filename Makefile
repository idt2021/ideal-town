compile-all: 
	make compile-common compile-bank compile-trading compile-government compile-idtbank compile-polling compile-pr compile-idea compile-charge compile-subs

compile-common:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative ./api/proto/v1/common/common.proto
	# protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/common/common.proto

compile-bank:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/bank/bank.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/bank/bank.proto

compile-trading:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/trading/trading.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/trading/trading.proto

compile-government:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/government/government.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/government/government.proto

compile-idtbank:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/idtbank/idtbank.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/idtbank/idtbank.proto

compile-polling:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/polling/polling.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/polling/polling.proto
	
compile-pr:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/pr/pr.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/pr/pr.proto
	

compile-idea:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/idea/idea.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/idea/idea.proto

compile-charge:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/charge/charge.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/charge/charge.proto

compile-subs:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/subs/subs.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/subs/subs.proto

compile-hello:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative  ./api/proto/v1/hello/hello.proto
	protoc --proto_path=. --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt allow_delete_body=true ./api/proto/v1/hello/hello.proto


compile-clean:
	rm -f api/proto/v1/common/common.pb.go
	rm -f api/proto/v1/bank/bank_grpc.pb.go api/proto/v1/bank/bank.pb.go
	rm -f api/proto/v1/trading/trading_grpc.pb.go api/proto/v1/trading/trading.pb.go
	rm -f api/proto/v1/government/government_grpc.pb.go api/proto/v1/government/government.pb.go
	rm -f api/proto/v1/idtbank/idtbank_grpc.pb.go api/proto/v1/idtbank/idtbank.pb.go
	rm -f api/proto/v1/polling/polling_grpc.pb.go api/proto/v1/polling/polling.pb.go
	rm -f api/proto/v1/pr/pr_grpc.pb.go api/proto/v1/pr/pr.pb.go
	rm -f api/proto/v1/idea/idea_grpc.pb.go api/proto/v1/idea/idea.pb.go
	rm -f api/proto/v1/charge/charge_grpc.pb.go api/proto/v1/charge/charge.pb.go
	rm -f api/proto/v1/subs/subs_grpc.pb.go api/proto/v1/subs/subs.pb.go

build-trading:
	go build services/trading/main.go -o build/trading

