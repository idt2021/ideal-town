// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package idtbank

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	common "ideal-town/api/proto/v1/common"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// IdtAssetClient is the client API for IdtAsset service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type IdtAssetClient interface {
	// 查看使用者在某個idea中擁有的IDT
	// arg: iid, uid
	GetAmountByUser(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReply, error)
	// 查看使用者在各個idea中擁有的IDT
	//arg: uid
	GetManyAmountByUser(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReplyList, error)
	// 查看某個idea中所有的IDT數量
	// arg: iid
	GetAmountByIdea(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReplyList, error)
	// 將IDT轉出IDT銀行
	//  1.  查看Assets中IDT餘額是否足夠&是否有鎖上
	//  2.  如果足夠，扣除IDT餘額，更新IdtRecords
	//  3.  同時更新Assets中的數量
	Withdraw(ctx context.Context, in *IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error)
	// 將IDT存入IDT銀行
	// 同時更新Assets中的數量
	Deposit(ctx context.Context, in *IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error)
	// 將IDT進行鎖定
	// 1. 檢查餘額是否足夠
	// 2. 在idtbank新增一筆 action是lock的紀錄 回傳id
	// 3. (這次先不會實踐)逾期沒有Confirm就會自動Unlock
	Lock(ctx context.Context, in *LockRequest, opts ...grpc.CallOption) (*IdInfo, error)
	// 發生錯誤時，將IDT解鎖
	// 1. 檢查bank是否存在此tid
	// 2. 將tid從lock中刪除
	Unlock(ctx context.Context, in *IdInfo, opts ...grpc.CallOption) (*common.Empty, error)
	// 執行成功，將IDT轉為支付紀錄
	// 1. 檢查是否存在id
	// 2. 將此id的action改為Withdraw
	Confirm(ctx context.Context, in *IdInfo, opts ...grpc.CallOption) (*common.Empty, error)
}

type idtAssetClient struct {
	cc grpc.ClientConnInterface
}

func NewIdtAssetClient(cc grpc.ClientConnInterface) IdtAssetClient {
	return &idtAssetClient{cc}
}

func (c *idtAssetClient) GetAmountByUser(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReply, error) {
	out := new(AmountReply)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/GetAmountByUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) GetManyAmountByUser(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReplyList, error) {
	out := new(AmountReplyList)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/GetManyAmountByUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) GetAmountByIdea(ctx context.Context, in *AmountRequest, opts ...grpc.CallOption) (*AmountReplyList, error) {
	out := new(AmountReplyList)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/GetAmountByIdea", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) Withdraw(ctx context.Context, in *IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	out := new(common.Empty)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/Withdraw", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) Deposit(ctx context.Context, in *IdtAssetRequest, opts ...grpc.CallOption) (*common.Empty, error) {
	out := new(common.Empty)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/Deposit", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) Lock(ctx context.Context, in *LockRequest, opts ...grpc.CallOption) (*IdInfo, error) {
	out := new(IdInfo)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/Lock", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) Unlock(ctx context.Context, in *IdInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	out := new(common.Empty)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/Unlock", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *idtAssetClient) Confirm(ctx context.Context, in *IdInfo, opts ...grpc.CallOption) (*common.Empty, error) {
	out := new(common.Empty)
	err := c.cc.Invoke(ctx, "/idtbank.IdtAsset/Confirm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// IdtAssetServer is the server API for IdtAsset service.
// All implementations must embed UnimplementedIdtAssetServer
// for forward compatibility
type IdtAssetServer interface {
	// 查看使用者在某個idea中擁有的IDT
	// arg: iid, uid
	GetAmountByUser(context.Context, *AmountRequest) (*AmountReply, error)
	// 查看使用者在各個idea中擁有的IDT
	//arg: uid
	GetManyAmountByUser(context.Context, *AmountRequest) (*AmountReplyList, error)
	// 查看某個idea中所有的IDT數量
	// arg: iid
	GetAmountByIdea(context.Context, *AmountRequest) (*AmountReplyList, error)
	// 將IDT轉出IDT銀行
	//  1.  查看Assets中IDT餘額是否足夠&是否有鎖上
	//  2.  如果足夠，扣除IDT餘額，更新IdtRecords
	//  3.  同時更新Assets中的數量
	Withdraw(context.Context, *IdtAssetRequest) (*common.Empty, error)
	// 將IDT存入IDT銀行
	// 同時更新Assets中的數量
	Deposit(context.Context, *IdtAssetRequest) (*common.Empty, error)
	// 將IDT進行鎖定
	// 1. 檢查餘額是否足夠
	// 2. 在idtbank新增一筆 action是lock的紀錄 回傳id
	// 3. (這次先不會實踐)逾期沒有Confirm就會自動Unlock
	Lock(context.Context, *LockRequest) (*IdInfo, error)
	// 發生錯誤時，將IDT解鎖
	// 1. 檢查bank是否存在此tid
	// 2. 將tid從lock中刪除
	Unlock(context.Context, *IdInfo) (*common.Empty, error)
	// 執行成功，將IDT轉為支付紀錄
	// 1. 檢查是否存在id
	// 2. 將此id的action改為Withdraw
	Confirm(context.Context, *IdInfo) (*common.Empty, error)
	mustEmbedUnimplementedIdtAssetServer()
}

// UnimplementedIdtAssetServer must be embedded to have forward compatible implementations.
type UnimplementedIdtAssetServer struct {
}

func (UnimplementedIdtAssetServer) GetAmountByUser(context.Context, *AmountRequest) (*AmountReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAmountByUser not implemented")
}
func (UnimplementedIdtAssetServer) GetManyAmountByUser(context.Context, *AmountRequest) (*AmountReplyList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetManyAmountByUser not implemented")
}
func (UnimplementedIdtAssetServer) GetAmountByIdea(context.Context, *AmountRequest) (*AmountReplyList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAmountByIdea not implemented")
}
func (UnimplementedIdtAssetServer) Withdraw(context.Context, *IdtAssetRequest) (*common.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Withdraw not implemented")
}
func (UnimplementedIdtAssetServer) Deposit(context.Context, *IdtAssetRequest) (*common.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Deposit not implemented")
}
func (UnimplementedIdtAssetServer) Lock(context.Context, *LockRequest) (*IdInfo, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Lock not implemented")
}
func (UnimplementedIdtAssetServer) Unlock(context.Context, *IdInfo) (*common.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Unlock not implemented")
}
func (UnimplementedIdtAssetServer) Confirm(context.Context, *IdInfo) (*common.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Confirm not implemented")
}
func (UnimplementedIdtAssetServer) mustEmbedUnimplementedIdtAssetServer() {}

// UnsafeIdtAssetServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to IdtAssetServer will
// result in compilation errors.
type UnsafeIdtAssetServer interface {
	mustEmbedUnimplementedIdtAssetServer()
}

func RegisterIdtAssetServer(s grpc.ServiceRegistrar, srv IdtAssetServer) {
	s.RegisterService(&IdtAsset_ServiceDesc, srv)
}

func _IdtAsset_GetAmountByUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AmountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).GetAmountByUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/GetAmountByUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).GetAmountByUser(ctx, req.(*AmountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_GetManyAmountByUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AmountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).GetManyAmountByUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/GetManyAmountByUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).GetManyAmountByUser(ctx, req.(*AmountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_GetAmountByIdea_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AmountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).GetAmountByIdea(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/GetAmountByIdea",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).GetAmountByIdea(ctx, req.(*AmountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_Withdraw_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdtAssetRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).Withdraw(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/Withdraw",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).Withdraw(ctx, req.(*IdtAssetRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_Deposit_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdtAssetRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).Deposit(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/Deposit",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).Deposit(ctx, req.(*IdtAssetRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_Lock_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LockRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).Lock(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/Lock",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).Lock(ctx, req.(*LockRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_Unlock_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdInfo)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).Unlock(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/Unlock",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).Unlock(ctx, req.(*IdInfo))
	}
	return interceptor(ctx, in, info, handler)
}

func _IdtAsset_Confirm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdInfo)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IdtAssetServer).Confirm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/idtbank.IdtAsset/Confirm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IdtAssetServer).Confirm(ctx, req.(*IdInfo))
	}
	return interceptor(ctx, in, info, handler)
}

// IdtAsset_ServiceDesc is the grpc.ServiceDesc for IdtAsset service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var IdtAsset_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "idtbank.IdtAsset",
	HandlerType: (*IdtAssetServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetAmountByUser",
			Handler:    _IdtAsset_GetAmountByUser_Handler,
		},
		{
			MethodName: "GetManyAmountByUser",
			Handler:    _IdtAsset_GetManyAmountByUser_Handler,
		},
		{
			MethodName: "GetAmountByIdea",
			Handler:    _IdtAsset_GetAmountByIdea_Handler,
		},
		{
			MethodName: "Withdraw",
			Handler:    _IdtAsset_Withdraw_Handler,
		},
		{
			MethodName: "Deposit",
			Handler:    _IdtAsset_Deposit_Handler,
		},
		{
			MethodName: "Lock",
			Handler:    _IdtAsset_Lock_Handler,
		},
		{
			MethodName: "Unlock",
			Handler:    _IdtAsset_Unlock_Handler,
		},
		{
			MethodName: "Confirm",
			Handler:    _IdtAsset_Confirm_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/proto/v1/idtbank/idtbank.proto",
}
