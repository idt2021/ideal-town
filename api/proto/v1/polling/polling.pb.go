// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.6.1
// source: api/proto/v1/polling/polling.proto

package polling

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	common "ideal-town/api/proto/v1/common"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

//
//Transfer <idc> from <srcId> to <dstId>:
//srcId: iid(I), sid(S, Winner), NULL(W)
//dstId: iid(I), sid(S, Winner), NULL(W)
type TransferRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid   string  `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	Idc   float32 `protobuf:"fixed32,2,opt,name=idc,proto3" json:"idc,omitempty"`
	SrcId uint32  `protobuf:"varint,3,opt,name=srcId,proto3" json:"srcId,omitempty"`
	DstId uint32  `protobuf:"varint,4,opt,name=dstId,proto3" json:"dstId,omitempty"`
}

func (x *TransferRequest) Reset() {
	*x = TransferRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_polling_polling_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TransferRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TransferRequest) ProtoMessage() {}

func (x *TransferRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_polling_polling_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TransferRequest.ProtoReflect.Descriptor instead.
func (*TransferRequest) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_polling_polling_proto_rawDescGZIP(), []int{0}
}

func (x *TransferRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *TransferRequest) GetIdc() float32 {
	if x != nil {
		return x.Idc
	}
	return 0
}

func (x *TransferRequest) GetSrcId() uint32 {
	if x != nil {
		return x.SrcId
	}
	return 0
}

func (x *TransferRequest) GetDstId() uint32 {
	if x != nil {
		return x.DstId
	}
	return 0
}

type TransitionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Iid uint32 `protobuf:"varint,1,opt,name=iid,proto3" json:"iid,omitempty"`
}

func (x *TransitionRequest) Reset() {
	*x = TransitionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_polling_polling_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TransitionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TransitionRequest) ProtoMessage() {}

func (x *TransitionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_polling_polling_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TransitionRequest.ProtoReflect.Descriptor instead.
func (*TransitionRequest) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_polling_polling_proto_rawDescGZIP(), []int{1}
}

func (x *TransitionRequest) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

// 不填: getPolling
// iid: getPollingByIid
// sid: getPollingBySid
// uid: getPollingByUid
type PollingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Iid uint32 `protobuf:"varint,1,opt,name=iid,proto3" json:"iid,omitempty"`
	Sid uint32 `protobuf:"varint,2,opt,name=sid,proto3" json:"sid,omitempty"`
	Uid string `protobuf:"bytes,3,opt,name=uid,proto3" json:"uid,omitempty"`
}

func (x *PollingRequest) Reset() {
	*x = PollingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_polling_polling_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PollingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PollingRequest) ProtoMessage() {}

func (x *PollingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_polling_polling_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PollingRequest.ProtoReflect.Descriptor instead.
func (*PollingRequest) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_polling_polling_proto_rawDescGZIP(), []int{2}
}

func (x *PollingRequest) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

func (x *PollingRequest) GetSid() uint32 {
	if x != nil {
		return x.Sid
	}
	return 0
}

func (x *PollingRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

type PollingInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Pid    uint32  `protobuf:"varint,1,opt,name=pid,proto3" json:"pid,omitempty"`
	Iid    uint32  `protobuf:"varint,2,opt,name=iid,proto3" json:"iid,omitempty"`
	Sid    uint32  `protobuf:"varint,3,opt,name=sid,proto3" json:"sid,omitempty"`
	Uid    string  `protobuf:"bytes,4,opt,name=uid,proto3" json:"uid,omitempty"`
	Target int32   `protobuf:"varint,5,opt,name=target,proto3" json:"target,omitempty"`
	Idc    float32 `protobuf:"fixed32,6,opt,name=idc,proto3" json:"idc,omitempty"`
}

func (x *PollingInfo) Reset() {
	*x = PollingInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_polling_polling_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PollingInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PollingInfo) ProtoMessage() {}

func (x *PollingInfo) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_polling_polling_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PollingInfo.ProtoReflect.Descriptor instead.
func (*PollingInfo) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_polling_polling_proto_rawDescGZIP(), []int{3}
}

func (x *PollingInfo) GetPid() uint32 {
	if x != nil {
		return x.Pid
	}
	return 0
}

func (x *PollingInfo) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

func (x *PollingInfo) GetSid() uint32 {
	if x != nil {
		return x.Sid
	}
	return 0
}

func (x *PollingInfo) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *PollingInfo) GetTarget() int32 {
	if x != nil {
		return x.Target
	}
	return 0
}

func (x *PollingInfo) GetIdc() float32 {
	if x != nil {
		return x.Idc
	}
	return 0
}

type PollingReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PollingInfo []*PollingInfo `protobuf:"bytes,1,rep,name=pollingInfo,proto3" json:"pollingInfo,omitempty"`
}

func (x *PollingReply) Reset() {
	*x = PollingReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_polling_polling_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PollingReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PollingReply) ProtoMessage() {}

func (x *PollingReply) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_polling_polling_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PollingReply.ProtoReflect.Descriptor instead.
func (*PollingReply) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_polling_polling_proto_rawDescGZIP(), []int{4}
}

func (x *PollingReply) GetPollingInfo() []*PollingInfo {
	if x != nil {
		return x.PollingInfo
	}
	return nil
}

var File_api_proto_v1_polling_polling_proto protoreflect.FileDescriptor

var file_api_proto_v1_polling_polling_proto_rawDesc = []byte{
	0x0a, 0x22, 0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x70,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x1a, 0x20, 0x61,
	0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a,
	0x29, 0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x61, 0x0a, 0x0f, 0x54, 0x72,
	0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a,
	0x03, 0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12,
	0x10, 0x0a, 0x03, 0x69, 0x64, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x02, 0x52, 0x03, 0x69, 0x64,
	0x63, 0x12, 0x14, 0x0a, 0x05, 0x73, 0x72, 0x63, 0x49, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0d,
	0x52, 0x05, 0x73, 0x72, 0x63, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x64, 0x73, 0x74, 0x49, 0x64,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x05, 0x64, 0x73, 0x74, 0x49, 0x64, 0x22, 0x25, 0x0a,
	0x11, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x69, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52,
	0x03, 0x69, 0x69, 0x64, 0x22, 0x46, 0x0a, 0x0e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x69, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x73, 0x69, 0x64, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x73, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69,
	0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x22, 0x7f, 0x0a, 0x0b,
	0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x10, 0x0a, 0x03, 0x70,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x70, 0x69, 0x64, 0x12, 0x10, 0x0a,
	0x03, 0x69, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x69, 0x64, 0x12,
	0x10, 0x0a, 0x03, 0x73, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x73, 0x69,
	0x64, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03,
	0x75, 0x69, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x74, 0x61, 0x72, 0x67, 0x65, 0x74, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x06, 0x74, 0x61, 0x72, 0x67, 0x65, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x69,
	0x64, 0x63, 0x18, 0x06, 0x20, 0x01, 0x28, 0x02, 0x52, 0x03, 0x69, 0x64, 0x63, 0x22, 0x46, 0x0a,
	0x0c, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x36, 0x0a,
	0x0b, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x49, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x14, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c,
	0x6c, 0x69, 0x6e, 0x67, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x0b, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x49, 0x6e, 0x66, 0x6f, 0x32, 0xd0, 0x09, 0x0a, 0x07, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x12, 0x4e, 0x0a, 0x0a, 0x47, 0x65, 0x74, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x12,
	0x17, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69,
	0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22,
	0x10, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x0a, 0x12, 0x08, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x12, 0x5e, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x42,
	0x79, 0x49, 0x69, 0x64, 0x12, 0x17, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e,
	0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x22, 0x1b, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x15, 0x12, 0x13, 0x2f, 0x70,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x69, 0x64, 0x65, 0x61, 0x2f, 0x7b, 0x69, 0x69, 0x64,
	0x7d, 0x12, 0x5d, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x42,
	0x79, 0x53, 0x69, 0x64, 0x12, 0x17, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e,
	0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x22, 0x1a, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x14, 0x12, 0x12, 0x2f, 0x70,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x73, 0x6f, 0x6c, 0x2f, 0x7b, 0x73, 0x69, 0x64, 0x7d,
	0x12, 0x5e, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x42, 0x79,
	0x55, 0x69, 0x64, 0x12, 0x17, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f,
	0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x70,
	0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x50, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65,
	0x70, 0x6c, 0x79, 0x22, 0x1b, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x15, 0x12, 0x13, 0x2f, 0x70, 0x6f,
	0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x75, 0x73, 0x65, 0x72, 0x2f, 0x7b, 0x75, 0x69, 0x64, 0x7d,
	0x12, 0x58, 0x0a, 0x0b, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x57, 0x32, 0x49, 0x12,
	0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66,
	0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x20, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1a,
	0x22, 0x15, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x66, 0x65, 0x72, 0x2f, 0x57, 0x32, 0x49, 0x3a, 0x01, 0x2a, 0x12, 0x58, 0x0a, 0x0b, 0x54, 0x72,
	0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x49, 0x32, 0x53, 0x12, 0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c,
	0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70,
	0x74, 0x79, 0x22, 0x20, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1a, 0x22, 0x15, 0x2f, 0x70, 0x6f, 0x6c,
	0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2f, 0x49, 0x32,
	0x53, 0x3a, 0x01, 0x2a, 0x12, 0x58, 0x0a, 0x0b, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72,
	0x57, 0x32, 0x53, 0x12, 0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72,
	0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x20, 0x82, 0xd3,
	0xe4, 0x93, 0x02, 0x1a, 0x22, 0x15, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2f, 0x57, 0x32, 0x53, 0x3a, 0x01, 0x2a, 0x12, 0x58,
	0x0a, 0x0b, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x49, 0x32, 0x57, 0x12, 0x18, 0x2e,
	0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x20, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1a, 0x22, 0x15,
	0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65,
	0x72, 0x2f, 0x49, 0x32, 0x57, 0x3a, 0x01, 0x2a, 0x12, 0x58, 0x0a, 0x0b, 0x54, 0x72, 0x61, 0x6e,
	0x73, 0x66, 0x65, 0x72, 0x53, 0x32, 0x57, 0x12, 0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x22, 0x20, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1a, 0x22, 0x15, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69,
	0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2f, 0x53, 0x32, 0x57, 0x3a,
	0x01, 0x2a, 0x12, 0x6d, 0x0a, 0x19, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e,
	0x54, 0x6f, 0x42, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x54, 0x72, 0x61, 0x64, 0x69, 0x6e, 0x67, 0x12,
	0x1a, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x69,
	0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x25, 0x82, 0xd3, 0xe4, 0x93,
	0x02, 0x1f, 0x22, 0x1a, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61,
	0x6e, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x2f, 0x62, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x3a, 0x01,
	0x2a, 0x12, 0x5f, 0x0a, 0x10, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x53, 0x32, 0x57,
	0x69, 0x6e, 0x6e, 0x65, 0x72, 0x12, 0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2e,
	0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x22,
	0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1c, 0x22, 0x17, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67,
	0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2f, 0x53, 0x32, 0x57, 0x69, 0x6e, 0x3a,
	0x01, 0x2a, 0x12, 0x5f, 0x0a, 0x10, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x49, 0x32,
	0x57, 0x69, 0x6e, 0x6e, 0x65, 0x72, 0x12, 0x18, 0x2e, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67,
	0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x22, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1c, 0x22, 0x17, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2f, 0x49, 0x32, 0x57, 0x69, 0x6e,
	0x3a, 0x01, 0x2a, 0x12, 0x63, 0x0a, 0x13, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x69, 0x74, 0x69, 0x6f,
	0x6e, 0x54, 0x6f, 0x54, 0x72, 0x61, 0x64, 0x69, 0x6e, 0x67, 0x12, 0x1a, 0x2e, 0x70, 0x6f, 0x6c,
	0x6c, 0x69, 0x6e, 0x67, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e,
	0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x21, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1b, 0x22, 0x16, 0x2f,
	0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x69, 0x74, 0x69,
	0x6f, 0x6e, 0x2f, 0x74, 0x6f, 0x3a, 0x01, 0x2a, 0x42, 0x21, 0x5a, 0x1f, 0x69, 0x64, 0x65, 0x61,
	0x6c, 0x2d, 0x74, 0x6f, 0x77, 0x6e, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2f, 0x76, 0x31, 0x2f, 0x70, 0x6f, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_api_proto_v1_polling_polling_proto_rawDescOnce sync.Once
	file_api_proto_v1_polling_polling_proto_rawDescData = file_api_proto_v1_polling_polling_proto_rawDesc
)

func file_api_proto_v1_polling_polling_proto_rawDescGZIP() []byte {
	file_api_proto_v1_polling_polling_proto_rawDescOnce.Do(func() {
		file_api_proto_v1_polling_polling_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_proto_v1_polling_polling_proto_rawDescData)
	})
	return file_api_proto_v1_polling_polling_proto_rawDescData
}

var file_api_proto_v1_polling_polling_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_api_proto_v1_polling_polling_proto_goTypes = []interface{}{
	(*TransferRequest)(nil),   // 0: polling.TransferRequest
	(*TransitionRequest)(nil), // 1: polling.TransitionRequest
	(*PollingRequest)(nil),    // 2: polling.PollingRequest
	(*PollingInfo)(nil),       // 3: polling.PollingInfo
	(*PollingReply)(nil),      // 4: polling.PollingReply
	(*common.Empty)(nil),      // 5: common.Empty
}
var file_api_proto_v1_polling_polling_proto_depIdxs = []int32{
	3,  // 0: polling.PollingReply.pollingInfo:type_name -> polling.PollingInfo
	2,  // 1: polling.Polling.GetPolling:input_type -> polling.PollingRequest
	2,  // 2: polling.Polling.GetPollingByIid:input_type -> polling.PollingRequest
	2,  // 3: polling.Polling.GetPollingBySid:input_type -> polling.PollingRequest
	2,  // 4: polling.Polling.GetPollingByUid:input_type -> polling.PollingRequest
	0,  // 5: polling.Polling.TransferW2I:input_type -> polling.TransferRequest
	0,  // 6: polling.Polling.TransferI2S:input_type -> polling.TransferRequest
	0,  // 7: polling.Polling.TransferW2S:input_type -> polling.TransferRequest
	0,  // 8: polling.Polling.TransferI2W:input_type -> polling.TransferRequest
	0,  // 9: polling.Polling.TransferS2W:input_type -> polling.TransferRequest
	1,  // 10: polling.Polling.TransitionToBeforeTrading:input_type -> polling.TransitionRequest
	0,  // 11: polling.Polling.TransferS2Winner:input_type -> polling.TransferRequest
	0,  // 12: polling.Polling.TransferI2Winner:input_type -> polling.TransferRequest
	1,  // 13: polling.Polling.TransitionToTrading:input_type -> polling.TransitionRequest
	4,  // 14: polling.Polling.GetPolling:output_type -> polling.PollingReply
	4,  // 15: polling.Polling.GetPollingByIid:output_type -> polling.PollingReply
	4,  // 16: polling.Polling.GetPollingBySid:output_type -> polling.PollingReply
	4,  // 17: polling.Polling.GetPollingByUid:output_type -> polling.PollingReply
	5,  // 18: polling.Polling.TransferW2I:output_type -> common.Empty
	5,  // 19: polling.Polling.TransferI2S:output_type -> common.Empty
	5,  // 20: polling.Polling.TransferW2S:output_type -> common.Empty
	5,  // 21: polling.Polling.TransferI2W:output_type -> common.Empty
	5,  // 22: polling.Polling.TransferS2W:output_type -> common.Empty
	5,  // 23: polling.Polling.TransitionToBeforeTrading:output_type -> common.Empty
	5,  // 24: polling.Polling.TransferS2Winner:output_type -> common.Empty
	5,  // 25: polling.Polling.TransferI2Winner:output_type -> common.Empty
	5,  // 26: polling.Polling.TransitionToTrading:output_type -> common.Empty
	14, // [14:27] is the sub-list for method output_type
	1,  // [1:14] is the sub-list for method input_type
	1,  // [1:1] is the sub-list for extension type_name
	1,  // [1:1] is the sub-list for extension extendee
	0,  // [0:1] is the sub-list for field type_name
}

func init() { file_api_proto_v1_polling_polling_proto_init() }
func file_api_proto_v1_polling_polling_proto_init() {
	if File_api_proto_v1_polling_polling_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_proto_v1_polling_polling_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TransferRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_polling_polling_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TransitionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_polling_polling_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PollingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_polling_polling_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PollingInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_polling_polling_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PollingReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_proto_v1_polling_polling_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_proto_v1_polling_polling_proto_goTypes,
		DependencyIndexes: file_api_proto_v1_polling_polling_proto_depIdxs,
		MessageInfos:      file_api_proto_v1_polling_polling_proto_msgTypes,
	}.Build()
	File_api_proto_v1_polling_polling_proto = out.File
	file_api_proto_v1_polling_polling_proto_rawDesc = nil
	file_api_proto_v1_polling_polling_proto_goTypes = nil
	file_api_proto_v1_polling_polling_proto_depIdxs = nil
}
