// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.6.1
// source: api/proto/v1/pr/pr.proto

package pr

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	common "ideal-town/api/proto/v1/common"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

//specify idea_id user_id and gitlab link
type PRRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Iid  uint32 `protobuf:"varint,1,opt,name=iid,proto3" json:"iid,omitempty"`
	Uid  string `protobuf:"bytes,2,opt,name=uid,proto3" json:"uid,omitempty"`
	Link string `protobuf:"bytes,3,opt,name=link,proto3" json:"link,omitempty"`
	Gid  string `protobuf:"bytes,4,opt,name=gid,proto3" json:"gid,omitempty"`
}

func (x *PRRequest) Reset() {
	*x = PRRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_pr_pr_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PRRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PRRequest) ProtoMessage() {}

func (x *PRRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_pr_pr_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PRRequest.ProtoReflect.Descriptor instead.
func (*PRRequest) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_pr_pr_proto_rawDescGZIP(), []int{0}
}

func (x *PRRequest) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

func (x *PRRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *PRRequest) GetLink() string {
	if x != nil {
		return x.Link
	}
	return ""
}

func (x *PRRequest) GetGid() string {
	if x != nil {
		return x.Gid
	}
	return ""
}

type GetPR struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Rid uint32 `protobuf:"varint,1,opt,name=rid,proto3" json:"rid,omitempty"`
	Iid uint32 `protobuf:"varint,2,opt,name=iid,proto3" json:"iid,omitempty"`
	Uid string `protobuf:"bytes,3,opt,name=uid,proto3" json:"uid,omitempty"`
}

func (x *GetPR) Reset() {
	*x = GetPR{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_pr_pr_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetPR) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetPR) ProtoMessage() {}

func (x *GetPR) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_pr_pr_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetPR.ProtoReflect.Descriptor instead.
func (*GetPR) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_pr_pr_proto_rawDescGZIP(), []int{1}
}

func (x *GetPR) GetRid() uint32 {
	if x != nil {
		return x.Rid
	}
	return 0
}

func (x *GetPR) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

func (x *GetPR) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

type PR struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Rid  uint32 `protobuf:"varint,1,opt,name=rid,proto3" json:"rid,omitempty"`
	Iid  uint32 `protobuf:"varint,2,opt,name=iid,proto3" json:"iid,omitempty"`
	Uid  string `protobuf:"bytes,3,opt,name=uid,proto3" json:"uid,omitempty"`
	Link string `protobuf:"bytes,4,opt,name=link,proto3" json:"link,omitempty"`
	Gid  string `protobuf:"bytes,5,opt,name=gid,proto3" json:"gid,omitempty"`
}

func (x *PR) Reset() {
	*x = PR{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_pr_pr_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PR) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PR) ProtoMessage() {}

func (x *PR) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_pr_pr_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PR.ProtoReflect.Descriptor instead.
func (*PR) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_pr_pr_proto_rawDescGZIP(), []int{2}
}

func (x *PR) GetRid() uint32 {
	if x != nil {
		return x.Rid
	}
	return 0
}

func (x *PR) GetIid() uint32 {
	if x != nil {
		return x.Iid
	}
	return 0
}

func (x *PR) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *PR) GetLink() string {
	if x != nil {
		return x.Link
	}
	return ""
}

func (x *PR) GetGid() string {
	if x != nil {
		return x.Gid
	}
	return ""
}

type PRList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Pr []*PR `protobuf:"bytes,1,rep,name=pr,proto3" json:"pr,omitempty"`
}

func (x *PRList) Reset() {
	*x = PRList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_proto_v1_pr_pr_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PRList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PRList) ProtoMessage() {}

func (x *PRList) ProtoReflect() protoreflect.Message {
	mi := &file_api_proto_v1_pr_pr_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PRList.ProtoReflect.Descriptor instead.
func (*PRList) Descriptor() ([]byte, []int) {
	return file_api_proto_v1_pr_pr_proto_rawDescGZIP(), []int{3}
}

func (x *PRList) GetPr() []*PR {
	if x != nil {
		return x.Pr
	}
	return nil
}

var File_api_proto_v1_pr_pr_proto protoreflect.FileDescriptor

var file_api_proto_v1_pr_pr_proto_rawDesc = []byte{
	0x0a, 0x18, 0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x70,
	0x72, 0x2f, 0x70, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x02, 0x70, 0x72, 0x1a, 0x20,
	0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x1a, 0x29, 0x61, 0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x55, 0x0a, 0x09, 0x50,
	0x52, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x69, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69,
	0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04,
	0x6c, 0x69, 0x6e, 0x6b, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6c, 0x69, 0x6e, 0x6b,
	0x12, 0x10, 0x0a, 0x03, 0x67, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x67,
	0x69, 0x64, 0x22, 0x3d, 0x0a, 0x05, 0x47, 0x65, 0x74, 0x50, 0x52, 0x12, 0x10, 0x0a, 0x03, 0x72,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x72, 0x69, 0x64, 0x12, 0x10, 0x0a,
	0x03, 0x69, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x69, 0x64, 0x12,
	0x10, 0x0a, 0x03, 0x75, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69,
	0x64, 0x22, 0x60, 0x0a, 0x02, 0x50, 0x52, 0x12, 0x10, 0x0a, 0x03, 0x72, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x72, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x69, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x75,
	0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12, 0x12, 0x0a,
	0x04, 0x6c, 0x69, 0x6e, 0x6b, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6c, 0x69, 0x6e,
	0x6b, 0x12, 0x10, 0x0a, 0x03, 0x67, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03,
	0x67, 0x69, 0x64, 0x22, 0x20, 0x0a, 0x06, 0x50, 0x52, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x16, 0x0a,
	0x02, 0x70, 0x72, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x06, 0x2e, 0x70, 0x72, 0x2e, 0x50,
	0x52, 0x52, 0x02, 0x70, 0x72, 0x32, 0xe8, 0x02, 0x0a, 0x0b, 0x50, 0x75, 0x6c, 0x6c, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x38, 0x0a, 0x08, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50,
	0x52, 0x12, 0x0d, 0x2e, 0x70, 0x72, 0x2e, 0x50, 0x52, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x0e, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x08, 0x22, 0x03, 0x2f, 0x70, 0x72, 0x3a, 0x01, 0x2a, 0x12,
	0x3c, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x50, 0x52, 0x42, 0x79, 0x49, 0x64, 0x65, 0x61, 0x12, 0x09,
	0x2e, 0x70, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x50, 0x52, 0x1a, 0x0a, 0x2e, 0x70, 0x72, 0x2e, 0x50,
	0x52, 0x4c, 0x69, 0x73, 0x74, 0x22, 0x16, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x10, 0x12, 0x0e, 0x2f,
	0x70, 0x72, 0x2f, 0x69, 0x64, 0x65, 0x61, 0x2f, 0x7b, 0x69, 0x69, 0x64, 0x7d, 0x12, 0x3c, 0x0a,
	0x0b, 0x47, 0x65, 0x74, 0x50, 0x52, 0x42, 0x79, 0x55, 0x73, 0x65, 0x72, 0x12, 0x09, 0x2e, 0x70,
	0x72, 0x2e, 0x47, 0x65, 0x74, 0x50, 0x52, 0x1a, 0x0a, 0x2e, 0x70, 0x72, 0x2e, 0x50, 0x52, 0x4c,
	0x69, 0x73, 0x74, 0x22, 0x16, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x10, 0x12, 0x0e, 0x2f, 0x70, 0x72,
	0x2f, 0x75, 0x73, 0x65, 0x72, 0x2f, 0x7b, 0x75, 0x69, 0x64, 0x7d, 0x12, 0x34, 0x0a, 0x09, 0x47,
	0x65, 0x74, 0x50, 0x52, 0x42, 0x79, 0x49, 0x44, 0x12, 0x09, 0x2e, 0x70, 0x72, 0x2e, 0x47, 0x65,
	0x74, 0x50, 0x52, 0x1a, 0x06, 0x2e, 0x70, 0x72, 0x2e, 0x50, 0x52, 0x22, 0x14, 0x82, 0xd3, 0xe4,
	0x93, 0x02, 0x0e, 0x12, 0x0c, 0x2f, 0x70, 0x72, 0x2f, 0x69, 0x64, 0x2f, 0x7b, 0x72, 0x69, 0x64,
	0x7d, 0x12, 0x37, 0x0a, 0x08, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x52, 0x12, 0x06, 0x2e,
	0x70, 0x72, 0x2e, 0x50, 0x52, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45,
	0x6d, 0x70, 0x74, 0x79, 0x22, 0x14, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x0e, 0x32, 0x0c, 0x2f, 0x70,
	0x72, 0x2f, 0x69, 0x64, 0x2f, 0x7b, 0x72, 0x69, 0x64, 0x7d, 0x12, 0x34, 0x0a, 0x08, 0x44, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x50, 0x52, 0x12, 0x09, 0x2e, 0x70, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x50,
	0x52, 0x1a, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x22, 0x0e, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x08, 0x2a, 0x03, 0x2f, 0x70, 0x72, 0x3a, 0x01, 0x2a,
	0x42, 0x1c, 0x5a, 0x1a, 0x69, 0x64, 0x65, 0x61, 0x6c, 0x2d, 0x74, 0x6f, 0x77, 0x6e, 0x2f, 0x61,
	0x70, 0x69, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x76, 0x31, 0x2f, 0x70, 0x72, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_api_proto_v1_pr_pr_proto_rawDescOnce sync.Once
	file_api_proto_v1_pr_pr_proto_rawDescData = file_api_proto_v1_pr_pr_proto_rawDesc
)

func file_api_proto_v1_pr_pr_proto_rawDescGZIP() []byte {
	file_api_proto_v1_pr_pr_proto_rawDescOnce.Do(func() {
		file_api_proto_v1_pr_pr_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_proto_v1_pr_pr_proto_rawDescData)
	})
	return file_api_proto_v1_pr_pr_proto_rawDescData
}

var file_api_proto_v1_pr_pr_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_api_proto_v1_pr_pr_proto_goTypes = []interface{}{
	(*PRRequest)(nil),    // 0: pr.PRRequest
	(*GetPR)(nil),        // 1: pr.GetPR
	(*PR)(nil),           // 2: pr.PR
	(*PRList)(nil),       // 3: pr.PRList
	(*common.Empty)(nil), // 4: common.Empty
}
var file_api_proto_v1_pr_pr_proto_depIdxs = []int32{
	2, // 0: pr.PRList.pr:type_name -> pr.PR
	0, // 1: pr.PullRequest.CreatePR:input_type -> pr.PRRequest
	1, // 2: pr.PullRequest.GetPRByIdea:input_type -> pr.GetPR
	1, // 3: pr.PullRequest.GetPRByUser:input_type -> pr.GetPR
	1, // 4: pr.PullRequest.GetPRByID:input_type -> pr.GetPR
	2, // 5: pr.PullRequest.UpdatePR:input_type -> pr.PR
	1, // 6: pr.PullRequest.DeletePR:input_type -> pr.GetPR
	4, // 7: pr.PullRequest.CreatePR:output_type -> common.Empty
	3, // 8: pr.PullRequest.GetPRByIdea:output_type -> pr.PRList
	3, // 9: pr.PullRequest.GetPRByUser:output_type -> pr.PRList
	2, // 10: pr.PullRequest.GetPRByID:output_type -> pr.PR
	4, // 11: pr.PullRequest.UpdatePR:output_type -> common.Empty
	4, // 12: pr.PullRequest.DeletePR:output_type -> common.Empty
	7, // [7:13] is the sub-list for method output_type
	1, // [1:7] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_api_proto_v1_pr_pr_proto_init() }
func file_api_proto_v1_pr_pr_proto_init() {
	if File_api_proto_v1_pr_pr_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_proto_v1_pr_pr_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PRRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_pr_pr_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetPR); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_pr_pr_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PR); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_proto_v1_pr_pr_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PRList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_proto_v1_pr_pr_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_proto_v1_pr_pr_proto_goTypes,
		DependencyIndexes: file_api_proto_v1_pr_pr_proto_depIdxs,
		MessageInfos:      file_api_proto_v1_pr_pr_proto_msgTypes,
	}.Build()
	File_api_proto_v1_pr_pr_proto = out.File
	file_api_proto_v1_pr_pr_proto_rawDesc = nil
	file_api_proto_v1_pr_pr_proto_goTypes = nil
	file_api_proto_v1_pr_pr_proto_depIdxs = nil
}
