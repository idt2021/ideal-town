module ideal-town

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.43.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.4
)

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.7.2
	github.com/rs/cors v1.8.2
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa
)
