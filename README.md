# Ideal-Town



## Getting started

We are Idealer!


## Recommended way to implement a service

1. add your `schemas` in `pkg/schemas` (including golang & .sql)
2. add your `service functions` with `input, output` (but DON'T implement)
3. add your `database functions` with `input, output` (but DON'T implement)
4. add `mock` struct, functions or something else for testing
4. add `corresponding tests` of the functions above
5. pass the tests!!

## How to cooperation through git?
```bash

# Change your branch to dev
git checkout dev

# Create a branch for your service
git branch <service-name>

# Change to the branch
git checkout <service-name>

# Compile protobufs in MAKEFILE
make compile-all

# Now, create a protobuf folder and a service folder
mkdir /api/proto/v1/<service-name>
mkdir /api/services/<service-name>

# Enter the two folders and write your own stuff!
(如果你寫完proto，可以去makefile裡面加入你的compile方法!)


############## Developing... #######################

# When you complete ALL OF YOUR jobs
git add .
git commit -m "your message"

# Checkout your branch to dev
git checkout dev

# Merge branch
git merge --no-ff <service-name>

# Close branch
git branch -d <service-name>

```

## Single service directory layout

For example, trading service under services folder:
```
📦trading                (service name)
 ┣ 📂internal            (package only for this project)
 ┃ ┗ 📂tradeserver
 ┃ ┃ ┗ 📂mock            (mock interface to help servie test)
 ┃ ┃ ┃ ┗ mock.go
 ┃ ┃ ┗ 📂tradedb         (db interface to help servie implementation)
 ┃ ┃ ┃ ┃ tradedb.go       
 ┃ ┃ ┃ ┗ tradedb_test.go
 ┃ ┃ ┣ 📜trading.go
 ┃ ┃ ┗ 📜trading_test.go (put the auto generate test aside your package)
 ┃ ┗ 📂handlers          (optional)
 ┃   ┗ 📜handler.go      (handle restful api route)
 ┣ 📜main.go             (every single service needs a main.go)
 ┗ 📜README.md           (what you have done?)

```

## Testing helper
<br/><br/>
### For database

`sqlmanager.Agent` implements connection functions to databases.

For safety consideration, **DO NOT** use `NewUnsafeConn` when release

Since `NewUnsafeConn` does not block global update.

#### How to connect

program will `panic` when connection failed

```go
agent := sqlmanager.NewUnsafeConn(&sqlmanager.ConnInfo{
    Username: "your name",
    Password: "your password",
    Host:     "your host",
    Port:     "your port",
    DBName:   "your database name",
    DebugMode: true // if true, log sql command
})

// Insert an user
agent.Conn.Create(&schema.User{})
```

#### Reset database

**NOTE** program will `panic` if agent is not in `unsafe mode`

```go
// truncate table(s) as many as you want
agent.ResetDB(&schemas.Assets{}, &schemas.IdtRecords{}, &schemas.Orders{}, &schemas.Transactions{})
agent.ResetDB("assets","idt_records","orders","transaction")
```

<br/><br/>
---
<br/><br/>

### For Grpc


#### How to connect (Bank example)
```go
// Create dialer with db connection
func dialer() func(context.Context, string) (net.Conn, error) {
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	agent := sqlmanager.NewConn(&sqlmanager.ConnInfo{
		/* SAME AS ABOVE */
	})
	pb.RegisterBankServer(server, &BankServer{
		Agent: agent,
	})
	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()
	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}

func createConn(t *testing.T) (context.Context, *grpc.ClientConn, pb.BankClient) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, DialTarget, 
        grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {
		t.Fatalf("Fail to dial %s: %v", DialTarget, err)
	}
	client := pb.NewBankClient(conn)
	return ctx, conn, client
}
```
